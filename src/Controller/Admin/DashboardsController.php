<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Dashboards Controller
 *
 * @method \App\Model\Entity\Dashboard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {

        if(!boolval($user['is_admin'])){
            throw new ForbiddenException();
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function index()
    {

    }

    public function counts(){
        $users = TableRegistry::getTableLocator()->get('Users')->find()->count();
        $divisions = TableRegistry::getTableLocator()->get('Divisions')->find()->count();
        $suppliers = TableRegistry::getTableLocator()->get('Suppliers')->find()->count();
        $collection = new Collection([
            [
                'users' => intval($users),
                'divisions' => intval($divisions),
                'suppliers' => intval($suppliers)
            ]
        ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection->first()));
    }

    public function getSupplies(){
        $data = TableRegistry::getTableLocator()->get('Supplies')
            ->find()
            ->contain([
                'Divisions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->select([
                                'division'
                            ]);
                    }
                ]
            ])
            ->select([
               'count' => 'COUNT(Supplies.division_id)',
            ])
            ->group([
                'Supplies.division_id'
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function procurements(){
        $requests = TableRegistry::getTableLocator()->get('Requests')->find('all')->count();
        $orders = TableRegistry::getTableLocator()->get('Orders')->find('all')->count();
        $inspections = TableRegistry::getTableLocator()->get('Inspections')->find('all')->count();
        $requisitions = TableRegistry::getTableLocator()->get('Requisitions')->find('all')->count();

        $collection = new Collection([
            [
                'requests' => intval($requests),
                'orders' => intval($orders),
                'inspections' => intval($inspections),
                'requisitions' => intval($requisitions),
            ]
        ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection->first()));
    }

    public function getProcurementsIntervals(){
        $now = (new Moment(null,'Asia/Manila'));

        $weekStart = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $weekEnd = (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(1)->format('Y-m-d');

        $monthStart = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $monthEnd = (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(1)->format('Y-m-d');

        $yearStart = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $yearEnd = (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');

        $requests = TableRegistry::getTableLocator()->get('Requests');
        $orders = TableRegistry::getTableLocator()->get('Orders');
        $inspections = TableRegistry::getTableLocator()->get('Inspections');
        $requisitions = TableRegistry::getTableLocator()->get('Requisitions');


        $collection = new Collection([
            [
                'today' => [
                    'requests_approved' => $requests->find('all')->where([
                        'created >=' => $now->format('Y-m-d'),
                        'created <=' => $now->addDays(1)->format('Y-m-d'),
                        'is_approved' => intval(1)
                    ])->count(),
                    'requests_declined' => $requests->find('all')->where([
                        'created >=' => $now->format('Y-m-d'),
                        'created <=' => $now->addDays(1)->format('Y-m-d'),
                        'is_declined' => intval(1)
                    ])->count(),
                    'orders' => $orders->find('all')->where([
                        'created >=' => $now->format('Y-m-d'),
                        'created <=' => $now->addDays(1)->format('Y-m-d'),
                    ])->count(),
                    'inspections' => $inspections->find('all')->where([
                        'created >=' => $now->format('Y-m-d'),
                        'created <=' => $now->addDays(1)->format('Y-m-d'),
                    ])->count(),
                    'requisitions' => $requisitions->find('all')->where([
                        'created >=' => $now->format('Y-m-d'),
                        'created <=' => $now->addDays(1)->format('Y-m-d'),
                    ])->count(),
                ],
                'week' => [
                    'requests_approved' => $requests->find('all')->where([
                        'created >=' => $weekStart,
                        'created <=' => $weekEnd,
                        'is_approved' => intval(1)
                    ])->count(),
                    'requests_declined' => $requests->find('all')->where([
                        'created >=' => $weekStart,
                        'created <=' => $weekEnd,
                        'is_declined' => intval(1)
                    ])->count(),
                    'orders' => $orders->find('all')->where([
                        'created >=' => $weekStart,
                        'created <=' => $weekEnd,
                    ])->count(),
                    'inspections' => $inspections->find('all')->where([
                        'created >=' => $weekStart,
                        'created <=' => $weekEnd,
                    ])->count(),
                    'requisitions' => $requisitions->find('all')->where([
                        'created >=' => $weekStart,
                        'created <=' => $weekEnd,
                    ])->count(),
                ],
                'month' => [
                    'requests_approved' => $requests->find('all')->where([
                        'created >=' => $monthStart,
                        'created <=' => $monthEnd,
                        'is_approved' => intval(1)
                    ])->count(),
                    'requests_declined' => $requests->find('all')->where([
                        'created >=' => $monthStart,
                        'created <=' => $monthEnd,
                        'is_declined' => intval(1)
                    ])->count(),
                    'orders' => $orders->find('all')->where([
                        'created >=' => $monthStart,
                        'created <=' => $monthEnd,
                    ])->count(),
                    'inspections' => $inspections->find('all')->where([
                        'created >=' => $monthStart,
                        'created <=' => $monthEnd,
                    ])->count(),
                    'requisitions' => $requisitions->find('all')->where([
                        'created >=' => $monthStart,
                        'created <=' => $monthEnd,
                    ])->count(),
                ],
                'year' => [
                    'requests_approved' => $requests->find('all')->where([
                        'created >=' => $yearStart,
                        'created <=' => $yearEnd,
                        'is_approved' => intval(1)
                    ])->count(),
                    'requests_declined' => $requests->find('all')->where([
                        'created >=' => $yearStart,
                        'created <=' => $yearEnd,
                        'is_declined' => intval(1)
                    ])->count(),
                    'orders' => $orders->find('all')->where([
                        'created >=' => $yearStart,
                        'created <=' => $yearEnd,
                    ])->count(),
                    'inspections' => $inspections->find('all')->where([
                        'created >=' => $yearStart,
                        'created <=' => $yearEnd,
                    ])->count(),
                    'requisitions' => $requisitions->find('all')->where([
                        'created >=' => $yearStart,
                        'created <=' => $yearEnd,
                    ])->count(),
                ]
            ]
        ]);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection->first()));
    }

    public function applications(){
        $applications = TableRegistry::getTableLocator()->get('Applications')
            ->find()
            ->count();

        $ris = TableRegistry::getTableLocator()->get('Serieses')
            ->find()
            ->where([
                'is_expendable =' => intval(1)
            ])
            ->count();

        $ics = TableRegistry::getTableLocator()->get('Serieses')
            ->find()
            ->where([
                'is_expendable =' => intval(0)
            ])
            ->count();

        $collection = (new Collection([
            [
                'applications' => intval($applications),
                'ris' => intval($ris),
                'ics' => intval($ics)
            ]
        ]));

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection->first()));

    }

    public function divisionApplications(){
        $data = TableRegistry::getTableLocator()->get('Applications')
            ->find()
            ->select([
                'month' => 'MONTHNAME(created)',
                'total' => 'COUNT(MONTHNAME(created))'
            ])
            ->group([
                'month'
            ])
            ->order(['created' => 'ASC'], true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSerieses(){
        $query = TableRegistry::getTableLocator()->get('Applications')
            ->find();

        $pending = $query->where([
            'is_declined =' => intval(0),
            'is_approved =' => intval(0)
        ])->count();

        $approved = $query->where([
            'is_approved =' => intval(1)
        ])->count();

        $declined = $query->where([
            'is_declined =' => intval(1)
        ])->count();

        $data = (new Collection([
            [
                'label' => strtoupper('approved'),
                'total' => doubleval($approved)
            ],
            [
                'label' => strtoupper('declined'),
                'total' => doubleval($declined)
            ],
            [
                'label' => strtoupper('pending'),
                'total' => doubleval($pending)
            ]
        ]))->sortBy('total',SORT_DESC, SORT_NUMERIC)->toList();

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

}
