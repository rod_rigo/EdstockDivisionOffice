<?php
declare(strict_types=1);

namespace App\Controller\Division;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;

/**
 * Positions Controller
 *
 * @property \App\Model\Table\PositionsTable $Positions
 * @method \App\Model\Entity\Position[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PositionsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('division');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {

        if(!boolval($user['is_division'])){
            throw new ForbiddenException();
        }

        $actions = [strtolower('edit'), strtolower('delete'), strtolower('restore'), strtolower('hardDelete')];
        if (in_array(strtolower($this->request->getParam('action')), $actions)) {

            $paramId = (int)$this->request->getParam('pass.0');
            if ($this->Positions->isOwnedBy(intval($paramId), intval($user['division_id']))) {
                return true;
            }

            throw new ForbiddenException();
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Positions->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getPositions(){
        $data = $this->Positions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Divisions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'division_id =' => intval($this->Auth->user('division_id'))
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getPositionsDeleted(){
        $data = $this->Positions->find('all',['withDeleted'])
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Divisions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'division_id =' => intval($this->Auth->user('division_id'))
            ])
            ->whereNotNull('Positions.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $position = $this->Positions->newEmptyEntity();
        if ($this->request->is('post')) {
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            $position->user_id = intval($this->Auth->user('id'));
            $position->division_id = intval($this->Auth->user('division_id'));
            if ($this->Positions->save($position)) {
                $result = ['message' => ucwords('The position has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($position->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'), 'errors' => $position->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Position id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $position = $this->Positions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            $position->user_id = intval($this->Auth->user('id'));
            $position->division_id = intval($this->Auth->user('division_id'));
            if ($this->Positions->save($position)) {
                $result = ['message' => ucwords('The position has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($position->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'), 'errors' => $position->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($position));
    }

    /**
     * Delete method
     *
     * @param string|null $id Position id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id,[
            'contain' => [
                'Officials' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]
        ]);

        if(count($position->officials)){
            $result = ['message' => ucwords('The position has been constrained to officials'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Positions->delete($position)) {
            $result = ['message' => ucwords('The position has been deleted.'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position could not be deleted. Please, try again.'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id,[
            'withDeleted'
        ]);
        if ($this->Positions->restore($position)) {
            $result = ['message' => ucwords('The position has been restored.'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position could not be restored. Please, try again.'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id,[
            'withDeleted',
            'contain' => [
                'Officials' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]
        ]);

        if(count($position->officials)){
            $result = ['message' => ucwords('The position has been constrained to officials'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Positions->hardDelete($position)) {
            $result = ['message' => ucwords('The position has been deleted.'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position could not be deleted. Please, try again.'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
