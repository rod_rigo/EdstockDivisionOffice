<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Application Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $division_id
 * @property int $office_id
 * @property string $no
 * @property int $is_approved
 * @property \Cake\I18n\FrozenTime|null $approved_at
 * @property int $is_declined
 * @property \Cake\I18n\FrozenTime|null $declined_at
 * @property string $purpose
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\ApplicationSupply[] $application_supplies
 * @property \App\Model\Entity\Seriese[] $serieses
 */
class Application extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division_id' => true,
        'office_id' => true,
        'no' => true,
        'is_approved' => true,
        'approved_at' => true,
        'is_declined' => true,
        'declined_at' => true,
        'purpose' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'division' => true,
        'office' => true,
        'application_supplies' => true,
        'serieses' => true,
    ];

}
