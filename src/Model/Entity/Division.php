<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Division Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $division
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Inspection[] $inspections
 * @property \App\Model\Entity\Office[] $offices
 * @property \App\Model\Entity\Official[] $officials
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Profile[] $profiles
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\Requisition[] $requisitions
 * @property \App\Model\Entity\Plan[] $plans
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\Supplier[] $suppliers
 * @property \App\Model\Entity\FundCluster[] $fund_clusters
 * @property \App\Model\Entity\Method[] $methods
 * @property \App\Model\Entity\Supply[] $supplies
 */
class Division extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'inspections' => true,
        'offices' => true,
        'officials' => true,
        'orders' => true,
        'profiles' => true,
        'requests' => true,
        'requisitions' => true,
        'plans' => true,
        'positions' => true,
        'suppliers' => true,
        'fund_clusters' => true,
        'methods' => true,
        'supplies' => true,
    ];

    protected function _setDivision($value){
        return $this->division = ucwords($value);
    }

}
