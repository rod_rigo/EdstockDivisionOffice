<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Office Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $division_id
 * @property string $office
 * @property string $office_code
 * @property string $telephone_number
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Inspection[] $inspections
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Profile[] $profiles
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\Requisition[] $requisitions
 */
class Office extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division_id' => true,
        'office' => true,
        'office_code' => true,
        'telephone_number' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'division' => true,
        'inspections' => true,
        'orders' => true,
        'profiles' => true,
        'requests' => true,
        'requisitions' => true,
    ];

    protected function _setOffice($value){
        return $this->office = ucwords($value);
    }

    protected function _setOfficeCode($value){
        return $this->office_code = strtoupper($value);
    }

}
