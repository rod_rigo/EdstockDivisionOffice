<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Part Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $plan_id
 * @property string $part
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\PlanDetail[] $plan_details
 * @property \App\Model\Entity\PlanTitle[] $plan_titles
 */
class Part extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'plan_id' => true,
        'part' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'plan' => true,
        'plan_details' => true,
        'plan_titles' => true,
    ];

    protected function _setPart($value){
        return $this->part = ucwords($value);
    }

}
