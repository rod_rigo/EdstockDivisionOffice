<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plan Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $division_id
 * @property string $plan
 * @property string $description
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Part[] $parts
 * @property \App\Model\Entity\PlanDetail[] $plan_details
 * @property \App\Model\Entity\PlanTitle[] $plan_titles
 */
class Plan extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division_id' => true,
        'plan' => true,
        'description' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'division' => true,
        'parts' => true,
        'plan_details' => true,
        'plan_titles' => true,
    ];

    protected function _setPlan($value){
        return $this->plan = ucwords($value);
    }

}
