<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlanDetail Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $plan_id
 * @property int $plan_title_id
 * @property int $part_id
 * @property string $supply_code
 * @property int $supply_id
 * @property float $quantity
 * @property float $price
 * @property float $total
 * @property float $january
 * @property float $february
 * @property float $march
 * @property float $quarter_one
 * @property float $april
 * @property float $may
 * @property float $june
 * @property float $quarter_two
 * @property float $july
 * @property float $august
 * @property float $september
 * @property float $quarter_three
 * @property float $october
 * @property float $november
 * @property float $december
 * @property float $quarter_four
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\PlanTitle $plan_title
 * @property \App\Model\Entity\Part $part
 * @property \App\Model\Entity\Supply $supply
 */
class PlanDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'plan_id' => true,
        'plan_title_id' => true,
        'part_id' => true,
        'supply_code' => true,
        'supply_id' => true,
        'quantity' => true,
        'price' => true,
        'total' => true,
        'january' => true,
        'february' => true,
        'march' => true,
        'quarter_one' => true,
        'april' => true,
        'may' => true,
        'june' => true,
        'quarter_two' => true,
        'july' => true,
        'august' => true,
        'september' => true,
        'october' => true,
        'quarter_three' => true,
        'november' => true,
        'december' => true,
        'quarter_four' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'plan' => true,
        'plan_title' => true,
        'part' => true,
        'supply' => true,
    ];
}
