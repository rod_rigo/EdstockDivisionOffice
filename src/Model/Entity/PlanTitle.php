<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlanTitle Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $plan_id
 * @property int $part_id
 * @property string $plan_title
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\Part $part
 * @property \App\Model\Entity\PlanDetail[] $plan_details
 */
class PlanTitle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'plan_id' => true,
        'part_id' => true,
        'plan_title' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'plan' => true,
        'part' => true,
        'plan_details' => true,
    ];

    protected function _setPlanTitle($value){
        return $this->plan_title = ucwords($value);
    }

}
