<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Range Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $code
 * @property float $start_price
 * @property float $end_price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Supply[] $supplies
 */
class Range extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'code' => true,
        'start_price' => true,
        'end_price' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'supplies' => true,
    ];

    protected function _setCode($value){
        return $this->code = strtoupper($value);
    }

}
