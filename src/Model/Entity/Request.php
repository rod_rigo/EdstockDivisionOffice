<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Request Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $division_id
 * @property int $office_id
 * @property int $fund_cluster_id
 * @property string $no
 * @property string $purpose
 * @property int $is_approved
 * @property \Cake\I18n\FrozenTime $approved_at
 * @property int $is_declined
 * @property \Cake\I18n\FrozenTime $declined_at
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\FundCluster $fund_cluster
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\RequestSupply[] $request_supplies
 * @property \App\Model\Entity\Inspection $inspection
 * @property \App\Model\Entity\Requisition $requisition
 */
class Request extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division_id' => true,
        'office_id' => true,
        'fund_cluster_id' => true,
        'no' => true,
        'purpose' => true,
        'is_approved' => true,
        'approved_at' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'division' => true,
        'office' => true,
        'fund_cluster' => true,
        'order' => true,
        'request_supplies' => true,
        'inspection' => true,
        'requisition' => true,
    ];
}
