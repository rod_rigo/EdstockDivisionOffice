<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Requisition Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $request_id
 * @property int $order_id
 * @property int $inspection_id
 * @property int $division_id
 * @property int $office_id
 * @property int $fund_cluster_id
 * @property int $supplier_id
 * @property int $method_id
 * @property string $no
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Inspection $inspection
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\FundCluster $fund_cluster
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Method $method
 * @property \App\Model\Entity\RequisitionSupply[] $requisition_supplies
 */
class Requisition extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'request_id' => true,
        'order_id' => true,
        'inspection_id' => true,
        'division_id' => true,
        'office_id' => true,
        'fund_cluster_id' => true,
        'supplier_id' => true,
        'method_id' => true,
        'no' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'request' => true,
        'order' => true,
        'inspection' => true,
        'division' => true,
        'office' => true,
        'fund_cluster' => true,
        'supplier' => true,
        'method' => true,
        'requisition_supplies' => true,
    ];
}
