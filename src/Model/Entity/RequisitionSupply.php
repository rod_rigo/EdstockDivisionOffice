<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequisitionSupply Entity
 *
 * @property int $id
 * @property int $requisition_id
 * @property int $supply_id
 * @property float $price
 * @property float $quantity
 * @property float $total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Requisition $requisition
 * @property \App\Model\Entity\Supply $supply
 */
class RequisitionSupply extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'requisition_id' => true,
        'supply_id' => true,
        'price' => true,
        'quantity' => true,
        'total' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'requisition' => true,
        'supply' => true,
    ];
}
