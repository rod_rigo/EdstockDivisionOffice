<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Seriese Entity
 *
 * @property int $id
 * @property int $application_id
 * @property string $no
 * @property int $is_expendable
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Application $application
 * @property \App\Model\Entity\ApplicationSupply $application_supplies
 */
class Seriese extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_id' => true,
        'no' => true,
        'is_expendable' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'application' => true,
        'application_supplies' => true,
    ];
}
