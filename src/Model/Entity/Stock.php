<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stock Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $supply_id
 * @property float $quantity
 * @property int $is_in
 * @property float $total_stocks
 * @property float $previous_stocks
 * @property string $description
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Supply $supply
 */
class Stock extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'supply_id' => true,
        'quantity' => true,
        'is_in' => true,
        'total_stocks' => true,
        'previous_stocks' => true,
        'description' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'supply' => true,
    ];

    protected function _setDesription($value){
        return $this->description = ucwords($value);
    }

}
