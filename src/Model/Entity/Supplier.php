<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $division_id
 * @property string $supplier
 * @property string $tin_number
 * @property string $address
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Inspection[] $inspections
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Requisition[] $requisitions
 */
class Supplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'division_id' => true,
        'supplier' => true,
        'tin_number' => true,
        'address' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'division' => true,
        'inspections' => true,
        'orders' => true,
        'requisitions' => true,
    ];

    protected function _setSupplier($value){
        return $this->supplier = strtoupper($value);
    }

    protected function _setAddress($value){
        return $this->address = ucwords($value);
    }

}
