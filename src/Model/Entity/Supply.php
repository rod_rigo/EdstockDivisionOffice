<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supply Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $supply_name
 * @property string $description
 * @property int $category_id
 * @property int $division_id
 * @property int $unit_id
 * @property int $range_id
 * @property string $supply_code
 * @property float $stock
 * @property float $price
 * @property int $in_stock
 * @property int $is_expendable
 * @property \Cake\I18n\FrozenTime $expired_at
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Stock[] $stocks
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Division $division
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Range $range
 * @property \App\Model\Entity\InspectionSupply[] $inspection_supplies
 * @property \App\Model\Entity\OrderSupply[] $order_supplies
 * @property \App\Model\Entity\PlanDetail[] $plan_details
 * @property \App\Model\Entity\RequestSupply[] $request_supplies
 * @property \App\Model\Entity\RequisitionSupply[] $requisition_supplies
 */
class Supply extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'supply_name' => true,
        'description' => true,
        'division_id' => true,
        'category_id' => true,
        'unit_id' => true,
        'range_id' => true,
        'supply_code' => true,
        'stock' => true,
        'price' => true,
        'in_stock' => true,
        'is_expendable' => true,
        'expired_at' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'category' => true,
        'division' => true,
        'unit' => true,
        'range' => true,
        'stocks' => true,
        'inspection_supplies' => true,
        'order_supplies' => true,
        'plan_details' => true,
        'request_supplies' => true,
        'requisition_supplies' => true,
    ];

    protected function _setSupplyName($value){
        return $this->supply_name = ucwords($value);
    }

    protected function _setSupplyCode($value){
        return $this->supply_code = strtoupper($value);
    }

}
