<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $contact_number
 * @property string $password
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property int $is_admin
 * @property int $is_division
 * @property int $is_secretary
 * @property int $is_non_teaching
 * @property int $is_active
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Division[] $divisions
 * @property \App\Model\Entity\FundCluster[] $fund_clusters
 * @property \App\Model\Entity\Inspection[] $inspections
 * @property \App\Model\Entity\Method[] $methods
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\Office[] $offices
 * @property \App\Model\Entity\Official[] $officials
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Part[] $parts
 * @property \App\Model\Entity\PlanDetail[] $plan_details
 * @property \App\Model\Entity\PlanTitle[] $plan_titles
 * @property \App\Model\Entity\Plan[] $plans
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\Profile $profile
 * @property \App\Model\Entity\Range[] $ranges
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\Requisition[] $requisitions
 * @property \App\Model\Entity\Stock[] $stocks
 * @property \App\Model\Entity\Supplier[] $suppliers
 * @property \App\Model\Entity\Supply[] $supplies
 * @property \App\Model\Entity\Unit[] $units
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'email' => true,
        'contact_number' => true,
        'password' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'is_admin' => true,
        'is_division' => true,
        'is_secretary' => true,
        'is_non_teaching' => true,
        'is_active' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'categories' => true,
        'divisions' => true,
        'fund_clusters' => true,
        'inspections' => true,
        'methods' => true,
        'notifications' => true,
        'offices' => true,
        'officials' => true,
        'orders' => true,
        'parts' => true,
        'plan_details' => true,
        'plan_titles' => true,
        'plans' => true,
        'positions' => true,
        'profile' => true,
        'ranges' => true,
        'requests' => true,
        'requisitions' => true,
        'stocks' => true,
        'suppliers' => true,
        'supplies' => true,
        'units' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword($value)
    {
        if (strlen($value) > 0) {
            return (new DefaultPasswordHasher())->hash($value);
        }
    }

    protected function _setFirstName($value){
        return $this->first_name = ucwords($value);
    }

    protected function _setMiddleName($value){
        return $this->middle_name = ucwords($value);
    }

    protected function _setLastName($value){
        return $this->last_name = ucwords($value);
    }

}
