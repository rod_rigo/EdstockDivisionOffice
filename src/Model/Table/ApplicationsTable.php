<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Applications Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\BelongsTo $Divisions
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\ApplicationSuppliesTable&\Cake\ORM\Association\HasMany $ApplicationSupplies
 * @property \App\Model\Table\SeriesesTable&\Cake\ORM\Association\HasMany $Serieses
 *
 * @method \App\Model\Entity\Application newEmptyEntity()
 * @method \App\Model\Entity\Application newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Application[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Application get($primaryKey, $options = [])
 * @method \App\Model\Entity\Application findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Application patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Application[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Application|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Application saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Application[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Application[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Application[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Application[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('applications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ApplicationSupplies', [
            'foreignKey' => 'application_id',
        ])->setDependent(true);
        $this->hasMany('Serieses', [
            'foreignKey' => 'application_id',
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('division_id',ucwords('please select division'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select division'),false);

        $validator
            ->numeric('office_id',ucwords('please select office'))
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please select office'),false);

        $validator
            ->scalar('no')
            ->maxLength('no', 255)
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please enter a no'), false);

        $validator
            ->requirePresence('is_approved', true)
            ->notEmptyString('is_approved', ucwords('please select is approved'), false)
            ->add('is_approved','is_approved',[
                'rule' => function($value){
                    $isApproved = [0, 1];

                    if(!in_array(intval($value),$isApproved)){
                        return ucwords('please select is approved');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('approved_at')
            ->allowEmptyDateTime('approved_at');

        $validator
            ->notEmptyString('is_declined')
            ->notEmptyString('is_declined', ucwords('please select is declined'), false)
            ->add('is_declined','is_declined',[
                'rule' => function($value){
                    $isApproved = [0, 1];

                    if(!in_array(intval($value),$isApproved)){
                        return ucwords('please select is approved');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('declined_at')
            ->allowEmptyDateTime('declined_at');

        $validator
            ->scalar('purpose')
            ->maxLength('purpose', 4294967295)
            ->requirePresence('purpose', true)
            ->notEmptyString('purpose', ucwords('please enter a purpose'),false);

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 4294967295)
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please enter a remarks'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])->where(['id =' => intval($paramId), 'division_id =' => intval($divisionId)])->count();
    }

    public function isOwned($paramId, $userId)
    {
        return $this->find('all',['withDeleted'])->where(['id =' => intval($paramId), 'user_id =' => intval($userId)])->count();
    }

    public function no(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $application = $this->find()
            ->where([
                'Applications.created >=' => $startMonth,
                'Applications.created <=' => $endMonth,
                'Applications.is_approved =' => intval(1),
            ])->last();
        $year = date('Y');
        $month = date('m');
        $no = (!empty($application))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $application->no)[2]) + 1)), 4, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 4, '0', STR_PAD_LEFT));
        return strval($no);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->existsIn(['office_id'], 'Offices'), ['errorField' => 'office_id']);

        return $rules;
    }
}
