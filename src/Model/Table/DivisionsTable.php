<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Divisions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\HasMany $Inspections
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\HasMany $Offices
 * @property \App\Model\Table\OfficialsTable&\Cake\ORM\Association\HasMany $Officials
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ProfilesTable&\Cake\ORM\Association\HasMany $Profiles
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 *
 * @method \App\Model\Entity\Division newEmptyEntity()
 * @method \App\Model\Entity\Division newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Division[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Division get($primaryKey, $options = [])
 * @method \App\Model\Entity\Division findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Division patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Division[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Division|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Division saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Division[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Division[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Division[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Division[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DivisionsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('divisions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Inspections', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Offices', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Officials', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Profiles', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Requisitions', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Plans', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Suppliers', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('FundClusters', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Methods', [
            'foreignKey' => 'division_id',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'division_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('division')
            ->maxLength('division', 255)
            ->requirePresence('division', true)
            ->notEmptyString('division', ucwords('please enter a division'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['division'], ucwords('this division is already exists')), ['errorField' => 'division']);

        return $rules;
    }
}
