<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * FundClusters Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\HasMany $Inspections
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 *
 * @method \App\Model\Entity\FundCluster newEmptyEntity()
 * @method \App\Model\Entity\FundCluster newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\FundCluster[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FundCluster get($primaryKey, $options = [])
 * @method \App\Model\Entity\FundCluster findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\FundCluster patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FundCluster[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\FundCluster|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FundCluster saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FundCluster[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FundCluster[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\FundCluster[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FundCluster[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FundClustersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('fund_clusters');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Inspections', [
            'foreignKey' => 'fund_cluster_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'fund_cluster_id',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'fund_cluster_id',
        ]);
        $this->hasMany('Requisitions', [
            'foreignKey' => 'fund_cluster_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('division_id', ucwords('please select a division'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select a division'), false);

        $validator
            ->scalar('fund_cluster')
            ->maxLength('fund_cluster', 255)
            ->requirePresence('fund_cluster', true)
            ->notEmptyString('fund_cluster', ucwords('please enter fund cluster'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['division_id'], 'Users'), ['errorField' => 'division_id']);
        $rules->add($rules->isUnique(['fund_cluster', 'division_id'], ucwords('this fund cluster has already exists in this division')), ['errorField' => 'fund_cluster']);

        return $rules;
    }
}
