<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * InspectionSupplies Model
 *
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\BelongsTo $Inspections
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \App\Model\Entity\InspectionSupply newEmptyEntity()
 * @method \App\Model\Entity\InspectionSupply newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\InspectionSupply[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InspectionSupply get($primaryKey, $options = [])
 * @method \App\Model\Entity\InspectionSupply findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\InspectionSupply patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InspectionSupply[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\InspectionSupply|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InspectionSupply saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InspectionSupply[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionSupply[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionSupply[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionSupply[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InspectionSuppliesTable extends Table
{

    use SoftDeleteTrait;

    public function beforeSave(EventInterface $event)
    {
        $entity = $event->getData('entity');

        $connection = ConnectionManager::get('default');

        if ($entity->isNew()) {
            $connection->begin();
            try{
                $connection->execute('SET FOREIGN_KEY_CHECKS=0;');

                $supply = TableRegistry::getTableLocator()->get('Supplies')->get(intval($entity->supply_id));
                $stocks = $supply->stock;

                $total = intval($stocks) + intval($entity->quantity);
                $supply->stock = intval($total);

                TableRegistry::getTableLocator()->get('Supplies')->save($supply);

                $stock = TableRegistry::getTableLocator()->get('Supplies')->newEmptyEntity();
                $stock->user_id = intval($_SESSION['Auth']['User']['id']);
                $stock->supply_id = intval($supply->id);
                $stock->description = strtoupper('PROCUREMENT (IN)');
                $stock->price = doubleval($entity->price);
                $stock->previous_stocks = intval($stocks);
                $stock->quantity = intval($entity->quantity);
                $stock->total_stocks = intval($total);
                TableRegistry::getTableLocator()->get('Stocks')->save($stock);

                $connection->commit();
                return true;
            }catch (\Exception $exception){
                $connection->rollback();
                dd($exception->getMessage());
            }



        }
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('inspection_supplies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Inspections', [
            'foreignKey' => 'inspection_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('supply_id', ucwords('please enter a supply'))
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('please enter a supply'));

        $validator
            ->numeric('price', ucwords('please enter a price'))
            ->requirePresence('price', true)
            ->notEmptyString('price', ucwords('please enter a price'));

        $validator
            ->numeric('quantity', ucwords('please enter a quantity'))
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please enter a quantity'));

        $validator
            ->numeric('total', ucwords('please enter a total'))
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please enter a total'));

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['inspection_id'], 'Inspections'), ['errorField' => 'inspection_id']);
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'), ['errorField' => 'supply_id']);

        return $rules;
    }
}
