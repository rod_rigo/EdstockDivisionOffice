<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Orders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\BelongsTo $Requests
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\BelongsTo $Divisions
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\FundClustersTable&\Cake\ORM\Association\BelongsTo $FundClusters
 * @property \App\Model\Table\SuppliersTable&\Cake\ORM\Association\BelongsTo $Suppliers
 * @property \App\Model\Table\MethodsTable&\Cake\ORM\Association\BelongsTo $Methods
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\HasMany $Inspections
 * @property \App\Model\Table\OrderSuppliesTable&\Cake\ORM\Association\HasMany $OrderSupplies
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 *
 * @method \App\Model\Entity\Order newEmptyEntity()
 * @method \App\Model\Entity\Order newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FundClusters', [
            'foreignKey' => 'fund_cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Methods', [
            'foreignKey' => 'method_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne('Inspections', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('OrderSupplies', [
            'foreignKey' => 'order_id',
        ])->setDependent(true);
        $this->hasOne('Requisitions', [
            'foreignKey' => 'order_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('no')
            ->maxLength('no', 255)
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please enter a no'), false);

        $validator
            ->numeric('request_id', ucwords('please select a request'))
            ->requirePresence('request_id', true)
            ->notEmptyString('request_id', ucwords('please select a request'), false);

        $validator
            ->numeric('division_id', ucwords('please select a division'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select a division'), false);

        $validator
            ->numeric('office_id', ucwords('please select a office'))
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please select a office'), false);

        $validator
            ->numeric('fund_cluster_id', ucwords('please select a fund cluster'))
            ->requirePresence('fund_cluster_id', true)
            ->notEmptyString('fund_cluster_id', ucwords('please select a fund cluster'), false);

        $validator
            ->numeric('supplier_id', ucwords('please select a supplier'))
            ->requirePresence('supplier_id', true)
            ->notEmptyString('supplier_id', ucwords('please select a supplier'), false);

        $validator
            ->numeric('method_id', ucwords('please select a method'))
            ->requirePresence('method_id', true)
            ->notEmptyString('method_id', ucwords('please select a method'), false);

        $validator
            ->scalar('place_of_delivery')
            ->maxLength('place_of_delivery', 4294967295)
            ->requirePresence('place_of_delivery', false)
            ->notEmptyString('place_of_delivery', ucwords('please enter a place of delivery'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['request_id'], 'Requests'), ['errorField' => 'request_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->existsIn(['office_id'], 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn(['fund_cluster_id'], 'FundClusters'), ['errorField' => 'fund_cluster_id']);
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'), ['errorField' => 'supplier_id']);
        $rules->add($rules->existsIn(['method_id'], 'Methods'), ['errorField' => 'method_id']);
        $rules->add($rules->isUnique(['request_id'], ucwords('this request has already order')), ['errorField' => 'request_id']);

        return $rules;
    }
}
