<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * PlanDetails Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PlansTable&\Cake\ORM\Association\BelongsTo $Plans
 * @property \App\Model\Table\PlanTitlesTable&\Cake\ORM\Association\BelongsTo $PlanTitles
 * @property \App\Model\Table\PartsTable&\Cake\ORM\Association\BelongsTo $Parts
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \App\Model\Entity\PlanDetail newEmptyEntity()
 * @method \App\Model\Entity\PlanDetail newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\PlanDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PlanDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\PlanDetail findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\PlanDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PlanDetail[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\PlanDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanDetail[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanDetail[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanDetail[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanDetail[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlanDetailsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('plan_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PlanTitles', [
            'foreignKey' => 'plan_title_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Parts', [
            'foreignKey' => 'part_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('plan_id', ucwords('please select a plan'))
            ->requirePresence('plan_id', true)
            ->notEmptyString('plan_id', ucwords('please select a plan'), false);

        $validator
            ->numeric('plan_title_id', ucwords('please select a plan title'))
            ->requirePresence('plan_title_id', true)
            ->notEmptyString('plan_title_id', ucwords('please select a plan title'), false);

        $validator
            ->numeric('part_id', ucwords('please select a part'))
            ->requirePresence('part_id', true)
            ->notEmptyString('part_id', ucwords('please select a part'), false);

        $validator
            ->numeric('supply_id', ucwords('please select a supply'))
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('please select a supply'), false);

        $validator
            ->scalar('supply_code')
            ->maxLength('supply_code', 255)
            ->requirePresence('supply_code', true)
            ->notEmptyString('supply_code', ucwords('please enter a supply code'), false);

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please enter a quantity'), false);

        $validator
            ->numeric('price')
            ->requirePresence('price', true)
            ->notEmptyString('price', ucwords('please enter a price'), false);

        $validator
            ->numeric('total')
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please enter a supply code'), false);

        $validator
            ->numeric('january')
            ->requirePresence('january', true)
            ->notEmptyString('january', ucwords('please enter a quantity for january'), false);

        $validator
            ->numeric('february')
            ->requirePresence('february', true)
            ->notEmptyString('february', ucwords('please enter a quantity for february'), false);

        $validator
            ->numeric('march')
            ->requirePresence('march', true)
            ->notEmptyString('march', ucwords('please enter a quantity for march'), false);

        $validator
            ->numeric('quarter_one')
            ->requirePresence('quarter_one', true)
            ->notEmptyString('quarter_one', ucwords('please enter a quantity for quarter one'), false);

        $validator
            ->numeric('april')
            ->requirePresence('april', true)
            ->notEmptyString('april', ucwords('please enter a quantity for april'), false);

        $validator
            ->numeric('may')
            ->requirePresence('may', true)
            ->notEmptyString('may', ucwords('please enter a quantity for may'), false);

        $validator
            ->numeric('june')
            ->requirePresence('june', true)
            ->notEmptyString('june', ucwords('please enter a quantity for june'), false);

        $validator
            ->numeric('quarter_two')
            ->requirePresence('quarter_two', true)
            ->notEmptyString('quarter_two', ucwords('please enter a quantity for quarter two'), false);

        $validator
            ->numeric('july')
            ->requirePresence('july', true)
            ->notEmptyString('july', ucwords('please enter a quantity for july'), false);

        $validator
            ->numeric('august')
            ->requirePresence('august', true)
            ->notEmptyString('august', ucwords('please enter a quantity for august'), false);

        $validator
            ->numeric('september')
            ->requirePresence('september', true)
            ->notEmptyString('september', ucwords('please enter a quantity for september'), false);

        $validator
            ->numeric('quarter_three')
            ->requirePresence('quarter_three', true)
            ->notEmptyString('quarter_three', ucwords('please enter a quantity for quarter three'), false);

        $validator
            ->numeric('october')
            ->requirePresence('october', true)
            ->notEmptyString('october', ucwords('please enter a quantity for october'), false);

        $validator
            ->numeric('november')
            ->requirePresence('november', true)
            ->notEmptyString('november', ucwords('please enter a quantity for november'), false);

        $validator
            ->numeric('december')
            ->requirePresence('december', true)
            ->notEmptyString('december', ucwords('please enter a quantity for december'), false);

        $validator
            ->numeric('quarter_four')
            ->requirePresence('quarter_four', true)
            ->notEmptyString('quarter_four', ucwords('please enter a quantity for quarter four'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['plan_id'], 'Plans'), ['errorField' => 'plan_id']);
        $rules->add($rules->existsIn(['plan_title_id'], 'PlanTitles'), ['errorField' => 'plan_title_id']);
        $rules->add($rules->existsIn(['part_id'], 'Parts'), ['errorField' => 'part_id']);
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'), ['errorField' => 'supply_id']);

        return $rules;
    }
}
