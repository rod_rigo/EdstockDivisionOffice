<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Plans Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\BelongsTo $Divisions
 * @property \App\Model\Table\PartsTable&\Cake\ORM\Association\HasMany $Parts
 * @property \App\Model\Table\PlanDetailsTable&\Cake\ORM\Association\HasMany $PlanDetails
 * @property \App\Model\Table\PlanTitlesTable&\Cake\ORM\Association\HasMany $PlanTitles
 *
 * @method \App\Model\Entity\Plan newEmptyEntity()
 * @method \App\Model\Entity\Plan newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Plan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Plan get($primaryKey, $options = [])
 * @method \App\Model\Entity\Plan findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Plan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Plan[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Plan|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plan saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plan[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plan[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plan[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plan[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlansTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('plans');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Parts', [
            'foreignKey' => 'plan_id',
        ]);
        $this->hasMany('PlanDetails', [
            'foreignKey' => 'plan_id',
        ]);
        $this->hasMany('PlanTitles', [
            'foreignKey' => 'plan_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('division_id', ucwords('please select division unit'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select a division'), false);

        $validator
            ->scalar('plan')
            ->maxLength('plan', 255)
            ->requirePresence('plan', true)
            ->notEmptyString('plan', ucwords('please enter a plan'), false);

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please enter a description'), false);

        $validator
            ->notEmptyString('is_active',ucwords('please select a active'), false)
            ->add('is_active', 'is_active', [
                'rule' => function($value){
                    $array = [intval(0), intval(1)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('please select a is active');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->isUnique(['division_id', 'plan'], ucwords('this plan already exists')), ['errorField' => 'plan']);

        return $rules;
    }
}
