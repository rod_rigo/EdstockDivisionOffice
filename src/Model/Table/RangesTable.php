<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Ranges Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\HasMany $Supplies
 *
 * @method \App\Model\Entity\Range newEmptyEntity()
 * @method \App\Model\Entity\Range newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Range[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Range get($primaryKey, $options = [])
 * @method \App\Model\Entity\Range findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Range patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Range[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Range|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Range saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Range[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Range[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Range[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Range[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RangesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('ranges');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'range_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', true)
            ->notEmptyString('code', ucwords('please enter a code'), false);

        $validator
            ->numeric('start_price')
            ->requirePresence('start_price', true)
            ->notEmptyString('start_price', ucwords('please enter a start price'), false)
            ->add('start_price','start_price',[
                'rule' => function($value){
                    if(intval($value) < intval(0)){
                        return ucwords('start price must not lower to 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('end_price')
            ->requirePresence('end_price', true)
            ->notEmptyString('end_price')
            ->notEmptyString('end_price', ucwords('please enter a end price'), false)
            ->add('end_price','end_price',[
                'rule' => function($value){
                    if(intval($value) < intval(0)){
                        return ucwords('end price must not lower to 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['code'], ucwords('this code is already exists')), ['errorField' => 'code']);

        return $rules;
    }
}
