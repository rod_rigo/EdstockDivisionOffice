<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * RequestSupplies Model
 *
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\BelongsTo $Requests
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \App\Model\Entity\RequestSupply newEmptyEntity()
 * @method \App\Model\Entity\RequestSupply newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RequestSupply[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RequestSupply get($primaryKey, $options = [])
 * @method \App\Model\Entity\RequestSupply findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RequestSupply patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RequestSupply[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RequestSupply|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestSupply saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestSupply[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestSupply[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestSupply[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestSupply[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequestSuppliesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('request_supplies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('supply_id', ucwords('please select a supply'))
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('please select a supply'),false);

        $validator
            ->numeric('price', ucwords('please enter a price'))
            ->requirePresence('price', true)
            ->notEmptyString('price', ucwords('please enter a price'),false);

        $validator
            ->numeric('quantity', ucwords('please enter a quantity'))
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please enter a quantity'),false);

        $validator
            ->numeric('total', ucwords('please enter a total'))
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please enter a total'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'), ['errorField' => 'request_id']);
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'), ['errorField' => 'supply_id']);

        return $rules;
    }
}
