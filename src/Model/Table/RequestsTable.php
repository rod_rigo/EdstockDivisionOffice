<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;
/**
 * Requests Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\BelongsTo $Divisions
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\FundClustersTable&\Cake\ORM\Association\BelongsTo $FundClusters
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\RequestSuppliesTable&\Cake\ORM\Association\HasMany $RequestSupplies
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 *
 * @method \App\Model\Entity\Request newEmptyEntity()
 * @method \App\Model\Entity\Request newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Request[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Request get($primaryKey, $options = [])
 * @method \App\Model\Entity\Request findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Request patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Request[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Request|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequestsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FundClusters', [
            'foreignKey' => 'fund_cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne('Orders', [
            'foreignKey' => 'request_id',
        ]);
        $this->hasMany('RequestSupplies', [
            'foreignKey' => 'request_id',
        ])->setDependent(true);
        $this->hasOne('Inspections', [
            'foreignKey' => 'request_id',
        ]);
        $this->hasOne('Requisitions', [
            'foreignKey' => 'request_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('division_id', ucwords('please select a division'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select a division'), false);

        $validator
            ->numeric('office_id', ucwords('please select a office'))
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please select a office'), false);

        $validator
            ->numeric('fund_cluster_id', ucwords('please select a fund cluster'))
            ->requirePresence('fund_cluster_id', true)
            ->notEmptyString('fund_cluster_id', ucwords('please select a fund cluster'), false);

        $validator
            ->scalar('no')
            ->maxLength('no', 255)
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please enter a no'), false);

        $validator
            ->scalar('purpose')
            ->maxLength('purpose', 4294967295)
            ->requirePresence('purpose', true)
            ->notEmptyString('purpose', ucwords('please enter a purpose'), false);

        $validator
            ->requirePresence('is_approved',true)
            ->notEmptyString('is_approved', ucwords('please select is approved'), true)
            ->add('is_approved', 'is_approved', [
                'rule' => function($value){
                    $array = [intval(0)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('invalid is approved');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('approved_at')
            ->requirePresence('approved_at',true)
            ->allowEmptyDateTime('approved_at', ucwords('invalid is approved date'), false)
            ->allowEmptyDateTime('approved_at', ucwords('invalid is approved date'), true);

        $validator
            ->requirePresence('is_declined',true)
            ->notEmptyString('is_declined', ucwords('please select is declined'), true)
            ->add('is_declined', 'is_declined', [
                'rule' => function($value){
                    $array = [intval(0)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('invalid is declined');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('declined_at')
            ->requirePresence('declined_at',true)
            ->allowEmptyDateTime('declined_at', ucwords('invalid is declined date'), false)
            ->allowEmptyDateTime('declined_at', ucwords('invalid is declined date'), true);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->existsIn(['office_id'], 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn(['fund_cluster_id'], 'FundClusters'), ['errorField' => 'fund_cluster_id']);

        return $rules;
    }

    public function no(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $request = $this->find()
            ->where([
                'Requests.created >=' => $startMonth,
                'Requests.created <=' => $endMonth,
                'Requests.is_approved =' => intval(1),
            ])->last();
        $year = date('Y');
        $month = date('m');
        $no = (!empty($request))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $request->no)[2]) + 1)), 4, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 4, '0', STR_PAD_LEFT));
        return strval($no);
    }

}
