<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Requisitions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\BelongsTo $Requests
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\BelongsTo $Inspections
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\BelongsTo $Divisions
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\FundClustersTable&\Cake\ORM\Association\BelongsTo $FundClusters
 * @property \App\Model\Table\SuppliersTable&\Cake\ORM\Association\BelongsTo $Suppliers
 * @property \App\Model\Table\MethodsTable&\Cake\ORM\Association\BelongsTo $Methods
 * @property \App\Model\Table\RequisitionSuppliesTable&\Cake\ORM\Association\HasMany $RequisitionSupplies
 *
 * @method \App\Model\Entity\Requisition newEmptyEntity()
 * @method \App\Model\Entity\Requisition newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Requisition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Requisition get($primaryKey, $options = [])
 * @method \App\Model\Entity\Requisition findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Requisition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Requisition[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Requisition|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requisition saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requisition[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Requisition[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Requisition[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Requisition[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequisitionsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('requisitions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Inspections', [
            'foreignKey' => 'inspection_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FundClusters', [
            'foreignKey' => 'fund_cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Methods', [
            'foreignKey' => 'method_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('RequisitionSupplies', [
            'foreignKey' => 'requisition_id',
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('no')
            ->maxLength('no', 255)
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please enter a no'), false);

        $validator
            ->numeric('request_id', ucwords('please select a request'))
            ->requirePresence('request_id', true)
            ->notEmptyString('request_id', ucwords('please select a request'), false);

        $validator
            ->numeric('order_id', ucwords('please select a order'))
            ->requirePresence('order_id', true)
            ->notEmptyString('order_id', ucwords('please select a order'), false);

        $validator
            ->numeric('inspection_id', ucwords('please select a inspection'))
            ->requirePresence('inspection_id', true)
            ->notEmptyString('inspection_id', ucwords('please select a inspection'), false);

        $validator
            ->numeric('division_id', ucwords('please select a division'))
            ->requirePresence('division_id', true)
            ->notEmptyString('division_id', ucwords('please select a division'), false);

        $validator
            ->numeric('office_id', ucwords('please select a office'))
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please select a office'), false);

        $validator
            ->numeric('fund_cluster_id', ucwords('please select a fund cluster'))
            ->requirePresence('fund_cluster_id', true)
            ->notEmptyString('fund_cluster_id', ucwords('please select a fund cluster'), false);

        $validator
            ->numeric('supplier_id', ucwords('please select a supplier'))
            ->requirePresence('supplier_id', true)
            ->notEmptyString('supplier_id', ucwords('please select a supplier'), false);

        $validator
            ->numeric('method_id', ucwords('please select a method'))
            ->requirePresence('method_id', true)
            ->notEmptyString('method_id', ucwords('please select a method'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['request_id'], 'Requests'), ['errorField' => 'request_id']);
        $rules->add($rules->existsIn(['order_id'], 'Orders'), ['errorField' => 'order_id']);
        $rules->add($rules->existsIn(['inspection_id'], 'Inspections'), ['errorField' => 'inspection_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->existsIn(['office_id'], 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn(['fund_cluster_id'], 'FundClusters'), ['errorField' => 'fund_cluster_id']);
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'), ['errorField' => 'supplier_id']);
        $rules->add($rules->existsIn(['method_id'], 'Methods'), ['errorField' => 'method_id']);

        $rules->add($rules->isUnique(['request_id'], ucwords('this request has already requisition')), ['errorField' => 'request_id']);
        $rules->add($rules->isUnique(['order_id'], ucwords('this order has already requisition')), ['errorField' => 'order_id']);
        $rules->add($rules->isUnique(['inspection_id'], ucwords('this inspection has already requisition')), ['errorField' => 'inspection_id']);
        $rules->add($rules->isUnique(['request_id', 'order_id','inspection_id'], ucwords('this request has already order, inspection, and requisition')), ['errorField' => 'request_id']);

        return $rules;
    }
}
