<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Serieses Model
 *
 * @property \App\Model\Table\ApplicationsTable&\Cake\ORM\Association\BelongsTo $Applications
 *
 * @method \App\Model\Entity\Seriese newEmptyEntity()
 * @method \App\Model\Entity\Seriese newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Seriese[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Seriese get($primaryKey, $options = [])
 * @method \App\Model\Entity\Seriese findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Seriese patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Seriese[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Seriese|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Seriese saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Seriese[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Seriese[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Seriese[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Seriese[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SeriesesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('serieses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Applications', [
            'foreignKey' => 'application_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('ApplicationSupplies', [
            'foreignKey' => 'series_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('no')
            ->maxLength('no', 255)
            ->requirePresence('no', 'create')
            ->notEmptyString('no');

        $validator
            ->requirePresence('is_expendable', 'create')
            ->notEmptyString('is_expendable');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->exists(['id' => $paramId, 'Applications.division_id' => $divisionId]);
    }

    public function no($isExpendable = null){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $request = $this->find()
            ->where([
                'Serieses.created >=' => $startMonth,
                'Serieses.created <=' => $endMonth,
                'Serieses.is_expendable =' => intval($isExpendable),
            ])->last();
        $year = date('Y');
        $month = date('m');
        $series = (!empty($request))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $request->no)[2]) + 1)), 4, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 4, '0', STR_PAD_LEFT));
        return $series;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['application_id'], 'Applications'), ['errorField' => 'application_id']);

        return $rules;
    }
}
