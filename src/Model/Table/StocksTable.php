<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Stocks Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \App\Model\Entity\Stock newEmptyEntity()
 * @method \App\Model\Entity\Stock newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Stock[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Stock get($primaryKey, $options = [])
 * @method \App\Model\Entity\Stock findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Stock patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Stock[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Stock|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stock saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StocksTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stocks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('supply_id', ucwords('no supply available'))
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('no supply available'), false);

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please enter quantity to process'), false)
            ->add('quantity', 'quantity', [
                'rule' => function($value){

                    if(intval($value) < 1){
                        return ucwords('quantity must be higher than 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_in', true)
            ->notEmptyString('is_in', ucwords('please select process'), false)
            ->add('is_in','is_in',[
                'rule' => function($value){

                    $array = [intval(0), intval(1)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('invalid process value');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('total_stocks')
            ->requirePresence('total_stocks', true)
            ->notEmptyString('total_stocks', ucwords('total stocks is invalid'), false)
            ->add('total_stocks', 'total_stocks', [
                'rule' => function($value){

                    if(intval($value) < 1){
                        return ucwords('total stocks must be higher than 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('previous_stocks')
            ->requirePresence('previous_stocks', true)
            ->notEmptyString('previous_stocks', ucwords('previous stocks is invalid'), false)
            ->add('previous_stocks', 'previous_stocks', [
                'rule' => function($value){

                    if(intval($value) < 1){
                        return ucwords('previous stocks must be higher than 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please enter a description'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'), ['errorField' => 'supply_id']);

        return $rules;
    }
}
