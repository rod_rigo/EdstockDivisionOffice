<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Supplies Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\UnitsTable&\Cake\ORM\Association\BelongsTo $Units
 * @property \App\Model\Table\RangesTable&\Cake\ORM\Association\BelongsTo $Ranges
 * @property \App\Model\Table\InspectionSuppliesTable&\Cake\ORM\Association\HasMany $InspectionSupplies
 * @property \App\Model\Table\OrderSuppliesTable&\Cake\ORM\Association\HasMany $OrderSupplies
 * @property \App\Model\Table\PlanDetailsTable&\Cake\ORM\Association\HasMany $PlanDetails
 * @property \App\Model\Table\RequestSuppliesTable&\Cake\ORM\Association\HasMany $RequestSupplies
 * @property \App\Model\Table\RequisitionSuppliesTable&\Cake\ORM\Association\HasMany $RequisitionSupplies
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\HasMany $Stocks
 *
 * @method \App\Model\Entity\Supply newEmptyEntity()
 * @method \App\Model\Entity\Supply newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Supply[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Supply get($primaryKey, $options = [])
 * @method \App\Model\Entity\Supply findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Supply patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Supply[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Supply|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supply saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supply[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Supply[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Supply[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Supply[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SuppliesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('supplies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Divisions', [
            'foreignKey' => 'division_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Units', [
            'foreignKey' => 'unit_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Ranges', [
            'foreignKey' => 'range_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('InspectionSupplies', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('OrderSupplies', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('PlanDetails', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('RequestSupplies', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('RequisitionSupplies', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('Stocks', [
            'foreignKey' => 'supply_id',
        ]);
        $this->hasMany('PlanDetails', [
            'foreignKey' => 'supply_id',
        ]);
    }

    public function beforeFind(EventInterface $event, Query $query, \ArrayObject $options, $primary)
    {
        TableRegistry::getTableLocator()->get('Supplies')->updateAll([
            'in_stock' => intval(0)
        ],[
            'stock <=' => intval(0)
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('supply_name')
            ->maxLength('supply_name', 255)
            ->requirePresence('supply_name', true)
            ->notEmptyString('supply_name', ucwords('please enter a supply name'), false);

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please enter a description'), false);

        $validator
            ->numeric('category_id', ucwords('please select a category'))
            ->requirePresence('category_id', true)
            ->notEmptyString('category_id', ucwords('please select a category'), false);

        $validator
            ->numeric('division_id', ucwords('please select a division'))
            ->requirePresence('division_id', 'update')
            ->notEmptyString('division_id', ucwords('please select a division'), 'update');

        $validator
            ->numeric('unit_id', ucwords('please select a unit'))
            ->requirePresence('unit_id', true)
            ->notEmptyString('unit_id', ucwords('please select a unit'), false);

        $validator
            ->requirePresence('range_id', true)
            ->notEmptyString('range_id', ucwords('please select a range'), false);

        $validator
            ->scalar('supply_code')
            ->maxLength('supply_code', 255)
            ->requirePresence('supply_code', true)
            ->notEmptyString('supply_code', ucwords('please enter a supply code'), false);

        $validator
            ->numeric('stock')
            ->requirePresence('stock', 'create')
            ->notEmptyString('stock', ucwords('please enter a stock'), false)
            ->add('stock','stock',[
                'rule' => function($value){

                    if(doubleval($value) < doubleval(0)){
                        return ucwords('stocks must be higher than 0');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('price')
            ->requirePresence('price', true)
            ->notEmptyString('price', ucwords('please enter a price'), false)
            ->add('price','price',[
                'rule' => function($value){

                    $range = TableRegistry::getTableLocator()->get('Ranges')->find()->select([
                        'max' => 'MAX(end_price)'
                    ])
                    ->first();

                    if(empty($range)){
                        return ucwords('No Range Values Available');
                    }

                    if(doubleval($value) > doubleval($range->max)){
                        return ucwords('the price must be '.(strval($range->max).' below'));
                    }

                    return true;

                }
            ]);

        $validator
            ->requirePresence('in_stock', true)
            ->notEmptyString('in_stock', ucwords('please select a in stock value'), false)
            ->add('in_stock','in_stock',[
                'rule' => function($value){

                    $array = [intval(0), intval(1)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('invalid in stock value');
                    }

                    return true;

                }
            ]);

        $validator
            ->requirePresence('is_expendable', true)
            ->notEmptyString('is_expendable', ucwords('please select a is expendable value'), false)
            ->add('is_expendable','is_expendable',[
                'rule' => function($value){

                    $array = [intval(0), intval(1)];

                    if(!in_array(intval($value), $array)){
                        return ucwords('is expendable stock value');
                    }

                    return true;

                }
            ]);

        $validator
            ->date('expired_at')
            ->requirePresence('expired_at', false)
            ->allowEmptyDate('expired_at', ucwords('please select expired at'), true)
            ->add('expired_at', 'expired_at', [
                'rule' => function($value){

                    if(intval(strtotime('today')) < intval(strtotime($value))){
                        return ucwords('expired at must be higher from today');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $divisionId)
    {
        return $this->find('all',['withDeleted'])
            ->where([
                'id =' => intval($paramId),
                'division_id =' => intval($divisionId)
            ])->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['category_id'], 'Categories'), ['errorField' => 'category_id']);
        $rules->add($rules->existsIn(['division_id'], 'Divisions'), ['errorField' => 'division_id']);
        $rules->add($rules->existsIn(['unit_id'], 'Units'), ['errorField' => 'unit_id']);
        $rules->add($rules->existsIn(['range_id'], 'Ranges'), ['errorField' => 'range_id']);
        $rules->add($rules->isUnique(['supply_name', 'division_id'], ucwords('this supply is already exists in this division')), ['errorField' => 'supply_name']);
        $rules->add($rules->isUnique(['supply_code', 'division_id'], ucwords('this supply code is already exists in this division')), ['errorField' => 'supply_code']);

        return $rules;
    }
}
