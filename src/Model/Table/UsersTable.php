<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Filesystem\Folder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use LetterAvatar\LetterAvatar;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\HasMany $Categories
 * @property \App\Model\Table\DivisionsTable&\Cake\ORM\Association\HasMany $Divisions
 * @property \App\Model\Table\FundClustersTable&\Cake\ORM\Association\HasMany $FundClusters
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\HasMany $Inspections
 * @property \App\Model\Table\MethodsTable&\Cake\ORM\Association\HasMany $Methods
 * @property \App\Model\Table\NotificationsTable&\Cake\ORM\Association\HasMany $Notifications
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\HasMany $Offices
 * @property \App\Model\Table\OfficialsTable&\Cake\ORM\Association\HasMany $Officials
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\PartsTable&\Cake\ORM\Association\HasMany $Parts
 * @property \App\Model\Table\PlanDetailsTable&\Cake\ORM\Association\HasMany $PlanDetails
 * @property \App\Model\Table\PlanTitlesTable&\Cake\ORM\Association\HasMany $PlanTitles
 * @property \App\Model\Table\PlansTable&\Cake\ORM\Association\HasMany $Plans
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\HasMany $Positions
 * @property \App\Model\Table\ProfilesTable&\Cake\ORM\Association\HasMany $Profiles
 * @property \App\Model\Table\RangesTable&\Cake\ORM\Association\HasMany $Ranges
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\HasMany $Stocks
 * @property \App\Model\Table\SuppliersTable&\Cake\ORM\Association\HasMany $Suppliers
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\HasMany $Supplies
 * @property \App\Model\Table\UnitsTable&\Cake\ORM\Association\HasMany $Units
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    use SoftDeleteTrait;

    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Categories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Divisions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('FundClusters', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Inspections', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Methods', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Offices', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Officials', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Parts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('PlanDetails', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('PlanTitles', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Plans', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('Profiles', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Ranges', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Requisitions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Stocks', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Suppliers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Units', [
            'foreignKey' => 'user_id',
        ]);
    }

    public function afterSave(EventInterface $event, EntityInterface $entity, \ArrayObject $arrayObject){

        try{

            $path = WWW_ROOT. 'img'. DS. 'user-avatar';
            $folder = new Folder();
            if(!$folder->cd($path)){
                $folder->create($path);
            }

            $letterAvatar = new LetterAvatar();

            $fontpath = WWW_ROOT. 'font'. DS. 'Poppins-ExtraBold.ttf';

            if($entity->isNew()){
                $letter = substr($entity->first_name,0,1);
                $filepath  = WWW_ROOT. 'img'. DS. 'user-avatar'. DS. strval($entity->id).'.png';

                $letterAvatar
                    ->setFontFile($fontpath)
                    ->generate(strtoupper($letter), null)
                    ->saveAsPng($filepath);
            }

            if($entity->isDirty('first_name')){
                $letter = substr($entity->first_name,0,1);
                $filepath  = WWW_ROOT. 'img'. DS. 'user-avatar'. DS. strval($entity->id).'.png';

                $letterAvatar
                    ->setFontFile($fontpath)
                    ->generate(strtoupper($letter), null)
                    ->saveAsPng($filepath);
            }

        }catch (\Exception $exception){

        }

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('contact_number')
            ->maxLength('contact_number', 255)
            ->requirePresence('contact_number', 'create')
            ->notEmptyString('contact_number');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 255)
            ->requirePresence('middle_name', 'create')
            ->notEmptyString('middle_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->requirePresence('is_admin')
            ->notEmptyString('is_admin')
            ->inList('is_admin', [intval(0), intval(1)], 'invalid admin value');

        $validator
            ->requirePresence('is_division')
            ->notEmptyString('is_division')
            ->inList('is_division', [intval(0), intval(1)], 'invalid division value');

        $validator
            ->requirePresence('is_secretary')
            ->notEmptyString('is_secretary')
            ->inList('is_secretary', [intval(0), intval(1)], 'invalid secretary value');

        $validator
            ->requirePresence('is_non_teaching')
            ->notEmptyString('is_non_teaching')
            ->inList('is_non_teaching', [intval(0), intval(1)], 'invalid non teaching value');

        $validator
            ->requirePresence('is_active')
            ->notEmptyString('is_active')
            ->inList('is_active', [intval(0), intval(1)], 'invalid active value');

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->requirePresence('token', 'create')
            ->notEmptyString('token');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'Users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query
            ->join([
                'Profiles' => [
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Profiles.user_id = Users.id',
                    ]
                ],
                'Divisions' => [
                    'table' => 'divisions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Divisions.id = Profiles.division_id',
                    ]
                ],
                'Offices' => [
                    'table' => 'offices',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Offices.id = Profiles.office_id',
                    ]
                ],
            ])
            ->select([
                'division' => 'Divisions.division',
                'division_id' => 'Profiles.division_id',
                'office' => 'Offices.office',
                'office_id' => 'Profiles.office_id'
            ])
            ->where([
            'OR' => [ //<-- we use OR operator for our SQL
                'username' => $options['username'], //<-- username column
                'email' => $options['username'] //<-- email column
            ],
            'is_active =' => intval(1)], [], true)
            ->enableAutoFields(true); // <-- true here means overwrite original query !IMPORTANT.

        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username'], ucwords('this username already registered')), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email'], ucwords('this email already registered')), ['errorField' => 'email']);
        $rules->add($rules->isUnique(['contact_number'], ucwords('this contact number already registered')), ['errorField' => 'contact_number']);

        return $rules;
    }
}
