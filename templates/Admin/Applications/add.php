<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Application $application
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $divisions
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 */
?>

<div class="modal fade" id="modal" data-backdrop="statics">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Supply Name</th>
                                    <th>Unit</th>
                                    <th>Stock</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($application,['id' => 'form', 'type' => 'file', 'class' => 'row']) ?>

        <div class="col-sm-12 col-md-5 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('division_id', ucwords('Division'))?>
                            <?= $this->Form->select('division_id', $divisions,[
                                'class' => 'form-control rounded-0',
                                'id' => 'division-id',
                                'required' => true,
                                'empty' => ucwords('Select Division'),
                                'title' => ucwords('please select a Division')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('office_id', ucwords('office'))?>
                            <?= $this->Form->select('office_id', [],[
                                'class' => 'form-control rounded-0',
                                'id' => 'office-id',
                                'required' => true,
                                'empty' => ucwords('Choose Division'),
                                'title' => ucwords('please select a office')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('purpose', ucwords('Purpose'))?>
                            <?= $this->Form->textarea('purpose',[
                                'class' => 'form-control rounded-0',
                                'id' => 'purpose',
                                'placeholder' => ucwords('Purpose'),
                                'title' => ucwords('please enter a Purpose'),
                                'required' => false
                            ]);?>
                            <small></small>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('user_id',['id' => 'user-id', 'required' => true, 'readonly' => true, 'value' => intval($auth['id'])]);?>
                    <?= $this->Form->hidden('is_approved',[
                        'id' => 'is-approved',
                        'required' => true,
                        'value' => intval(0)
                    ]);?>
                    <?= $this->Form->dateTime('approved_at',[
                        'id' => 'approved-at',
                        'class' => 'd-none'
                    ]);?>
                    <?= $this->Form->hidden('is_declined',[
                        'id' => 'is-declined',
                        'required' => true,
                        'value' => intval(0)
                    ]);?>
                    <?= $this->Form->dateTime('declined_at',[
                        'id' => 'declined-at',
                        'class' => 'd-none'
                    ]);?>
                    <?= $this->Form->hidden('no',[
                        'id' => 'no',
                        'class' => 'd-none',
                        'required' => true,
                        'readonly' => true,
                        'value' => strval($no)
                    ]);?>
                    <?= $this->Form->hidden('remarks',[
                        'id' => 'remarks',
                        'required' => true,
                        'value' => '-'
                    ]);?>
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-success rounded-0',
                        'type' => 'submit'
                    ])?>
                    <?=$this->Form->button('Reset',[
                        'class' => 'btn btn-danger rounded-0',
                        'type' => 'reset'
                    ])?>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-7 col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-10 col-md-10 col-lg-11">
                            <?=$this->Form->label('supply_name', ucwords('Supply Name'))?>
                            <div class="input-group">
                                <?=$this->Form->text('supply_name', [
                                    'class' => 'form-control rounded-0',
                                    'id' => 'supply-name',
                                    'required' => false,
                                    'autocomplete' => 'off',
                                    'pattern' => '(.){1,}',
                                    'placeholder' => ucwords('Supply Name'),
                                    'title' => ucwords('please enter a supply name')
                                ])?>
                                <div class="input-group-append rounded-0 border-info" id="reload-supplies-list">
                                <span class="input-group-text rounded-0 bg-info border-info">
                                    <i class="fas fa-refresh"></i>
                                </span>
                                </div>
                            </div>
                            <small></small>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-1 d-flex justify-content-center align-items-end">
                            <button type="button" id="toggle-modal" title="Show Supply Lists" class="btn btn-lg btn-primary rounded-0">
                                <i class="fas fa-list"></i>
                            </button>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="table-responsive">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Unit</th>
                                        <th>Supply Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody id="application-table">
                                    <tr id="application-table-message">
                                        <td colspan="7" class="text-center">
                                            <strong>Select Supply</strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?= $this->Form->end() ?>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;
        var editor;
        var is_expendables = ['Semi-Expendable', 'Expendable'];
        var in_stock = ['Out of Stock', 'In Stock'];
        var is_in = ['<i class="fas fa-minus"></i>', '<i class="fas fa-plus"></i>'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[500, 1000, 2000, 3000],
            ajax:{
                url:mainurl+'supplies/getSupplies',
                official: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [5],
                    orderable: false,
                    searchable:false,
                    data: null,
                    render: function ( data,type,row,meta) {
                        return '<div class="icheck-primary">' +
                            '<input type="checkbox" id="supply-'+(parseInt(row.id))+'" data-id="'+(parseInt(row.id))+'">' +
                            '<label for="supply-'+(parseInt(row.id))+'"></label>' +
                            '</div>';
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'supply_name'},
                { data: 'unit.unit'},
                { data: 'stock'},
                { data: 'price'},
                { data: 'id'},
            ]
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(baseurl,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#division-id').change(function (e) {
            var value = parseInt($(this).val());
            getOfficesList(parseInt(value));
        });

        $('#reload-supplies-list').click(function (e) {
            getSuppliesList();
        });

        $('#toggle-modal').click(function (e) {
            $('#modal').modal('toggle');
        });

        table.on('click' ,'input[type="checkbox"]', function (e) {
            var checked = $(this).prop('checked');
            var dataId = $(this).attr('data-id');
            var row = $('#row-'+(parseInt(dataId))+'').length;
            if(parseInt(Number(checked)) && !parseInt(row)){
                getSupply(parseInt(dataId));
            }else{
                $('#row-'+(parseInt(dataId))+'').remove();
            }
            return true;
        });

        $(document).on('click', 'button.button-remove', function (e) {
            var dataId = $(this).attr('data-id');
            $('#row-'+(parseInt(dataId))+'').remove();
            inputs();
        });

        $(document).on('keypress', 'input[type="number"]', function (e) {
            var key = e.key;
            var regex = /^([0-9\.])$/;
            if(!key.match(regex)){
                e.preventDefault();
            }
        });

        $(document).on('input', '.application-supplies-quantity', function (e) {
            var dataId = $(this).attr('data-id');
            var value = $(this).val();
            var regex = /^([0-9\.]){1,}$/;
            var price = $('#application-supplies-'+(parseInt(dataId))+'-price').val();
            var total = $('#application-supplies-'+(parseInt(dataId))+'-total');
            var result = (parseInt(value) * parseFloat(price)).toFixed(2);

            if(value.match(regex)){
                $(this).removeClass('is-invalid').next('small').empty();
                $('#application-total-text-'+(parseInt(dataId))+'').text((new Intl.NumberFormat().format(parseFloat(result))));
                total.val(result);
                return true;
            }

            $(this).addClass('is-invalid').next('small').text('Please Enter A Quantity');
        }).on('blur', '.application-supplies-quantity', function () {
            var value = $(this).val();

            if(!parseInt(value.length)){
                $(this).val(parseInt(1)).removeClass('is-invalid').next('small').empty();
            }

        });

        CKEDITOR.ClassicEditor.create(document.getElementById('purpose'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Purpose',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function getOfficesList(divisionId) {
            $.ajax({
                url: mainurl+'offices/getOfficesList/'+(parseInt(divisionId)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#office-id').prop('disabled', true).empty().append('<option value="">Please Wait</option>');
                },
            }).done(function (data, status, xhr) {
                $('#office-id').prop('disabled', false).empty().append('<option value="">Select Office</option>');
                $.map(data, function (data, key) {
                    $('#office-id').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Offices');
            });
        }

        function getSuppliesList() {
            $.ajax({
                url:mainurl+'supplies/getSuppliesList',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#supply-name').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#supply-name').autocomplete({
                    delay: 100,
                    minLength: 2,
                    autoFocus: true,
                    source: function (request, response) {
                        response($.map(data, function (data, key) {
                            var name = data.supply_name.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    key: parseInt(data.id),
                                    id: parseInt(data.id),
                                    label: data.supply_name,
                                    price: parseFloat(data.price),
                                    supply_code: data.supply_code,
                                    supply_name: data.supply_name,
                                    unit:{
                                        unit: data.unit.unit
                                    }
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    create: function (event, ui) {
                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                            return $('<li></li>')
                                .data('item.autocomplete', item)
                                .append('<li>'+(label)+'</li>')
                                .appendTo(ul);
                        };
                    },
                    focus: function(e, ui) {
                        e.preventDefault();
                    },
                    select: function(e, ui) {
                        var row = $('#row-'+(parseInt(ui.item.id))+'').length;
                        if(parseInt(row)){
                            $('#row-'+(parseInt(ui.item.id))+'').remove();
                        }
                        $('#application-table-message').fadeOut(100);
                        tbody(ui.item);
                    },
                    change: function(e, ui ) {
                        e.preventDefault();
                    },
                }).prop('disabled', false);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function getSupply(id) {
            $.ajax({
                url:mainurl+'supplies/getSupply/'+(parseInt(id)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#application-table-message').fadeOut(100);
                },
            }).done(function (data, status, xhr) {
                tbody(data);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Supply');
            });
        }

        function tbody(data) {
            var tr = '<tr id="row-'+(parseInt(data.id))+'"> ' +
                '<td class="application-details-no text-center"></td> ' +
                '<td>'+(data.unit.unit)+'</td> ' +
                '<td>'+(data.supply_name)+'<input type="hidden" name="" value="'+(parseInt(data.id))+'" class="application-supplies-supply-id" id="application-supplies-'+(parseInt(data.id))+'-supply-id" data-id="'+(parseInt(data.id))+'" required readonly></td>' +
                '<td>'+(new Intl.NumberFormat().format(parseFloat(data.price)))+'<input type="hidden" name="" value="'+(parseFloat(data.price))+'" class="application-supplies-price" id="application-supplies-'+(parseInt(data.id))+'-price" data-id="'+(parseInt(data.id))+'" required readonly></td></td> ' +
                '<td><input type="number" min="1" name="" value="'+(parseFloat(1))+'" step="any" class="application-supplies-quantity form-control form-control-border form-control-sm" id="application-supplies-'+(parseInt(data.id))+'-quantity" data-id="'+(parseInt(data.id))+'" required><small></small></td> ' +
                '<td><span id="application-total-text-'+(parseInt(data.id))+'">'+(new Intl.NumberFormat().format(parseFloat(data.price)))+'</span><input type="hidden" name="" value="'+(parseFloat(data.price))+'" class="application-supplies-total" id="application-supplies-'+(parseInt(data.id))+'-total" data-id="'+(parseInt(data.id))+'" required readonly></td> ' +
                '<td><button type="button" class="btn btn-danger btn-sm rounded-0 button-remove" data-id="'+(parseInt(data.id))+'"><i class="fa fa-times"></i></button></td> ' +
                '</tr>';
            $('#application-table').append(tr);
            inputs();
        }

        function inputs() {
            var counter = 0;
            var tr = document.getElementsByClassName('application-details-no');
            var supplyId = document.getElementsByClassName('application-supplies-supply-id');
            var price = document.getElementsByClassName('application-supplies-price');
            var quantity = document.getElementsByClassName('application-supplies-quantity');
            var total = document.getElementsByClassName('application-supplies-total');

            if(parseInt(tr.length)){
                $.map(supplyId, function (data, key) {
                    counter++;
                    tr[parseInt(key)].innerHTML = (parseInt(counter)).toString();
                    supplyId[parseInt(key)].name = 'application_supplies['+(parseInt(key))+'][supply_id]';
                    price[parseInt(key)].name = 'application_supplies['+(parseInt(key))+'][price]';
                    quantity[parseInt(key)].name = 'application_supplies['+(parseInt(key))+'][quantity]';
                    total[parseInt(key)].name = 'application_supplies['+(parseInt(key))+'][total]';
                });
            }else{
                $('#application-table-message').fadeIn(100);
            }

        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

        getSuppliesList();

    });
</script>
