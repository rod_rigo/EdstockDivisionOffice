<?php
/**
 * @var \App\View\AppView
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-users"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number" id="total-users">0</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-building"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Divisions</span>
                <span class="info-box-number" id="total-divisions">0</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-truck"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Suppliers</span>
                <span class="info-box-number" id="total-suppliers">0</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-pencil-alt"></i>
                            </span>
                            <h5 class="description-header" id="total-requests">0</h5>
                            <span class="description-text">TOTAL REQUESTS</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-shopping-cart"></i>
                            </span>
                            <h5 class="description-header" id="total-orders">0</h5>
                            <span class="description-text">TOTAL ORDERS</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-search"></i>
                            </span>
                            <h5 class="description-header" id="total-inspections">0</h5>
                            <span class="description-text">TOTAL INSPECTIONS</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="description-block">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-file-text"></i>
                            </span>
                            <h5 class="description-header" id="total-requisitions">0</h5>
                            <span class="description-text">TOTAL REQUISITIONS</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-3 col-md-4 col-lg-4">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-files-o"></i>
                            </span>
                            <h5 class="description-header" id="total-applications">0</h5>
                            <span class="description-text">TOTAL APPLICATIONS</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-4 col-lg-4">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-file-o"></i>
                            </span>
                            <h5 class="description-header" id="total-ris">0</h5>
                            <span class="description-text">TOTAL RIS</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-4 col-lg-4">
                        <div class="description-block border-right">
                            <span class="description-percentage text-dark">
                                <i class="fas fa-file-o"></i>
                            </span>
                            <h5 class="description-header" id="total-ics">0</h5>
                            <span class="description-text">TOTAL ICS</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-7">
        <div class="card"  style="height: 35em !important;">
            <div class="card-header">
                <h5 class="card-title">Division Supplies</h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-tool text-white bg-primary rounded-0" data-toggle="dropdown">
                            <i class="fas fa-download"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#supply-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#supply-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#supply-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#supply-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <canvas id="supply-chart" height="320" width="700"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-5">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card" style="height: 35em !important;">
                    <div class="card-header">
                        <h3 class="card-title">
                            Procurement Requests
                        </h3>
                        <div class="card-tools">
                            <ul class="nav nav-pills ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#procurement-day" data-toggle="tab">Day</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#procurement-week" data-toggle="tab">Week</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#procurement-month" data-toggle="tab">Month</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#procurement-year" data-toggle="tab">Year</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content p-0">
                            <div class="tab-pane active" id="procurement-day">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-success">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Approved)</span>
                                                <span class="info-box-number" id="requests-day-approved">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-danger">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Declined)</span>
                                                <span class="info-box-number" id="requests-day-declined">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-warning">
                                            <span class="info-box-icon">
                                                <i class="fas fa-shopping-cart"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Orders</span>
                                                <span class="info-box-number" id="orders-day">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-info">
                                            <span class="info-box-icon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Inspections</span>
                                                <span class="info-box-number" id="inspections-day">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-secondary">
                                            <span class="info-box-icon">
                                                <i class="fas fa-file-text"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requisitions</span>
                                                <span class="info-box-number" id="requisitions-day">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="procurement-week">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-success">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Approved)</span>
                                                <span class="info-box-number" id="requests-week-approved">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-danger">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Declined)</span>
                                                <span class="info-box-number" id="requests-week-declined">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-warning">
                                            <span class="info-box-icon">
                                                <i class="fas fa-shopping-cart"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Orders</span>
                                                <span class="info-box-number" id="orders-week">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-info">
                                            <span class="info-box-icon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Inspections</span>
                                                <span class="info-box-number" id="inspections-week">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-secondary">
                                            <span class="info-box-icon">
                                                <i class="fas fa-file-text"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requisitions</span>
                                                <span class="info-box-number" id="requisitions-week">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="procurement-month">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-success">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Approved)</span>
                                                <span class="info-box-number" id="requests-month-approved">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-danger">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Declined)</span>
                                                <span class="info-box-number" id="requests-month-declined">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-warning">
                                            <span class="info-box-icon">
                                                <i class="fas fa-shopping-cart"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Orders</span>
                                                <span class="info-box-number" id="orders-month">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-info">
                                            <span class="info-box-icon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Inspections</span>
                                                <span class="info-box-number" id="inspections-month">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-secondary">
                                            <span class="info-box-icon">
                                                <i class="fas fa-file-text"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requisitions</span>
                                                <span class="info-box-number" id="requisitions-month">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="procurement-year">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-success">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Approved)</span>
                                                <span class="info-box-number" id="requests-year-approved">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="info-box mb-3 bg-danger">
                                            <span class="info-box-icon">
                                                <i class="fas fa-pencil-alt"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requests (Declined)</span>
                                                <span class="info-box-number" id="requests-year-declined">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-warning">
                                            <span class="info-box-icon">
                                                <i class="fas fa-shopping-cart"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Orders</span>
                                                <span class="info-box-number" id="orders-year">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-info">
                                            <span class="info-box-icon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Inspections</span>
                                                <span class="info-box-number" id="inspections-year">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 col-lg-12">
                                        <div class="info-box mb-3 bg-secondary">
                                            <span class="info-box-icon">
                                                <i class="fas fa-file-text"></i>
                                            </span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Requisitions</span>
                                                <span class="info-box-number" id="requisitions-year">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-7">
        <div class="card"  style="height: 35em !important;">
            <div class="card-header">
                <h5 class="card-title">Application Phase</h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-tool text-white bg-primary rounded-0" data-toggle="dropdown">
                            <i class="fas fa-download"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#application-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#application-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#application-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#application-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <canvas id="application-chart" height="320" width="700"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-5">
        <div class="card"  style="height: 35em !important;">
            <div class="card-header">
                <h5 class="card-title">Series Methods</h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-tool text-white bg-primary rounded-0" data-toggle="dropdown">
                            <i class="fas fa-download"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#serieses-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#serieses-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#serieses-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#serieses-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <canvas id="serieses-chart" height="320" width="700"></canvas>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = mainurl+'dashboards/';

        const autocolors = window['chartjs-plugin-autocolors'];
        Chart.register(autocolors);

        var supply_canvas = document.querySelector('#supply-chart').getContext('2d');
        var supply_chart;

        function getSupplies() {
            var array = {
                'data':[],
                'label':[]
            };
            $.ajax({
                url:baseurl+'getSupplies',
                method: 'GET',
                type:'GET',
                dataType:'JSON',
                async: false,
                global: false,
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $.map(data,function (data) {
                    array.data.push(parseFloat(data.count).toFixed(2));
                    array.label.push((data.division.division).toUpperCase());
                });
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
            return array;
        }

        function getSuppliesChart() {
            supply_chart =  new Chart(supply_canvas, {
                type: 'bar',
                data: {
                    labels:getSupplies().label,
                    datasets: [
                        {
                            label:'No Of Supplies',
                            data: getSupplies().data
                        }
                    ]
                },
                options: {
                    indexAxis:'x',
                    responsive: true,
                    maintainAspectRatio: false,
                    plugins: {
                        autocolors: {
                            mode: 'data',
                            offset: 5
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        subtitle: {
                            display: false,
                            text: ''
                        },
                        legend: {
                            display:true,
                            onClick: function (evt, legendItem, legend) {
                                const index = legend.chart.data.labels.indexOf(legendItem.text);
                                legend.chart.toggleDataVisibility(index);
                                legend.chart.update();
                            },
                            labels:{
                                generateLabels: function (chart) {
                                    return  $.map(chart.data.labels, function( label, index ) {
                                        return {
                                            text:label,
                                            strokeStyle: chart.data.datasets[0].borderColor[index],
                                            fillStyle: chart.data.datasets[0].backgroundColor[index],
                                            hidden: !chart.getDataVisibility(index)
                                        };
                                    });
                                }
                            }
                        }
                    }
                },
                plugins:[
                    {
                        id: null,
                        beforeDraw: function (chart) {
                            const ctx = chart.canvas.getContext('2d');
                            ctx.save();
                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, chart.width, chart.height);
                            ctx.restore();
                        }
                    }
                ]
            });
        }

        var application_canvas = document.querySelector('#application-chart').getContext('2d');
        var application_chart;

        function getApplications() {
            var array = {
                'data':[],
                'label':[]
            };
            $.ajax({
                url:baseurl+'divisionApplications',
                method: 'GET',
                type:'GET',
                dataType:'JSON',
                async: false,
                global: false,
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $.map(data,function (data) {
                    array.data.push(parseFloat(data.total).toFixed(2));
                    array.label.push((data.month).toUpperCase());
                });
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
            return array;
        }

        function getApplicationsChart() {
            application_chart =  new Chart(application_canvas, {
                type: 'bar',
                data: {
                    labels:getApplications().label,
                    datasets: [
                        {
                            label:'Month Of Applications',
                            data: getApplications().data
                        }
                    ]
                },
                options: {
                    indexAxis:'x',
                    responsive: true,
                    maintainAspectRatio: false,
                    plugins: {
                        autocolors: {
                            mode: 'data',
                            offset: 5
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        subtitle: {
                            display: false,
                            text: ''
                        },
                        legend: {
                            display:true,
                            onClick: function (evt, legendItem, legend) {
                                const index = legend.chart.data.labels.indexOf(legendItem.text);
                                legend.chart.toggleDataVisibility(index);
                                legend.chart.update();
                            },
                            labels:{
                                generateLabels: function (chart) {
                                    return  $.map(chart.data.labels, function( label, index ) {
                                        return {
                                            text:label,
                                            strokeStyle: chart.data.datasets[0].borderColor[index],
                                            fillStyle: chart.data.datasets[0].backgroundColor[index],
                                            hidden: !chart.getDataVisibility(index)
                                        };
                                    });
                                }
                            }
                        }
                    }
                },
                plugins:[
                    {
                        id: null,
                        beforeDraw: function (chart) {
                            const ctx = chart.canvas.getContext('2d');
                            ctx.save();
                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, chart.width, chart.height);
                            ctx.restore();
                        }
                    }
                ]
            });
        }

        var serieses_canvas = document.querySelector('#serieses-chart').getContext('2d');
        var serieses_chart;

        function getSesrieses() {
            var array = {
                'data':[],
                'label':[]
            };
            $.ajax({
                url:baseurl+'getSerieses',
                method: 'GET',
                type:'GET',
                dataType:'JSON',
                async: false,
                global: false,
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $.map(data,function (data) {
                    array.data.push(parseFloat(data.total).toFixed(2));
                    array.label.push((data.label).toUpperCase());
                });
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
            return array;
        }

        function getSesriesesChart() {
            serieses_chart =  new Chart(serieses_canvas, {
                type: 'bar',
                data: {
                    labels:getSesrieses().label,
                    datasets: [
                        {
                            label:'Series Of Applications',
                            data: getSesrieses().data
                        }
                    ]
                },
                options: {
                    indexAxis:'x',
                    responsive: true,
                    maintainAspectRatio: false,
                    plugins: {
                        autocolors: {
                            mode: 'data',
                            offset: 5
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        subtitle: {
                            display: false,
                            text: ''
                        },
                        legend: {
                            display:true,
                            onClick: function (evt, legendItem, legend) {
                                const index = legend.chart.data.labels.indexOf(legendItem.text);
                                legend.chart.toggleDataVisibility(index);
                                legend.chart.update();
                            },
                            labels:{
                                generateLabels: function (chart) {
                                    return  $.map(chart.data.labels, function( label, index ) {
                                        return {
                                            text:label,
                                            strokeStyle: chart.data.datasets[0].borderColor[index],
                                            fillStyle: chart.data.datasets[0].backgroundColor[index],
                                            hidden: !chart.getDataVisibility(index)
                                        };
                                    });
                                }
                            }
                        }
                    }
                },
                plugins:[
                    {
                        id: null,
                        beforeDraw: function (chart) {
                            const ctx = chart.canvas.getContext('2d');
                            ctx.save();
                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, chart.width, chart.height);
                            ctx.restore();
                        }
                    }
                ]
            });
        }

        function counts() {
            $.ajax({
                url: baseurl+'counts',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                   $('#total-users, #total-divisions, #total-suppliers').text(parseInt(0));
                },
            }).done(function (data, status, xhr) {
                $('#total-users').text(parseInt(data.users));
                $('#total-divisions').text(parseInt(data.divisions));
                $('#total-suppliers').text(parseInt(data.suppliers));
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
        }

        function procurements() {
            $.ajax({
                url: baseurl+'procurements',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#total-requests, #total-orders, #total-inspections, #total-requisitions').text(parseInt(0));
                },
            }).done(function (data, status, xhr) {
                $('#total-requests').text(parseInt(data.requests));
                $('#total-orders').text(parseInt(data.orders));
                $('#total-inspections').text(parseInt(data.inspections));
                $('#total-requisitions').text(parseInt(data.requisitions));
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
        }

        function getProcurementsIntervals() {
            $.ajax({
                url: baseurl+'getProcurementsIntervals',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#requests-day-approved, #requests-day-declined, #orders-day, #inspections-day, #requisitions-day').text(parseInt(0));
                    $('#requests-week-approved, #requests-week-declined, #orders-week, #inspections-week, #requisitions-week').text(parseInt(0));
                    $('#requests-month-approved, #requests-month-declined, #orders-month, #inspections-month, #requisitions-month').text(parseInt(0));
                    $('#requests-year-approved, #requests-year-declined, #orders-year, #inspections-year, #requisitions-year').text(parseInt(0));
                },
            }).done(function (data, status, xhr) {
                $('#requests-day-approved').text(parseInt(data.today.requests_approved));
                $('#requests-day-declined').text(parseInt(data.today.requests_declined));
                $('#orders-day').text(parseInt(data.today.orders));
                $('#inspections-day').text(parseInt(data.today.inspections));
                $('#requisitions-day').text(parseInt(data.today.requisitions));

                $('#requests-week-approved').text(parseInt(data.week.requests_approved));
                $('#requests-week-declined').text(parseInt(data.week.requests_declined));
                $('#orders-week').text(parseInt(data.week.orders));
                $('#inspections-week').text(parseInt(data.week.inspections));
                $('#requisitions-week').text(parseInt(data.week.requisitions));

                $('#requests-month-approved').text(parseInt(data.month.requests_approved));
                $('#requests-month-declined').text(parseInt(data.month.requests_declined));
                $('#orders-month').text(parseInt(data.month.orders));
                $('#inspections-month').text(parseInt(data.month.inspections));
                $('#requisitions-month').text(parseInt(data.month.requisitions));

                $('#requests-year-approved').text(parseInt(data.year.requests_approved));
                $('#requests-year-declined').text(parseInt(data.year.requests_declined));
                $('#orders-year').text(parseInt(data.year.orders));
                $('#inspections-year').text(parseInt(data.year.inspections));
                $('#requisitions-year').text(parseInt(data.year.requisitions));
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
        }

        function applications() {
            $.ajax({
                url: baseurl+'applications',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#total-applications, #total-ics, #total-ris').text(parseInt(0));
                },
            }).done(function (data, status, xhr) {
                $('#total-applications').text(parseInt(data.applications));
                $('#total-ics').text(parseInt(data.ics));
                $('#total-ris').text(parseInt(data.ris));
            }).fail(function (data, status, xhr) {
                swal('warning', null, data.responseJSON.message);
            });
        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                toast: true,
                position: 'top-right',
                showConfirmButton: false,
                timerProgressBar: true,
            });
        }

        counts();
        procurements();
        getSuppliesChart();
        getApplicationsChart();
        getSesriesesChart();
        getProcurementsIntervals();
        applications();

        setInterval(function () {
            supply_chart.destroy();
            application_chart.destroy();
            serieses_chart.destroy();
            getSuppliesChart();
            getApplicationsChart();
            getSesriesesChart();
            counts();
            procurements();
            getProcurementsIntervals();
            applications();
        }, (parseInt(60 * 60 * 1000)));
    });
    $(document).ready(function (e) {
        $('.image').click(function (e) {
            var dataTarget = $(this).attr('data-target');
            Swal.fire({
                title: 'Export To Image',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                input: 'text',
                inputValidator: function (value) {
                    if (!value) {
                        return 'Please Enter A Filename';
                    }
                },
                inputAttributes: {
                    placeholder: 'Enter A Filename',
                    id: 'filename'
                }
            }).then(function (result) {
                if (result.isConfirmed) {
                    document.querySelector(dataTarget).toBlob(function(blob) {
                        saveAs(blob, ($('#filename').val())+'.png');
                    });
                }
            });
        });

        $('.print').click(function (e) {
            var dataTarget = $(this).attr('data-target');
            var chart =  document.querySelector(dataTarget);
            Swal.fire({
                title: 'Print Canvas',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    printJS({
                        printable: chart.toDataURL('image/png',1.0),
                        type: 'image',
                        imageStyle: 'width:100%;height:90vh;margin-bottom:20px;',
                        style: '@page { size: Letter landscape; }'
                    });
                }
            });
        });

        $('.pdf').click(function (e) {
            var dataTarget = $(this).attr('data-target');
            var canvas = document.querySelector(dataTarget);
            var chart = Chart.getChart(canvas);
            Swal.fire({
                title: 'Export To PDF',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    var  row = [
                        [
                            {text:'header'},
                            {text:'header'}
                        ]
                    ];

                    $.map(chart.config.data.labels,function (value, key) {
                        row.push([
                            { text : chart.config.data.labels[(key)], colspan : 1 },
                            { text : chart.config.data.datasets[0].data[(key)], colspan : 1 }
                        ]);
                    });

                    var docDefinition = {
                        pageOrientation: 'landscape',
                        pageMargins: [ 10, 10, 10, 10 ],
                        content: [
                            {
                                text: null,
                                fontSize: 15,
                                width:100
                            },
                            {
                                columns:[
                                    {
                                        image: canvas.toDataURL('image/png',1.0),
                                        width:500,
                                        height: 500
                                    },
                                    {
                                        width: 297.6377952755905,
                                        margin:[10,50],
                                        table: {
                                            headerRows: 2,
                                            widths: [ 100, 100 ],
                                            heights: [
                                                20
                                            ],
                                            body: row
                                        }
                                    }
                                ]
                            }
                        ]
                    };

                    pdfMake.createPdf(docDefinition).open();
                }
            });
        });

        $('.excel').click(function (e) {
            var dataTarget = $(this).attr('data-target');
            var canvas = document.querySelector(dataTarget);
            var chart = Chart.getChart(canvas);
            Swal.fire({
                title: 'Export To Excel',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                input: 'text',
                inputValidator: function (value) {
                    if (!value) {
                        return 'Please Enter A Filename';
                    }
                },
                inputAttributes: {
                    placeholder: 'Enter A Filename',
                    id: 'filename'
                }
            }).then(function (result) {
                if (result.isConfirmed) {
                    const workbook = new ExcelJS.Workbook();

                    workbook.creator = '';
                    workbook.lastModifiedBy = '';
                    workbook.created = new Date();
                    workbook.modified = new Date();
                    workbook.lastPrinted = new Date();

                    const worksheet = workbook.addWorksheet('New Sheet');
                    worksheet.columns = [
                        { header: 'Value', key: 'value' },
                        { header: 'Data', key: 'data' }
                    ];

                    // add image to workbook by base64
                    const image = canvas.toDataURL('image/png',1.0);
                    const imageId2 = workbook.addImage({
                        base64: image,
                        extension: 'png'
                    });
                    worksheet.addImage(imageId2, {
                        tl: { col: 3, row: 0 },
                        ext: { width: 1000, height: 1000 }
                    });

                    var rows = [];

                    for(var i = 0; i < chart.config.data.labels.length; i++){
                        rows.push({
                            value : chart.config.data.labels[i],
                            data : chart.config.data.datasets[0].data[i]
                        });
                    }

                    worksheet.addRows(rows);

                    workbook.xlsx.writeBuffer().then(function (data) {
                        const blob = new Blob([data], {
                            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
                        });
                        saveAs(blob, ($('#filename').val())+'.xlsx');
                    });
                }
            });
        });
    });
</script>
