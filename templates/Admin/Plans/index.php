<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FundCluster[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Plans', 'action' => 'add'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="New Plan">
            New Plan
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Plan</th>
                        <th>Division</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'plans/';
        var url = '';
        var isActives = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];
        var status = ['Inactive', 'Active'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getPlans',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [3],
                    data: null,
                    render: function(data,type,row,meta){
                        return isActives[parseInt(row.is_active)];
                    }
                },
                {
                    targets: [5],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [6],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        var buttons = [
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-warning rounded-0 text-white is-active" is-active="1" title="Inactive">Inactive</a> | ',
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white is-active" is-active="0" title="Active">Active</a> | '
                        ];
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                            (buttons[parseInt(row.is_active)])+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'plan'},
                { data: 'division.division'},
                { data: 'is_active'},
                { data: 'user.username'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.view',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'view/'+(parseInt(dataId));
            Swal.fire({
                title: 'View Data',
                text: 'Open In New Window?',
                icon: 'question',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                denyButtonColor: '#6e7881',
                cancelButtonText: 'No',
                denyButtonText: 'Cancel',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                    return true;
                }else if(result.isDenied){
                    Swal.close();
                    return true;
                }
                Turbolinks.visit(href,{ action:'advance' });
            });
        });
        
        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        datatable.on('click','.is-active',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var isActive = $(this).attr('is-active');
            var href = baseurl+'isActive/'+(parseInt(dataId))+'/'+(parseInt(isActive));
            Swal.fire({
                title: 'Mark As '+(status[parseInt(isActive)])+'',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
