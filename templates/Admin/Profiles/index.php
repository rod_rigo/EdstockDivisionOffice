<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Users', 'action' => 'profile'])?>" turbolink class="btn btn-primary rounded-0" title="New User">
            New Profile
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Contact No.</th>
                        <th>Division</th>
                        <th>Office</th>
                        <th>Is Division</th>
                        <th>Is Secretary</th>
                        <th>Is Non-Teaching</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'profiles/';
        var url = '';
        var isDivision = [
            '<span class="text-danger"><i class="fas fa-times"></i> No</span>',
            '<span class="text-success"><i class="fas fa-check"></i> Yes</span>',
        ];
        var isSecretary = [
            '<span class="text-danger"><i class="fas fa-times"></i> No</span>',
            '<span class="text-success"><i class="fas fa-check"></i> Yes</span>',
        ];
        var isNonTeaching = [
            '<span class="text-danger"><i class="fas fa-times"></i> No</span>',
            '<span class="text-success"><i class="fas fa-check"></i> Yes</span>',
        ];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getProfiles',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [3],
                    data: null,
                    render: function(data,type,row,meta){
                        return (row.user.first_name)+' '+(row.user.middle_name)+' '+(row.user.last_name);
                    }
                },
                {
                    targets: [7],
                    data: null,
                    render: function(data,type,row,meta){
                        return isDivision[parseInt(row.user.is_division)];
                    }
                },
                {
                    targets: [8],
                    data: null,
                    render: function(data,type,row,meta){
                        return isSecretary[parseInt(row.user.is_secretary)];
                    }
                },
                {
                    targets: [9],
                    data: null,
                    render: function(data,type,row,meta){
                        return isNonTeaching[parseInt(row.user.is_non_teaching)];
                    }
                },
                {
                    targets: [10],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [11],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.user_id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'user.username'},
                { data: 'user.email'},
                { data: 'user.first_name'},
                { data: 'user.contact_number'},
                { data: 'division.division'},
                { data: 'office.office'},
                { data: 'user.is_division'},
                { data: 'user.is_secretary'},
                { data: 'user.is_non_teaching'},
                { data: 'user.modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = mainurl+'users/update/'+(dataId);
            Turbolinks.visit(href, { action: 'advance'});
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
