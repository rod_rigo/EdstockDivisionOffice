<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$counter = 0;
$start = 6;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator($auth['username'])
    ->setTitle(ucwords('PR').strval($request->no))
    ->setSubject(ucwords('Procurement Requests'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', ucwords('Entity Name'))
    ->setCellValue('B1', (ucwords($request->division->division)))
    ->setCellValue('D1', ucwords('Fund Cluster'))
    ->setCellValue('E1', (ucwords($request->fund_cluster->fund_cluster)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A3', ucwords('Office/Section'))
    ->setCellValue('B3', (ucwords($request->office->office)))
    ->setCellValue('E3', (ucwords('PR No')))
    ->setCellValue('F3', (strval($request->no)))
    ->setCellValue('F4', (ucwords('Responsibility Code Center')))
    ->setCellValue('H3', (ucwords('Date')))
    ->setCellValue('I3', (strval(((new DateTime($request->created))->format('Y-M-d')))));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A6', ucwords('Stock/Property No.'))
    ->setCellValue('B6', ucwords('Unit'))
    ->setCellValue('C6', ucwords('Item Description'))
    ->setCellValue('D6', ucwords('Quantity'))
    ->setCellValue('E6', ucwords('Unit Cost'))
    ->setCellValue('F6', ucwords('Total Cost'));

foreach ($request->request_supplies as $key => $request_supply) {
    $counter++;
    $start++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.(strval($start)), intval($counter))
        ->setCellValue('B'.(strval($start)), $request_supply->supply->unit->unit)
        ->setCellValue('C'.(strval($start)), $request_supply->supply->supply_name)
        ->setCellValue('D'.(strval($start)), number_format(doubleval($request_supply->quantity),2))
        ->setCellValue('E'.(strval($start)), number_format(doubleval($request_supply->price),2))
        ->setCellValue('F'.(strval($start)), number_format(doubleval($request_supply->total),2));
}
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('E'.(strval($start)), strtoupper('Total'))
    ->setCellValue('F'.(strval($start)), number_format(doubleval($request->total),2));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Nothing Follows'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), strtoupper('Purpose'))
    ->setCellValue('B'.(strval($start)), (strip_tags($request->purpose)));

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), ucwords('Requested By'))
    ->setCellValue('D'.(strval($start)), ucwords('Approved'));

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Signature'))
    ->setCellValue('B'.(strval($start)), null)
    ->setCellValue('C'.(strval($start)), null)
    ->setCellValue('D'.(strval($start)), null);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Printed Name'))
    ->setCellValue('B'.(strval($start)), ucwords('Requester Name'))
    ->setCellValue('C'.(strval($start)), null)
    ->setCellValue('D'.(strval($start)), ucwords('Official Name'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Designation'))
    ->setCellValue('B'.(strval($start)), ucwords('Requester Position'))
    ->setCellValue('C'.(strval($start)), null)
    ->setCellValue('D'.(strval($start)), ucwords('Official Position'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Date'))
    ->setCellValue('B'.(strval($start)), (strval(((new DateTime($request->created))->format('Y-M-d')))))
    ->setCellValue('C'.(strval($start)), null)
    ->setCellValue('D'.(strval($start)), (strval(((new DateTime($request->created))->format('Y-M-d')))));

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('PR-'.strval($request->no).'.xlsx').'"');
$writer->save('php://output');
exit(0);