<?php

$checked = WWW_ROOT. 'img'. DS. 'check.png';

$file_info = new finfo(FILEINFO_MIME_TYPE);
$mime_type = $file_info->buffer(file_get_contents($checked));
$image = 'data:'.(strtolower($mime_type)).';base64,'.(base64_encode(file_get_contents($checked)));

class xtcpdf extends TCPDF {
    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 9);
        $this->Cell(0, 0, 'Appendix 63', 0, 01, 'R');
        $this->Ln(5);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'REQUISITION AND ISSUE SLIP', 0, 0, 'C');
        $this->Ln(6);
    }
}

$pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->SetMargins(10, 35, 5, true);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('times','',12);
$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$data = '';
$counter = 0;
$total = 0;
foreach ($requisition->requisition_supplies as $requisition_supply){
    $counter++;
    $data .='<tr>
                <td align="center">'.(intval($counter)).'</td>
                <td align="center">'.(ucwords($requisition_supply->supply->unit->unit)).'</td>
                <td align="center">'.(ucwords($requisition_supply->supply->supply_name)).'</td>
                <td align="center">'.(number_format(intval($requisition_supply->quantity))).'</td>
                <td align="center"><img src="'.($image).'" width="10" height="10" alt=""></td>
                <td align="center"></td>
                <td align="center">'.(number_format(doubleval($requisition_supply->quantity))).'</td>
                <td align="center"></td>
                </tr>
    ';

    $total += doubleval($requisition_supply->total);
}

$html = '<table width="100%">
    <tr>
        <td width="60%"><p>Entity Name: <b>'.(ucwords($requisition->office->office)).'</b></p></td>
        <td width="1%"></td>
        <td width="35%"><p>Fund Cluster: <b>'.(ucwords($requisition->fund_cluster->fund_cluster)).'</b></p></td>
    </tr>
</table>
<table width="100%" cellpadding="2" style="border-collapse: collapse; border: 1px solid black;">
    <tr>
        <td width="58%" style="font-size: small;">Division: <b><u>'.(ucwords($requisition->division->division)).'</u></b></td>
        <td width="41%" align="left" style="font-size: small; border-left: 1px solid black;">Responsibility Center Code:</td>
    </tr>
    <tr>
        <td style="font-size: small;">Office:</td>
        <td style="font-size: small; border-left: 1px solid black;" >RIS No: <b><u>'.(strval($requisition->no)).'</u></b></td>
    </tr>
</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
<tr>
    <td style="font-size: small;" width="49%" align="center"><b><i>Requisition</i></b></td>
    <td style="font-size: small;" width="18%" align="center"><b><i>Stock Available?</i></b></td>
    <td style="font-size: small;" width="32%" align="center"><b><i>Issue</i></b></td>
</tr>
</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
        <th width="7%" align="center">Stock No.</th>
        <th width="5%" align="center">Unit</th>
        <th width="25%" align="center">Item Description</th>
        <th width="12%" align="center">Quantity</th>
        <th width="9%" align="center">Yes</th>
        <th width="9%" align="center">No</th>
        <th width="13%" align="center">Quantity</th>
        <th width="19%" align="center">Remarks</th>
    </tr>
    '.$data.'
    <tr>
        <td colspan="8" align="center">Nothing Follows</td>
    </tr>
     <tr>
         <td colspan="8">Purpose:
            <p align="center"><u>'.(strip_tags($requisition->request->purpose)).'</u></p>
         </td>
    </tr>
    
</table>
<table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
<tr>
        <td width="12%"></td>
        <td width="25%" style="font-size: small; border-left: 1px solid black;"><p>Requested By: <b></b></p></td>
        <td width="21%" style="font-size: small; border-left: 1px solid black;"><p>Approved By: <b></b></p></td>
        <td width="22%" style="font-size: small; border-left: 1px solid black;"><p>Issued By: <b></b></p></td>
        <td width="19%" style="font-size: small; border-left: 1px solid black;"><p>Received By: <b></b></p></td>
    </tr>
<tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Signature:</td>
    <td width="25%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="21%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="22%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="19%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td></td>
</tr>
<tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Printed Name:</td>
    <td width="25%" align="center" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>Coordinator Name</b></td>
    <td width="21%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>FLORDELIZA C. GECOBE PHD, CESO V</b></td>
    <td width="22%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>Requester Name</b></td>
    <td width="19%" align="center" style="font-size: 7px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>Coordinator Name</b></td>
</tr>
<tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Designation:</td>
    <td width="25%" align="center" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Coordinatior Position</td>
    <td width="21%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;">SCHOOLS DIVISION SUPERINTENDENT</td>
    <td width="22%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;">SUPPLY AND/OR PROPERTY CUSTODIAN</td>
    <td width="19%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;">Coordinatior Position</td>
</tr>
<tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Date:</td>
    <td width="25%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="21%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="22%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="19%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
</tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('RIS-'.(strval($requisition->no)).'.pdf', 'I');
exit(0);