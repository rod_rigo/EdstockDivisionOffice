<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Requisition $requisition
 */
?>

<script>
    var id = parseInt(<?=(intval($requisition->id))?>);
</script>

<div class="row">

    <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-start align-items-center mb-3">
        <h5><?=(strval($requisition->no))?></h5>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'view', intval($requisition->request->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Request">
            Go To Request
        </a>
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'view', intval($requisition->order->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Order">
            Go To Order
        </a>
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'view', intval($requisition->inspection->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Inspection">
            Go To Inspection
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'pdf', intval($requisition->id)])?>" class="btn btn-secondary rounded-0 mx-1" title="PDF" target="_blank">
            PDF
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'xlsx', intval($requisition->id)])?>" class="btn btn-success rounded-0 mx-1" title="Return" target="_blank">
            XLSX
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0 ml-1" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($requisition,['id' => 'form', 'type' => 'file', 'class' => 'row']) ?>

        <div class="col-sm-12 col-md-5 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end">
                            <div class="icheck-primary">
                                <?= $this->Form->checkbox('completed',[
                                    'id' => 'completed',
                                    'hiddenField' => false,
                                    'checked' => (boolval($requisition->inspection->is_completed)? true: false),
                                    'disabled' => true,
                                ]);?>
                                <?=$this->Form->label('completed', ucwords('Completed'))?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end">
                            <div class="icheck-primary">
                                <?= $this->Form->checkbox('inspected',[
                                    'id' => 'inspected',
                                    'hiddenField' => false,
                                    'checked' => (boolval($requisition->inspection->is_inspected)? true: false),
                                    'disabled' => true,
                                ]);?>
                                <?=$this->Form->label('inspected', ucwords('Inspected'))?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('place_of_delivery', ucwords('Place Of Delivery'))?>
                            <?= $this->Form->textarea('place_of_delivery',[
                                'class' => 'form-control rounded-0',
                                'id' => 'place-of-delivery',
                                'placeholder' => ucwords('Place Of Delivery'),
                                'title' => ucwords('please enter a Place Of Delivery'),
                                'required' => false,
                                'value' => $requisition->order->place_of_delivery
                            ]);?>
                            <small></small>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('no',[
                        'id' => 'no',
                        'required' => true,
                        'readonly' => true,
                        'value' => strval($requisition->no)
                    ]);?>
                    <?= $this->Form->hidden('request_id',[
                        'id' => 'request-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval($requisition->request->id)
                    ]);?>
                    <?= $this->Form->hidden('order_id',[
                        'id' => 'order-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval($requisition->order->id)
                    ]);?>
                    <?= $this->Form->hidden('inspection_id',[
                        'id' => 'inspection-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval($requisition->inspection->id)
                    ]);?>
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-7 col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                            <?=$this->Form->label('division_id', ucwords('Division'))?>
                            <?= $this->Form->select('division_id', $divisions,[
                                'class' => 'form-control rounded-0 form-control-border',
                                'id' => 'division-id',
                                'required' => true,
                                'title' => ucwords('please select a Division')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                            <?=$this->Form->label('office_id', ucwords('office'))?>
                            <?= $this->Form->select('office_id', $offices,[
                                'class' => 'form-control rounded-0 form-control-border',
                                'id' => 'office-id',
                                'required' => true,
                                'title' => ucwords('please select a office')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                            <?=$this->Form->label('fund_cluster_id', ucwords('Fund Cluster'))?>
                            <?= $this->Form->select('fund_cluster_id', $fundClusters,[
                                'class' => 'form-control rounded-0 form-control-border',
                                'id' => 'fund-cluster-id',
                                'required' => true,
                                'title' => ucwords('please select a Fund Cluster')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                            <?=$this->Form->label('supplier_id', ucwords('Supplier'))?>
                            <?= $this->Form->select('supplier_id', $suppliers,[
                                'class' => 'form-control rounded-0 form-control-border',
                                'id' => 'supplier-id',
                                'required' => true,
                                'title' => ucwords('please select a Supplier')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                            <?=$this->Form->label('method_id', ucwords('Method'))?>
                            <?= $this->Form->select('method_id', $methods,[
                                'class' => 'form-control rounded-0 form-control-border',
                                'id' => 'method-id',
                                'required' => true,
                                'title' => ucwords('please select a Method')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="table-responsive">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Unit</th>
                                        <th>Supply Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody id="request-table">
                                    <?php $counter = 0;?>
                                    <?php foreach ($requisition->requisition_supplies as $key => $requisition_supply):?>
                                        <?php $counter++;?>
                                        <tr>
                                            <td>
                                                <?=intval($counter)?>
                                            </td>
                                            <td>
                                                <?=ucwords($requisition_supply->supply->unit->unit)?>
                                            </td>
                                            <td>
                                                <?=ucwords($requisition_supply->supply->supply_name)?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($requisition_supply->price))?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($requisition_supply->quantity))?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($requisition_supply->total))?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?= $this->Form->end() ?>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = mainurl+'requisitions/';
        var editor;
        var url = 'add/'+(parseInt(id));

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(data.redirect,{action:'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                        $('[name="'+(name)+'"]').parent().next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        CKEDITOR.ClassicEditor.create(document.getElementById('place-of-delivery'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Place Of Delivery',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>





