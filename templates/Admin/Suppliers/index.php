<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FundCluster[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('supplier', ucwords('Supplier'))?>
                        <?=$this->Form->text('supplier',[
                            'class' => 'form-control rounded-0',
                            'id' => 'supplier',
                            'required' => true,
                            'placeholder' => ucwords('Supplier'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Enter A Supplier')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('tin_number', ucwords('TIN Number'))?>
                        <?=$this->Form->text('tin_number',[
                            'class' => 'form-control rounded-0',
                            'id' => 'tin-number',
                            'required' => true,
                            'placeholder' => ucwords('TIN Number'),
                            'pattern' => '([0-9]{3})-([0-9]{3})-([0-9]{3})-([0-9]{3})',
                            'title' => ucwords('TIN Number Must Be 000-000-000-000')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                        <?=$this->Form->label('division_id', ucwords('Division'))?>
                        <?=$this->Form->select('division_id', $divisions,[
                            'class' => 'form-control rounded-0',
                            'id' => 'division-id',
                            'required' => true,
                            'empty' => ucwords('Select A Division'),
                            'title' => ucwords('Please Select A Division')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                        <?=$this->Form->label('address', ucwords('Address'))?>
                        <?=$this->Form->textarea('address',[
                            'class' => 'form-control rounded-0',
                            'id' => 'address',
                            'required' => true,
                            'placeholder' => ucwords('Address'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please enter a Address')
                        ])?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($auth['id'])
                ])?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Supplier">
            New Supplier
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Division</th>
                        <th>Supplier</th>
                        <th>TIN Number</th>
                        <th>Address</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'suppliers/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getSuppliers',
                supplier: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [6],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [7],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'division.division'},
                { data: 'supplier'},
                { data: 'tin_number'},
                { data: 'address'},
                { data: 'user.username'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+(parseInt(dataId));
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    url = 'edit/'+(parseInt(dataId));
                    $('button[type="reset"]').fadeOut(100);
                },
            }).done(function (data, status, xhr) {
                $('#division-id').val(parseInt(data.division_id));
                $('#supplier').val(data.supplier);
                $('#tin-number').val(data.tin_number);
                $('#address').val(data.address);
                $('#modal').modal('toggle');
                Swal.close();
            }).fail(function (data, status, xhr) {
                swal('error', 'Error', data.responseJSON.message);
            });

        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl + url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                $('#modal').modal('toggle');
                table.ajax.reload(null, false);
                swal('success', null, data.message);
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#toggle-modal').click(function (e) {
            url = 'add';
            $('#modal').modal('toggle');
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('#form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#supplier').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Supplier');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#tin-number').on('input', function (e) {
            var regex = /^([0-9]{3})-([0-9]{3})-([0-9]{3})-([0-9]{3})$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('TIN Number Must Be 000-000-000-000');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        }).mask('000-000-000-000', {placeholder: '000-000-000-000'});

        $('#address').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Address');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
