<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supply $supply
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $units
 * @var \Cake\Collection\CollectionInterface|string[] $ranges
 */
?>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($supply,['id' => 'form', 'type' => 'file']) ?>
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-8 col-lg-8 mt-4">
                        <?=$this->Form->label('supply_name', ucwords('Supply Name'))?>
                        <?= $this->Form->text('supply_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'supply-name',
                            'required' => true,
                            'placeholder' => ucwords('Supply Name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter a Supply Name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('range_id', ucwords('Range'))?>
                        <?= $this->Form->select('range_id', $ranges,[
                            'class' => 'form-control rounded-0',
                            'id' => 'range-id',
                            'required' => true,
                            'empty' => ucwords('Select Range'),
                            'title' => ucwords('please select a Range')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('category_id', ucwords('category'))?>
                        <?= $this->Form->select('category_id', $categories,[
                            'class' => 'form-control rounded-0',
                            'id' => 'category-id',
                            'required' => true,
                            'empty' => ucwords('Select category'),
                            'title' => ucwords('please select a category')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('unit_id', ucwords('unit'))?>
                        <?= $this->Form->select('unit_id', $units,[
                            'class' => 'form-control rounded-0',
                            'id' => 'unit-id',
                            'required' => true,
                            'empty' => ucwords('Select unit'),
                            'title' => ucwords('please select a unit')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('supply_code', ucwords('Supply code'))?>
                        <?= $this->Form->text('supply_code',[
                            'class' => 'form-control rounded-0',
                            'id' => 'supply-code',
                            'required' => true,
                            'placeholder' => ucwords('Supply code'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter a Supply code')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('price', ucwords('Price'))?>
                        <?= $this->Form->number('price',[
                            'class' => 'form-control rounded-0',
                            'id' => 'price',
                            'required' => true,
                            'placeholder' => ucwords('Price'),
                            'autocomplete' => 'off',
                            'title' => ucwords('please enter a Price'),
                            'min' => '0',
                            'step' => 'any'
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('stock', ucwords('Stock'))?>
                        <?= $this->Form->number('stock',[
                            'class' => 'form-control rounded-0',
                            'id' => 'stock',
                            'required' => true,
                            'placeholder' => ucwords('Stock'),
                            'autocomplete' => 'off',
                            'title' => ucwords('please enter a Stock'),
                            'min' => '0',
                            'step' => 'any'
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-5 mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="in-stock-0" class="in-stock" value="0"/>
                                    <label for="in-stock-0">Out Of Stock</label>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="in-stock-1" class="in-stock" value="1"/>
                                    <label for="in-stock-1">In Stock</label>
                                </div>
                            </div>

                            <?= $this->Form->hidden('in_stock',[
                                'id' => 'in-stock',
                                'required' => true,
                            ]);?>
                            <small></small>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-5 mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="is-expendable-0" class="is-expendable" value="0"/>
                                    <label for="is-expendable-0">Semi-Expendable</label>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="is-expendable-1" class="is-expendable" value="1"/>
                                    <label for="is-expendable-1">Expendable</label>
                                </div>
                            </div>

                            <?= $this->Form->hidden('is_expendable',[
                                'id' => 'is-expendable',
                                'required' => true,
                            ]);?>
                            <small></small>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 mt-4">
                        <?=$this->Form->label('expired_at',ucwords('Expired At'))?>
                        <?=$this->Form->date('expired_at',[
                            'class' => 'form-control rounded-0'
                        ])?>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('description', ucwords('Description'))?>
                        <?= $this->Form->textarea('description',[
                            'class' => 'form-control rounded-0',
                            'id' => 'description',
                            'placeholder' => ucwords('Description'),
                            'title' => ucwords('please enter a Description')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <h6>
                            <strong>Divisions</strong>
                        </h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap justify-content-start align-items-center">
                        <?php foreach ($divisions as $key => $value):?>
                            <div class="icheck-primary m-2">
                                <?=$this->Form->checkbox('division_ids[]',[
                                    'id' => 'divisions-'.(intval($key)),
                                    'value' => intval($key),
                                    'hiddenField' => false
                                ])?>
                                <label for="divisions-<?=(intval($key))?>"><?=ucwords($value)?></label>
                            </div>
                        <?php endforeach;?>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',['id' => 'user-id', 'required' => true, 'readonly' => true, 'value' => intval($auth['id'])]);?>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;
        var editor;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(data.redirect,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#supply-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Supply Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#supply-code').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Supply Code');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });


        $('input[type="number"]').keypress(function (e) {
            var key = e.key;
            var regex = /^([0-9\.]){1,}$/;
            if(!key.match(regex)){
                e.preventDefault();
            }
        });

        $('#price').on('input', function (e) {
            var regex = /^([0-9\.]){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Price');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#stock').on('input', function (e) {
            var regex = /^([0-9\.]){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Stocks');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('.in-stock').change(function (e) {
            var value = $(this).val();
            $(this).prop('checked', true);
            $('.in-stock:checked').not(this).prop('checked', false);
            $('#in-stock').val(parseInt(value));
        });

        $('.is-expendable').change(function (e) {
            var value = $(this).val();
            $(this).prop('checked', true);
            $('.is-expendable:checked').not(this).prop('checked', false);
            $('#is-expendable').val(parseInt(value));
        });

        CKEDITOR.ClassicEditor.create(document.getElementById('description'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Description',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
