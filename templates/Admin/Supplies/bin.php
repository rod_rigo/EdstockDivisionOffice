<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FundCluster[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Supply Name</th>
                        <th>Category</th>
                        <th>Unit</th>
                        <th>Range</th>
                        <th>Supply Code</th>
                        <th>Stock</th>
                        <th>Price</th>
                        <th>Stock Availability</th>
                        <th>Semi/Expendable</th>
                        <th>Modified By</th>
                        <th>Deleted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'supplies/';
        var url = '';
        var is_expendables = ['Semi-Expendable', 'Expendable'];
        var in_stock = ['Out of Stock', 'In Stock'];
        var is_in = ['<i class="fas fa-minus"></i>', '<i class="fas fa-plus"></i>'];
        var description = ['Out Stock', 'In Stock'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getSuppliesDeleted',
                official: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [6],
                    data: null,
                    render: function(data,type,row,meta){
                        return (new Intl.NumberFormat().format(parseFloat(row.stock)));
                    }
                },
                {
                    targets: [7],
                    data: null,
                    render: function(data,type,row,meta){
                        return (new Intl.NumberFormat().format(parseFloat(row.price)));
                    }
                },
                {
                    targets: [8],
                    data: null,
                    render: function(data,type,row,meta){
                        return (in_stock[parseInt(row.in_stock)]);
                    }
                },
                {
                    targets: [9],
                    data: null,
                    render: function(data,type,row,meta){
                        return (is_expendables[parseInt(row.is_expendable)]);
                    }
                },
                {
                    targets: [11],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.deleted).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [12],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white restore" title="Restore">Restore</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'supply_name'},
                { data: 'category.category'},
                { data: 'unit.unit'},
                { data: 'range.code'},
                { data: 'supply_code'},
                { data: 'stock'},
                { data: 'price'},
                { data: 'in_stock'},
                { data: 'is_expendable'},
                { data: 'user.username'},
                { data: 'deleted'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.restore',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'restore/'+(parseInt(dataId));
            Swal.fire({
                title: 'Restore Data',
                text: 'Are You Sure?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'hardDelete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
