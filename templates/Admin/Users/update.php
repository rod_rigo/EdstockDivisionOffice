<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($user,['id' => 'form', 'type' => 'file']) ?>
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('username', ucwords('Username'))?>
                        <?= $this->Form->text('username',[
                            'class' => 'form-control rounded-0',
                            'id' => 'username',
                            'required' => true,
                            'placeholder' => ucwords('Username'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter a username')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('email', ucwords('Email'))?>
                        <?= $this->Form->email('email',[
                            'class' => 'form-control rounded-0',
                            'id' => 'email',
                            'required' => true,
                            'placeholder' => ucwords('Email'),
                            'autocomplete' => 'off',
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-10 col-lg-10 mt-4">
                        <?=$this->Form->label('contact_number', ucwords('Contact number'))?>
                        <?= $this->Form->text('contact_number',[
                            'class' => 'form-control rounded-0',
                            'id' => 'contact-number',
                            'required' => true,
                            'placeholder' => ucwords('Contact number'),
                            'autocomplete' => 'off',
                            'pattern' => '(09|\+639)([0-9]{9})',
                            'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('first_name', ucwords('first name'))?>
                        <?= $this->Form->text('first_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'first-name',
                            'required' => true,
                            'placeholder' => ucwords('first name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your first name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('middle_name', ucwords('middle name'))?>
                        <?= $this->Form->text('middle_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'middle-name',
                            'required' => true,
                            'placeholder' => ucwords('middle name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your middle name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('last_name', ucwords('last name'))?>
                        <?= $this->Form->text('last_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'last-name',
                            'required' => true,
                            'placeholder' => ucwords('last name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your last name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-5 mt-4">
                        <?=$this->Form->label('profile.division_id', ucwords('Division'))?>
                        <?= $this->Form->select('profile.division_id', $divisions,[
                            'class' => 'form-control rounded-0',
                            'id' => 'profile-division-id',
                            'required' => true,
                            'empty' => ucwords('Select Division'),
                            'title' => ucwords('please select a Division')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-5 mt-4">
                        <?=$this->Form->label('profile.office_id', ucwords('office'))?>
                        <?= $this->Form->select('profile.office_id', $offices,[
                            'class' => 'form-control rounded-0',
                            'id' => 'profile-office-id',
                            'required' => true,
                            'empty' => ucwords('Choose Division'),
                            'title' => ucwords('please select a office')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="admin" <?=(boolval($user->is_admin)? 'checked': null);?>/>
                                    <label for="admin">Admin</label>
                                    <?= $this->Form->hidden('is_admin',[
                                        'id' => 'is-admin',
                                        'required' => true,
                                        'value' => intval($user->is_admin)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="division" <?=(boolval($user->is_division)? 'checked': null);?>/>
                                    <label for="division">Division</label>
                                    <?= $this->Form->hidden('is_division',[
                                        'id' => 'is-division',
                                        'required' => true,
                                        'value' => intval($user->divisions)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="secretary" <?=(boolval($user->is_secretary)? 'checked': null);?>/>
                                    <label for="secretary">Secretary</label>
                                    <?= $this->Form->hidden('is_secretary',[
                                        'id' => 'is-secretary',
                                        'required' => true,
                                        'value' => intval($user->is_secretary)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="non-teaching" <?=(boolval($user->is_non_teaching)? 'checked': null);?>/>
                                    <label for="non-teaching">Non-Teaching</label>
                                    <?= $this->Form->hidden('is_non_teaching',[
                                        'id' => 'is-non-teaching',
                                        'required' => true,
                                        'value' => intval($user->is_non_teaching)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="active" <?=(boolval($user->is_active)? 'checked': null);?>/>
                                    <label for="active">Active</label>
                                    <?= $this->Form->hidden('is_active',[
                                        'id' => 'is-active',
                                        'required' => true,
                                        'value' => intval($user->is_active)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('token',['id' => 'token', 'required' => true, 'readonly' => true, 'value' => uniqid()]);?>
                <?= $this->Form->hidden('is_active',['id' => 'is-active', 'required' => true, 'readonly' => true, 'value' => intval(1)]);?>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(data.redirect,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#username').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Username');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#email').on('input', function (e) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please @ In Email');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#first-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A First Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#middle-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Middle Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#last-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Last Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#contact-number').on('input', function (e) {
            var regex = /^(09|\+639)([0-9]{9})$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#admin').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-admin').val(Number(checked));
        });

        $('#division').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-division').val(Number(checked));
        });

        $('#secretary').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-secretary').val(Number(checked));
        });

        $('#non-teaching').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-non-teaching').val(Number(checked));
        });

        $('#active').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-active').val(Number(checked));
        });

        $('#profile-division-id').change(function (e) {
            var value = parseInt($(this).val());
            getOfficesList(parseInt(value));
        });

        function getOfficesList(divisionId) {
            $.ajax({
                url: mainurl+'offices/getOfficesList/'+(parseInt(divisionId)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#profile-office-id').prop('disabled', true).empty().append('<option value="">Please Wait</option>');
                },
            }).done(function (data, status, xhr) {
                $('#profile-office-id').prop('disabled', false).empty().append('<option value="">Select Office</option>');
                $.map(data, function (data, key) {
                    $('#profile-office-id').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Offices');
            });
        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>