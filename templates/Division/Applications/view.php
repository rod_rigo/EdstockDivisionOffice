<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Application $application
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $divisions
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 */
?>

<script>
    var id = parseInt(<?=intval($application->id)?>);
</script>

<div class="modal fade" id="approved-modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($application,['id' => 'approved-form', 'type' => 'file']) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('password', ucwords('Password'))?>
                        <div class="input-group">
                            <?= $this->Form->password('password', [
                                'class' => 'form-control rounded-0',
                                'id' => 'password',
                                'required' => true,
                                'placeholder' => ucwords('Password'),
                                'title' => ucwords('please enter a password'),
                                'pattern' => '(.){1,}'
                            ]);?>
                            <div class="input-group-append rounded-0 border-info">
                                <span class="input-group-text rounded-0 bg-info border-info">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                        </div>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="modal fade" id="declined-modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($application,['id' => 'declined-form', 'type' => 'file']) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('password', ucwords('Password'))?>
                        <div class="input-group">
                            <?= $this->Form->password('password', [
                                'class' => 'form-control rounded-0',
                                'id' => 'password',
                                'required' => true,
                                'placeholder' => ucwords('Password'),
                                'title' => ucwords('please enter a password'),
                                'pattern' => '(.){1,}'
                            ]);?>
                            <div class="input-group-append rounded-0 border-info">
                                <span class="input-group-text rounded-0 bg-info border-info">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                        </div>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">

        <?php if((!boolval($application->is_approved) && !boolval($application->is_declined))):?>
            <button type="button" class="btn btn-success rounded-0 mx-1" id="approved" title="Approve <?=strval($application->no)?>">
                Approve
            </button>
            <button type="button" class="btn btn-danger rounded-0 mx-1" id="declined" title="Decline <?=strval($application->no)?>">
                Decline
            </button>
        <?php endif;?>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Applications', 'action' => 'pdf', intval($application->id)])?>" class="btn btn-secondary rounded-0 mx-1" title="PDF" target="_blank">
            PDF
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Applications', 'action' => 'xlsx', intval($application->id)])?>" class="btn btn-success rounded-0 mx-1" title="XLSX" target="_blank">
            XLSX
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Applications', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0 ml-1" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($application,['id' => 'form', 'type' => 'file', 'class' => 'row']) ?>

        <div class="col-sm-12 col-md-5 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('division_id', ucwords('Division'))?>
                            <?= $this->Form->select('division_id', $divisions,[
                                'class' => 'form-control rounded-0',
                                'id' => 'division-id',
                                'required' => true,
                                'empty' => ucwords('Select Division'),
                                'title' => ucwords('please select a Division')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('office_id', ucwords('office'))?>
                            <?= $this->Form->select('office_id', $offices,[
                                'class' => 'form-control rounded-0',
                                'id' => 'office-id',
                                'required' => true,
                                'empty' => ucwords('Choose Division'),
                                'title' => ucwords('please select a office')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('purpose', ucwords('Purpose'))?>
                            <?= $this->Form->textarea('purpose',[
                                'class' => 'form-control rounded-0',
                                'id' => 'purpose',
                                'placeholder' => ucwords('Purpose'),
                                'title' => ucwords('please enter a Purpose'),
                                'required' => false
                            ]);?>
                            <small></small>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('user_id',['id' => 'user-id', 'required' => true, 'readonly' => true, 'value' => intval($auth['id'])]);?>
                    <?= $this->Form->hidden('no',[
                        'id' => 'no',
                        'class' => 'd-none',
                        'required' => true,
                        'readonly' => true,
                        'value' => strval($application->no)
                    ]);?>
                    <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Applications', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-success rounded-0',
                        'type' => 'submit'
                    ])?>
                    <?=$this->Form->button('Reset',[
                        'class' => 'btn btn-danger rounded-0',
                        'type' => 'reset'
                    ])?>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-7 col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-4">
                            <?=$this->Form->label('approved_at', ucwords('Approved At'))?>
                            <?= $this->Form->dateTime('approved_at',[
                                'id' => 'approved-at',
                                'class' => 'form-control form-control-border',
                                'readonly' => true,
                                'required' => true,
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end">
                            <div class="icheck-success">
                                <?= $this->Form->checkbox('approved',[
                                    'id' => 'approved',
                                    'required' => true,
                                    'disabled' => true,
                                    'hiddenField' => false,
                                    'checked' => (boolval($application->is_approved)? true: false)
                                ]);?>
                                <?=$this->Form->label('approved', ucwords('Approved'))?>
                            </div>
                            <?= $this->Form->hidden('is_approved',[
                                'id' => 'is-approved',
                                'required' => true,
                                'value' => intval($application->is_approved)
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-4">
                            <?=$this->Form->label('declined_at', ucwords('Declined At'))?>
                            <?= $this->Form->dateTime('declined_at',[
                                'id' => 'declined-at',
                                'class' => 'form-control form-control-border',
                                'readonly' => true,
                                'required' => true,
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end">
                            <div class="icheck-danger">
                                <?= $this->Form->checkbox('declined',[
                                    'id' => 'declined',
                                    'required' => true,
                                    'disabled' => true,
                                    'hiddenField' => false,
                                    'checked' => (boolval($application->is_declined)? true: false)
                                ]);?>
                                <?=$this->Form->label('declined', ucwords('Declined'))?>
                            </div>
                            <?= $this->Form->hidden('is_declined',[
                                'id' => 'is-declined',
                                'required' => true,
                                'value' => intval($application->is_declined)
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="table-responsive">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Unit</th>
                                        <th>Supply Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody id="application-table">
                                    <?php $counter = 1;?>
                                    <?php foreach ($application->application_supplies as $application_supply):?>
                                        <tr>
                                            <td>
                                                <?=$counter++?>
                                            </td>
                                            <td>
                                                <?=ucwords($application_supply->supply->unit->unit)?>
                                            </td>
                                            <td>
                                                <?=ucwords($application_supply->supply->supply_name)?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($application_supply->price))?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($application_supply->quantity))?>
                                            </td>
                                            <td>
                                                <?=number_format(doubleval($application_supply->total))?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?= $this->Form->end() ?>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = mainurl+'applications/edit/'+(id);
        var editor;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#approved-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: mainurl+'applications/approved/'+(id),
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#approved-form')[0].reset();
                $('#approved-modal').modal('toggle');
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('#approved-form').find('[name="'+(name)+'"]').addClass('is-invalid');
                        $('#approved-form').find('[name="'+(name)+'"]').parent().next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#declined-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: mainurl+'applications/declined/'+(id),
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#declined-form')[0].reset();
                $('#declined-modal').modal('toggle');
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('#declined-form').find('[name="'+(name)+'"]').addClass('is-invalid');
                        $('#declined-form').find('[name="'+(name)+'"]').parent().next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        CKEDITOR.ClassicEditor.create(document.getElementById('purpose'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Purpose',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        $('#approved').click(function (e) {
            Swal.fire({
                title: 'Approve Application',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#approved-modal').modal('toggle');
                }
            });
        });

        $('#approved-modal').on('hidden.bs.modal', function (e) {
            $('#approved-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#declined').click(function (e) {
            Swal.fire({
                title: 'Decline Application',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#declined-modal').modal('toggle');
                }
            });
        });

        $('#declined-modal').on('hidden.bs.modal', function (e) {
            $('#declined-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('input[type="password"]').on('input', function (e) {
            var value = $(this).val();
            var regex = /^(.){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').parent().next('small').text('Please A Password');
                return true;
            }

            $(this).removeClass('is-invalid').parent().next('small').empty();

        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
