<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Request[]|\Cake\Collection\CollectionInterface $requests
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requests', 'action' => 'add'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="New Request">
            New Request
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No</th>
                        <th>Request By</th>
                        <th>Division</th>
                        <th>Office</th>
                        <th>Fund Cluster</th>
                        <th>Is Approved</th>
                        <th>Is Declined</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'requests/';
        var url = '';
        var isApproved = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];
        var isDeclined = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getRequestsToday',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [6],
                    data: null,
                    render: function(data,type,row,meta){
                        return isApproved[(parseInt(row.is_approved))];
                    }
                },
                {
                    targets: [7],
                    data: null,
                    render: function(data,type,row,meta){
                        return isDeclined[(parseInt(row.is_declined))];
                    }
                },
                {
                    targets: [8],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [9],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        var order = (row.order == null)? '| <a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a> | ': ' | ';
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> '+
                            order +
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white pdf" title="PDF">PDF</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white xlsx" title="XLSX">XLSX</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'no'},
                { data: 'user.username'},
                { data: 'division.division'},
                { data: 'office.office'},
                { data: 'fund_cluster.fund_cluster'},
                { data: 'is_approved'},
                { data: 'is_declined'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });


        datatable.on('click','.view',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'view/'+(parseInt(dataId));
            Swal.fire({
                title: 'View Data',
                text: 'Open In New Window?',
                icon: 'question',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                denyButtonColor: '#6e7881',
                cancelButtonText: 'No',
                denyButtonText: 'Cancel',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                    return true;
                }else if(result.isDenied){
                    Swal.close();
                    return true;
                }
                Turbolinks.visit(href,{ action:'advance' });
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'pdf/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To PDF',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        datatable.on('click','.xlsx',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'xlsx/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To Excel',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>

