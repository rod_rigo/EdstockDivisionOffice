<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Request $request
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $divisions
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 * @var \Cake\Collection\CollectionInterface|string[] $fundClusters
 */
?>

<script>
    var id = parseInt(<?=($request->id)?>);
</script>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($request,['id' => 'decline-form', 'type' => 'file']) ?>
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('password', ucwords('Password'))?>
                        <div class="input-group">
                            <?= $this->Form->password('password', [
                                'class' => 'form-control rounded-0',
                                'id' => 'password',
                                'required' => true,
                                'placeholder' => ucwords('Password'),
                                'title' => ucwords('please enter a password')
                            ]);?>
                            <div class="input-group-append rounded-0 border-info">
                                <span class="input-group-text rounded-0 bg-info border-info">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                        </div>
                        <small></small>

                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?= $this->Form->hidden('is_declined',[
                    'id' => 'is-declined',
                    'required' => true,
                    'value' => intval(1)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="row">

    <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-start align-items-center mb-3">
        <h5><?=(strval($request->no))?></h5>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-end align-items-center mb-3">
        <?php if((!boolval($request->is_approved) && !boolval($request->is_declined)) && empty($request->order)):?>
            <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Orders', 'action' => 'add', intval($request->id)])?>" turbolink class="btn btn-success rounded-0 mr-1" title="Approve <?=strval($request->no)?>">
                Approve
            </a>
            <button type="button" id="toggle-modal" class="btn btn-danger rounded-0 mr-1" title="Decline <?=strval($request->no)?>">
                Decline
            </button>
        <?php endif;?>

        <?php if (!empty($request->order)):?>
            <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Orders', 'action' => 'view', intval($request->order->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Order">
                Go To Order
            </a>
        <?php endif;?>

        <?php if (!empty($request->inspection)):?>
            <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Inspections', 'action' => 'view', intval($request->inspection->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Inspection">
                Go To Inspection
            </a>
        <?php endif;?>

        <?php if (!empty($request->requisition)):?>
            <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requisitions', 'action' => 'view', intval($request->requisition->id)])?>" turbolink class="btn btn-primary rounded-0 mx-1" title="Go To Requisition">
                Go To Requisition
            </a>
        <?php endif;?>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requests', 'action' => 'pdf', intval($request->id)])?>" class="btn btn-secondary rounded-0 mx-1" title="PDF" target="_blank">
            PDF
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requests', 'action' => 'xlsx', intval($request->id)])?>" class="btn btn-success rounded-0 mx-1" title="Return" target="_blank">
            XLSX
        </a>

        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requests', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0 ml-1" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($request,['id' => 'form', 'type' => 'file', 'class' => 'row']) ?>

        <div class="col-sm-12 col-md-5 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('office_id', ucwords('office'))?>
                            <?= $this->Form->select('office_id', $offices,[
                                'class' => 'form-control rounded-0',
                                'id' => 'office-id',
                                'required' => true,
                                'empty' => ucwords('Choose Division'),
                                'title' => ucwords('please select a office')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('fund_cluster_id', ucwords('Fund Cluster'))?>
                            <?= $this->Form->select('fund_cluster_id', $fundClusters,[
                                'class' => 'form-control rounded-0',
                                'id' => 'fund-cluster-id',
                                'required' => true,
                                'empty' => ucwords('Select Fund Cluster'),
                                'title' => ucwords('please select a Fund Cluster')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?=$this->Form->label('purpose', ucwords('Purpose'))?>
                            <?= $this->Form->textarea('purpose',[
                                'class' => 'form-control rounded-0',
                                'id' => 'purpose',
                                'placeholder' => ucwords('Purpose'),
                                'title' => ucwords('please enter a Purpose')
                            ]);?>
                            <small></small>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('division_id',['id' => 'division-id', 'required' => true, 'readonly' => true, 'value' => intval($request->division_id)]);?>
                    <?= $this->Form->hidden('user_id',['id' => 'user-id', 'required' => true, 'readonly' => true, 'value' => intval($request->user_id)]);?>
                    <?= $this->Form->hidden('no',[
                        'id' => 'no',
                        'class' => 'd-none',
                        'required' => true,
                        'readonly' => true,
                        'value' => strval($request->no)
                    ]);?>
                    <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Requests', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                   <?php if(empty($request->order)):?>
                       <?=$this->Form->button('Submit',[
                           'class' => 'btn btn-success rounded-0',
                           'type' => 'submit'
                       ])?>
                       <?=$this->Form->button('Reset',[
                           'class' => 'btn btn-danger rounded-0',
                           'type' => 'reset'
                       ])?>
                   <?php endif;?>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-7 col-lg-8">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-4">
                            <?=$this->Form->label('approved_at', ucwords('Approved At'))?>
                            <?= $this->Form->dateTime('approved_at',[
                                'id' => 'approved-at',
                                'class' => 'form-control form-control-border',
                                'readonly' => true,
                                'required' => true,
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end">
                            <div class="icheck-success">
                                <?= $this->Form->checkbox('approved',[
                                    'id' => 'approved',
                                    'required' => true,
                                    'disabled' => true,
                                    'hiddenField' => false,
                                    'checked' => (boolval($request->is_approved)? true: false)
                                ]);?>
                                <?=$this->Form->label('approved', ucwords('Approved'))?>
                            </div>
                            <?= $this->Form->hidden('is_approved',[
                                'id' => 'is-approved',
                                'required' => true,
                                'value' => intval(0)
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-4">
                            <?=$this->Form->label('declined_at', ucwords('Declined At'))?>
                            <?= $this->Form->dateTime('declined_at',[
                                'id' => 'declined-at',
                                'class' => 'form-control form-control-border',
                                'readonly' => true,
                                'required' => true,
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end">
                            <div class="icheck-danger">
                                <?= $this->Form->checkbox('declined',[
                                    'id' => 'declined',
                                    'required' => true,
                                    'disabled' => true,
                                    'hiddenField' => false,
                                    'checked' => (boolval($request->is_declined)? true: false)
                                ]);?>
                                <?=$this->Form->label('declined', ucwords('Declined'))?>
                            </div>
                            <?= $this->Form->hidden('is_declined',[
                                'id' => 'is-declined',
                                'required' => true,
                                'value' => intval(0)
                            ]);?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="table-responsive">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Unit</th>
                                        <th>Supply Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody id="request-table">
                                    <?php $counter = 0;?>
                                    <?php foreach ($request->request_supplies as $key => $request_supply):?>
                                        <?php $counter++;?>
                                        <tr>
                                            <td><?=intval($counter)?></td>
                                            <td><?=ucwords($request_supply->supply->unit->unit)?></td>
                                            <td><?=ucwords($request_supply->supply->supply_name)?></td>
                                            <td><?=number_format(doubleval($request_supply->price))?></td>
                                            <td><?=number_format(doubleval($request_supply->quantity))?></td>
                                            <td><?=number_format(doubleval($request_supply->total))?></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?= $this->Form->end() ?>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = mainurl+'requests/';
        var editor;
        var url;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl+'edit/'+(parseInt(id)),
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                window.location.reload();
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#decline-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#decline-form')[0].reset();
                swal('success', null, data.message);
                window.location.reload();
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                        $('[name="'+(name)+'"]').parent().next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            });
        });

        $('#toggle-modal').click(function (e) {
            url = mainurl+'requests/decline/'+(parseInt(id));
            Swal.fire({
                title: 'Decline Requests',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#modal').modal('toggle');
                }
            });
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('#decline-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#division-id').change(function (e) {
            var value = parseInt($(this).val());
            getOfficesList(parseInt(value));
        });

        $('#password').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').parent().next('small').text('Please Enter A Password');
                return true;
            }

            $(this).removeClass('is-invalid').parent().next('small').empty();

        });

        CKEDITOR.ClassicEditor.create(document.getElementById('purpose'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Purpose',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function getOfficesList(divisionId) {
            $.ajax({
                url: mainurl+'offices/getOfficesList/'+(parseInt(divisionId)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#office-id').prop('disabled', true).empty().append('<option value="">Please Wait</option>');
                },
            }).done(function (data, status, xhr) {
                $('#office-id').prop('disabled', false).empty().append('<option value="">Select Office</option>');
                $.map(data, function (data, key) {
                    $('#office-id').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Offices');
            });
        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
