<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Ris', 'action' => 'pdf', intval($seriese->id)])?>" class="btn btn-secondary rounded-0" title="PDF" target="_blank">
            PDF
        </a>
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Ris', 'action' => 'xlsx', intval($seriese->id)])?>" class="btn btn-success rounded-0  mx-2" title="Return" target="_blank">
            XLSX
        </a>
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Ris', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('division', ucwords('Division'))?>
                        <?=$this->Form->text('division',[
                            'id' => 'division',
                            'placeholder' => ucwords('division'),
                            'class' => 'form-control form-control-border rounded-0',
                            'readonly' => true,
                            'value' => ucwords($seriese->application->division->division)
                        ])?>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('office', ucwords('Office'))?>
                        <?=$this->Form->text('office',[
                            'id' => 'office',
                            'placeholder' => ucwords('office'),
                            'class' => 'form-control form-control-border rounded-0',
                            'readonly' => true,
                            'value' => ucwords($seriese->application->office->office)
                        ])?>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="table-responsive">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Unit</th>
                                    <th>Supply Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="application-table">
                                <?php $counter = 1;?>
                                <?php foreach ($seriese->application_supplies as $seriese_supply):?>
                                    <tr>
                                        <td>
                                            <?=$counter++?>
                                        </td>
                                        <td>
                                            <?=ucwords($seriese_supply->supply->unit->unit)?>
                                        </td>
                                        <td>
                                            <?=ucwords($seriese_supply->supply->supply_name)?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($seriese_supply->price))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($seriese_supply->quantity))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($seriese_supply->total))?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('purpose', ucwords('Purpose'))?>
                        <?= $this->Form->textarea('purpose',[
                            'class' => 'form-control rounded-0',
                            'id' => 'purpose',
                            'placeholder' => ucwords('Purpose'),
                            'title' => ucwords('please enter a Purpose'),
                            'required' => false,
                            'value' => $seriese->application->purpose
                        ]);?>
                        <small></small>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var editor;

        CKEDITOR.ClassicEditor.create(document.getElementById('purpose'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Purpose',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            data.enableReadOnlyMode('editor');
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>