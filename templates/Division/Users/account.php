<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<script>
    var id = parseInt(<?=(intval(@$auth['id']))?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Profiles', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($user,['id' => 'form', 'type' => 'file']) ?>
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('username', ucwords('Username'))?>
                        <?= $this->Form->text('username',[
                            'class' => 'form-control rounded-0',
                            'id' => 'username',
                            'required' => true,
                            'placeholder' => ucwords('Username'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter a username')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('email', ucwords('Email'))?>
                        <?= $this->Form->email('email',[
                            'class' => 'form-control rounded-0',
                            'id' => 'email',
                            'required' => true,
                            'placeholder' => ucwords('Email'),
                            'autocomplete' => 'off',
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('first_name', ucwords('first name'))?>
                        <?= $this->Form->text('first_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'first-name',
                            'required' => true,
                            'placeholder' => ucwords('first name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your first name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('middle_name', ucwords('middle name'))?>
                        <?= $this->Form->text('middle_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'middle-name',
                            'required' => true,
                            'placeholder' => ucwords('middle name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your middle name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                        <?=$this->Form->label('last_name', ucwords('last name'))?>
                        <?= $this->Form->text('last_name',[
                            'class' => 'form-control rounded-0',
                            'id' => 'last-name',
                            'required' => true,
                            'placeholder' => ucwords('last name'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter your last name')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-10 col-lg-10 mt-4">
                        <?=$this->Form->label('contact_number', ucwords('Contact number'))?>
                        <?= $this->Form->text('contact_number',[
                            'class' => 'form-control rounded-0',
                            'id' => 'contact-number',
                            'required' => true,
                            'placeholder' => ucwords('Contact number'),
                            'autocomplete' => 'off',
                            'pattern' => '(09|\+639)([0-9]{9})',
                            'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="row">

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="division" <?=(boolval($user->is_division)? 'checked': null);?>/>
                                    <label for="division">Division</label>
                                    <?= $this->Form->hidden('is_division',[
                                        'id' => 'is-division',
                                        'required' => true,
                                        'value' => intval($user->divisions)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="secretary" <?=(boolval($user->is_secretary)? 'checked': null);?>/>
                                    <label for="secretary">Secretary</label>
                                    <?= $this->Form->hidden('is_secretary',[
                                        'id' => 'is-secretary',
                                        'required' => true,
                                        'value' => intval($user->is_secretary)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="non-teaching" <?=(boolval($user->is_non_teaching)? 'checked': null);?>/>
                                    <label for="non-teaching">Non-Teaching</label>
                                    <?= $this->Form->hidden('is_non_teaching',[
                                        'id' => 'is-non-teaching',
                                        'required' => true,
                                        'value' => intval($user->is_non_teaching)
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'required' => true,
                    'value' => intval($user->is_active)
                ]);?>
                <?= $this->Form->hidden('is_admin',[
                    'id' => 'is-admin',
                    'required' => true,
                    'value' => intval(0)
                ]);?>
                <?= $this->Form->hidden('token',['id' => 'token', 'required' => true, 'readonly' => true, 'value' => uniqid()]);?>
                <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Users', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($user,['id' => 'password-form', 'type' => 'file']) ?>
        <div class="card">
            <div class="card-body">
                <div class="row d-flex flex-wrap justify-content-center align-items-center">

                    <div class="col-sm-12 col-md-7 col-lg-7 mt-4">
                        <?=$this->Form->label('current_password', ucwords('password (Current)'))?>
                        <?= $this->Form->password('current_password',[
                            'class' => 'form-control rounded-0',
                            'id' => 'current-password',
                            'required' => true,
                            'placeholder' => ucwords('password (Current)'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){6,}',
                            'title' => ucwords('Password Must Be 6 Characters Long'),
                            ''
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7 mt-4">
                        <?=$this->Form->label('password', ucwords('password (New)'))?>
                        <?= $this->Form->password('password',[
                            'class' => 'form-control rounded-0',
                            'id' => 'password',
                            'required' => true,
                            'placeholder' => ucwords('password (New)'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){6,}',
                            'title' => ucwords('Password Must Be 6 Characters Long'),
                            'value' => ''
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7 mt-4">
                        <?=$this->Form->label('confirm_password', ucwords('password (Confirm)'))?>
                        <?= $this->Form->password('confirm_password',[
                            'class' => 'form-control rounded-0',
                            'id' => 'confirm-password',
                            'required' => true,
                            'placeholder' => ucwords('password (Confirm)'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){6,}',
                            'title' => ucwords('Password Must Be 6 Characters Long')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7 d-flex justify-content-start align-items-center mt-2">
                        <div class="icheck-primary">
                            <input type="checkbox" id="show-password" />
                            <label for="show-password">Show Password</label>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('token',['id' => 'token', 'required' => true, 'readonly' => true, 'value' => uniqid()]);?>
                <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Users', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#username').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Username');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#email').on('input', function (e) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please @ In Email');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#first-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A First Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#middle-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Middle Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#last-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Last Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#contact-number').on('input', function (e) {
            var regex = /^(09|\+639)([0-9]{9})$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#admin').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-admin').val(Number(checked));
        });

        $('#division').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-division').val(Number(checked));
        });

        $('#secretary').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-secretary').val(Number(checked));
        });

        $('#non-teaching').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-non-teaching').val(Number(checked));
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
    $(document).ready(function (e) {
        var baseurl = mainurl+'users/changepassword/'+(parseInt(id));

        $('#password-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#password-form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#current-password').on('input', function (e) {
            var regex = /^(.){6,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Curent Password Must Be 6 Characters Long');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#password').on('input', function (e) {
            var regex = /^(.){6,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('New Password Must Be 6 Characters Long');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#confirm-password').on('input', function (e) {
            var regex = /^(.){6,}$/;
            var value = $(this).val();
            var password = $('#password').val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Confirm Password Must Be 6 Characters Long');
                return true;
            }

            if(value != password){
                $(this).addClass('is-invalid').next('small').text('Confirm Password Do Not Matched');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#show-password').change(function (e) {
            var checked = ($(this).prop('checked'))? 'text': 'password';
            $('#current-password, #password, #confirm-password').attr('type', checked);
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }
    });
</script>