<?php


class xtcpdf extends TCPDF {
    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 9);
        $this->Cell(0, 0, 'Appendix 59', 0, 01, 'R');
        $this->Ln(5);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'REQUISITION AND ISSUE SLIP', 0, 0, 'C');
        $this->Ln(6);
    }
}

$html = '';

$pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->SetMargins(10, 35, 10, true);
$pdf->SetFont('times','',12);
$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$tr = '';
$counter = 0;
$total = 0;

foreach ($seriese->application_supplies as $application_supply){

    $tr .='<tr>
            <td align="center">'.number_format($application_supply->quantity).'</td>
            <td align="center">'.(ucwords($application_supply->supply->unit->unit)).'</td>
            <td align="center">'.number_format($application_supply->price).'</td>
            <td align="center">'.number_format($application_supply->total).'</td>
            <td align="center">'.ucwords($application_supply->supply->supply_name).'</td>
            <td align="center"></td>
            <td align="center"></td>
            </tr>
            ';
    $total +=$application_supply->total;

}

$html .= '<table width="100%">
        <tr>
        <td width="60%"><p>Entity Name: <b>'.($seriese->application->office->office).'</b></p></td>
        <td width="1%"></td>
        </tr>
        <tr>
        <td width="75%">Fund Cluster: <b>_______________________</b></td>
        <td width="40%">RIS No.: <b><u>SPLV-'.strval($seriese->no).'</u></b></td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
        <table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
        <tr>
        <th width="8%" align="center"><b>Quantity</b></th>
        <th width="5%" align="center"><b>Unit</b></th>
        <th width="20%" style="text-align: center;"><b>Amount</b></th>
        <th width="40%" align="center"><b>Item Description</b></th>
        <th width="13%" align="center"><b>Inventory Item No.</b></th>
        <th width="14.58%" align="center"><b>Estimated Useful Life</b></th>
        </tr>
        <tr>
        <th></th>
        <th></th>
        <th width="10%" align="center">Unit Cost</th>
        <th width="10%" align="center">Total Cost</th>
        <th width="40%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="13%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="14.58%"></th>
        </tr>
        '.($tr).'
        <tr>
        <td colspan="8" align="center">Nothing Follows</td>
        </tr>
        </table>
        <table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
        <tr>
        <td width="53%">Received from:</td>
        <td width="47.58%" align="left" style="border-collapse: collapse; border-left: 1px solid black;">Received by:</td>
        </tr>
        <tr>
        <td width="53%"></td>   
        <td width="53%" style="border-collapse: collapse; border-left: 1px solid black;"></td>
        </tr>
        <tr>
        <td width="53%" align="center"><b><u>JAMES PAUL R. SANTIAGO</u></b></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u></u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('signature over printed name')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">'.(ucwords('signature over printed name')).'</td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('administrative officer IV')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u></u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">Position/Office</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Position/Office</td>
        </tr>
        <tr>
        <td width="53%" align="center"><u>'.((new DateTime($seriese->created))->format('m/d/Y')).'</u></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><u>'.((new DateTime($seriese->created))->format('m/d/Y')).'</u></td>
        </tr>
        <tr>
        <td width="53%" align="center">Date</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Date</td>
        </tr>
        </table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('RIS'.(strval($seriese->no)).'.pdf', 'I');
exit(0);
