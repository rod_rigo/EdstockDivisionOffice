<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$counter = 0;
$start = 7;
$date = ((new DateTime($seriese->created))->format('m/d/Y'));

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator($auth['username'])
    ->setTitle(ucwords('ICS').strval($seriese->no))
    ->setSubject(ucwords('INVENTORY CUSTODIAN SLIP'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', ucwords('Entity Name'))
    ->setCellValue('B1', ucwords($seriese->application->office->office));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A3', ucwords('Fund Cluster'))
    ->setCellValue('B3', null)
    ->setCellValue('E3', (ucwords('ICS No')))
    ->setCellValue('F3', strval($seriese->no));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A5', ucwords('Quantity'))
    ->setCellValue('B5', ucwords('Unit'));

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('C5:D5')
    ->setCellValue('C5', ucwords('Amount'))
    ->getStyle('C5')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E5:F5')
    ->setCellValue('E5', ucwords('Item Description'))
    ->getStyle('E5')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('F5', (ucwords('Inventory Item No')))
    ->setCellValue('G5', ucwords('Estimated Useful Life'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('C6', (ucwords('Unit Cost')))
    ->setCellValue('D6', ucwords('Total Cost'));

foreach ($seriese->application_supplies as $application_supply){

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($start), intval($application_supply->quantity))
        ->setCellValue('B'.strval($start), ucwords($application_supply->supply->unit->unit))
        ->setCellValue('C'.strval($start), number_format(doubleval($application_supply->price)))
        ->setCellValue('D'.strval($start), number_format(doubleval($application_supply->total)));

    $spreadsheet->setActiveSheetIndex(0)
        ->mergeCells('E'.($start).':F'.strval($start))
        ->setCellValue('E'.strval($start), ucwords($application_supply->supply->supply_name))
        ->getStyle('E'.strval($start))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('F'.strval($start), strtoupper($application_supply->supply->supply_code))
        ->setCellValue('G'.strval($start), null);

    $start++;

}

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':G'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Nothing Follows'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
    ->setWrapText(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Received From'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
    ->setWrapText(true);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), ucwords('Received By'))
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
    ->setWrapText(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), null)
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
    ->setWrapText(true);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), null)
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
    ->setWrapText(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Signature Over Printed Name'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), ucwords('Signature Over Printed Name'))
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Position'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), ucwords('Position'))
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Position/Office'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), ucwords('Position/Office'))
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), $date)
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), $date)
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.($start).':C'.strval($start))
    ->setCellValue('A'.strval($start), ucwords('Date'))
    ->getStyle('A'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.($start).':G'.strval($start))
    ->setCellValue('E'.strval($start), ucwords('Date'))
    ->getStyle('E'.strval($start))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('ICS-'.strval($seriese->no).'.xlsx').'"');
$writer->save('php://output');
exit(0);