<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inspection[]|\Cake\Collection\CollectionInterface $inspections
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Secretary', 'controller' => 'Orders', 'action' => 'index'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="Go To Orders">
            Go To Orders
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No</th>
                        <th>Request By</th>
                        <th>Order By</th>
                        <th>Inspected By</th>
                        <th>Division</th>
                        <th>Office</th>
                        <th>Fund Cluster</th>
                        <th>Supplier</th>
                        <th>Method</th>
                        <th>Is Completed</th>
                        <th>Is Inspected</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'inspections/';
        var url = '';
        var isCompleted = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];
        var isInspected = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getInspectionsToday',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [10],
                    data: null,
                    render: function(data,type,row,meta){
                        return isCompleted[(parseInt(row.is_completed))];
                    }
                },
                {
                    targets: [11],
                    data: null,
                    render: function(data,type,row,meta){
                        return isCompleted[(parseInt(row.is_inspected))];
                    }
                },
                {
                    targets: [12],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [13],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white pdf" title="PDF">PDF</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white xlsx" title="XLSX">XLSX</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'no'},
                { data: 'request.user.username'},
                { data: 'order.user.username'},
                { data: 'user.username'},
                { data: 'division.division'},
                { data: 'office.office'},
                { data: 'fund_cluster.fund_cluster'},
                { data: 'supplier.supplier'},
                { data: 'method.method'},
                { data: 'is_completed'},
                { data: 'is_inspected'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.view',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'view/'+(parseInt(dataId));
            Swal.fire({
                title: 'View Data',
                text: 'Open In New Window?',
                icon: 'question',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                denyButtonColor: '#6e7881',
                cancelButtonText: 'No',
                denyButtonText: 'Cancel',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                    return true;
                }else if(result.isDenied){
                    Swal.close();
                    return true;
                }
                Turbolinks.visit(href,{ action:'advance' });
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'pdf/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To PDF',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        datatable.on('click','.xlsx',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'xlsx/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To Excel',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>



