<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

$counter = 0;
$start = 8;
$formatter = new NumberFormatter('en_US', NumberFormatter::SPELLOUT);
$check = WWW_ROOT. 'img'. DS. 'check.png';
$uncheck = WWW_ROOT. 'img'. DS. 'uncheck.png';

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator($auth['username'])
    ->setTitle(ucwords('PR').strval($inspection->no))
    ->setSubject(ucwords('Purchase Order'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', ucwords('Entity Name'))
    ->setCellValue('B1', (ucwords($inspection->division->division)))
    ->setCellValue('D1', ucwords('Fund Cluster'))
    ->setCellValue('E1', (ucwords($inspection->fund_cluster->fund_cluster)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A3', ucwords('Supplier'))
    ->setCellValue('B3', (ucwords($inspection->supplier->supplier)))
    ->setCellValue('D3', ucwords('IAR No'))
    ->setCellValue('E3', (strval($inspection->no)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A4', ucwords('PO No/Date'))
    ->setCellValue('B4', (strval($inspection->no)))
    ->setCellValue('D4', ucwords('Date'))
    ->setCellValue('E4', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A5', ucwords('Requisitioning Office/Dept'))
    ->setCellValue('B5', null)
    ->setCellValue('D5', ucwords('Invoice No'))
    ->setCellValue('E5', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A6', ucwords('Responsibility Center Code'))
    ->setCellValue('B6', null)
    ->setCellValue('D6', ucwords('Date'))
    ->setCellValue('E6', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(intval($start)), ucwords('Stock/Property No.'))
    ->setCellValue('B'.(intval($start)), ucwords('Unit'))
    ->setCellValue('C'.(intval($start)), ucwords('Item Description'))
    ->setCellValue('D'.(intval($start)), ucwords('Quantity'))
    ->setCellValue('E'.(intval($start)), ucwords('Unit Cost'))
    ->setCellValue('F'.(intval($start)), ucwords('Total Cost'));

foreach ($inspection->inspection_supplies as $key => $inspection_supply) {
    $counter++;
    $start++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.(strval($start)), intval($counter))
        ->setCellValue('B'.(strval($start)), $inspection_supply->supply->unit->unit)
        ->setCellValue('C'.(strval($start)), $inspection_supply->supply->supply_name)
        ->setCellValue('D'.(strval($start)), number_format(doubleval($inspection_supply->quantity),2))
        ->setCellValue('E'.(strval($start)), number_format(doubleval($inspection_supply->price),2))
        ->setCellValue('F'.(strval($start)), number_format(doubleval($inspection_supply->total),2));
}
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('E'.(strval($start)), strtoupper('Total'))
    ->setCellValue('F'.(strval($start)), number_format(doubleval($inspection->total),2));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Nothing Follows'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), strtoupper('Inspection'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('A'.(strval($start)))
    ->getFont()
    ->setItalic(true)
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('E'.(strval($start)), strtoupper('Acceptance'))
    ->getStyle('E'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('E'.(strval($start)))
    ->getFont()
    ->setItalic(true)
    ->setBold(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':B'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Date Inspected'))
    ->mergeCells('C'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('C'.(strval($start)), ucwords('Date Inspected'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':B'.(intval($start)));
$drawing = new Drawing();
$drawing->setWorksheet($spreadsheet->setActiveSheetIndex(0));
$drawing->setName('Image');
$drawing->setDescription('Image');
$drawing->setPath((boolval($inspection->is_inspected)? $check: $uncheck));
$drawing->setCoordinates('A'.(strval($start)));
$drawing->setWidth(23);
$drawing->setHeight(23);
$drawing->setOffsetX(120);
$drawing->setOffsetY(0);
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('C'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('C'.(strval($start)), ucwords('Inspected, verified and found in order as to quantity and specifications'))
    ->getStyle('C'.(strval($start)))
    ->getAlignment()
    ->setIndent(1)
    ->setWrapText(true);

$drawing = new Drawing();
$drawing->setWorksheet($spreadsheet->setActiveSheetIndex(0));
$drawing->setName('Image');
$drawing->setDescription('Image');
$drawing->setPath((boolval($inspection->is_completed)? $check: $uncheck));
$drawing->setCoordinates('E'.(strval($start)));
$drawing->setWidth(23);
$drawing->setHeight(23);
$drawing->setOffsetX(120);
$drawing->setOffsetY(0);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('F'.(strval($start)), ucwords('Completed'))
    ->getStyle('F'.(strval($start)));

$start++;
$drawing = new Drawing();
$drawing->setWorksheet($spreadsheet->setActiveSheetIndex(0));
$drawing->setName('Image');
$drawing->setDescription('Image');
$drawing->setPath($check);
$drawing->setCoordinates('E'.(strval($start)));
$drawing->setWidth(23);
$drawing->setHeight(23);
$drawing->setOffsetX(120);
$drawing->setOffsetY(0);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('F'.(strval($start)), ucwords('Partial (pls. specify quantity)'))
    ->getStyle('F'.(strval($start)))
    ->getAlignment()
    ->setIndent(1)
    ->setWrapText(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('E'.(strval($start)), '_________________')
    ->getStyle('E'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Official Name'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('A'.(strval($start)))
    ->getFont()
    ->setUnderline(true)
    ->setBold(true);
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Inspection Officer / Inspection Committee'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Official Name'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('A'.(strval($start)))
    ->getFont()
    ->setUnderline(true)
    ->setBold(true);
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Inspection Officer / Inspection Committee'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Official Name'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('A'.(strval($start)))
    ->getFont()
    ->setUnderline(true)
    ->setBold(true);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('E'.(strval($start)), ucwords('Requester Name'))
    ->getStyle('E'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('E'.(strval($start)))
    ->getFont()
    ->setUnderline(true)
    ->setBold(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Inspection Officer / Inspection Committee'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('E'.(strval($start)), ucwords('Administrative Officer IV'))
    ->getStyle('E'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('IAR-'.strval($inspection->no).'.xlsx').'"');
$writer->save('php://output');
exit(0);