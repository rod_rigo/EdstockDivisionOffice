<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$counter = 0;
$start = 13;
$formatter = new NumberFormatter('en_US', NumberFormatter::SPELLOUT);

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator($auth['username'])
    ->setTitle(ucwords('PR').strval($order->no))
    ->setSubject(ucwords('Purchase Order'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', ucwords('Entity Name'))
    ->setCellValue('B1', (ucwords($order->division->division)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A3', ucwords('Supplier'))
    ->setCellValue('B3', (ucwords($order->supplier->supplier)))
    ->setCellValue('A4', ucwords('Address'))
    ->setCellValue('B4', (ucwords($order->supplier->address)))
    ->setCellValue('A5', ucwords('TIN'))
    ->setCellValue('B5', (ucwords($order->supplier->tin_number)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('D3', ucwords('PO No'))
    ->setCellValue('E3', (strval($order->no)))
    ->setCellValue('D4', ucwords('Date'))
    ->setCellValue('E4', null)
    ->setCellValue('D5', ucwords('Mode Of Procurement'))
    ->setCellValue('E5', (ucwords($order->method->method)));

$spreadsheet->getActiveSheet()->mergeCells('A7:F7')
    ->setCellValue('A7', ucwords('Gentlemen'));
$spreadsheet->getActiveSheet()->mergeCells('A8:F8')
    ->setCellValue('A8', ucwords('Please furnish this office the following articles subject to the terms and conditions contained herein:'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A10', ucwords('Place of Delivery'))
    ->setCellValue('B10', (ucwords(strip_tags($order->place_of_delivery))))
    ->setCellValue('D10', ucwords('Delivery Term'))
    ->setCellValue('E10', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A11', ucwords('Delivery Of Delivery'))
    ->setCellValue('B11', null)
    ->setCellValue('D11', ucwords('Payment Term'))
    ->setCellValue('E11', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(intval($start)), ucwords('Stock/Property No.'))
    ->setCellValue('B'.(intval($start)), ucwords('Unit'))
    ->setCellValue('C'.(intval($start)), ucwords('Item Description'))
    ->setCellValue('D'.(intval($start)), ucwords('Quantity'))
    ->setCellValue('E'.(intval($start)), ucwords('Unit Cost'))
    ->setCellValue('F'.(intval($start)), ucwords('Total Cost'));

foreach ($order->order_supplies as $key => $order_supply) {
    $counter++;
    $start++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.(strval($start)), intval($counter))
        ->setCellValue('B'.(strval($start)), $order_supply->supply->unit->unit)
        ->setCellValue('C'.(strval($start)), $order_supply->supply->supply_name)
        ->setCellValue('D'.(strval($start)), number_format(doubleval($order_supply->quantity),2))
        ->setCellValue('E'.(strval($start)), number_format(doubleval($order_supply->price),2))
        ->setCellValue('F'.(strval($start)), number_format(doubleval($order_supply->total),2));
}
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('E'.(strval($start)), strtoupper('Total'))
    ->setCellValue('F'.(strval($start)), number_format(doubleval($order->total),2));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Nothing Follows'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('(Total Amount In Words)'))
    ->setCellValue('B'.(strval($start)), strval($formatter->format(doubleval($order->total))));

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':F'.(intval($start+1)))
    ->setCellValue('A'.(strval($start)), ucwords('In case of failure to make the full delivery within the time specified above, a penalty of one-tenth (1/10) percent for
every day of delay shall be imposed on the undelivered item/s.'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Confirm'))
    ->setCellValue('D'.(strval($start)), ucwords('Very truly yours:'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), strtoupper($order->supplier->supplier))
    ->getStyle('B'.(strval($start)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('E'.(strval($start)), null);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), ucwords('Signature over Printed Name'))
    ->setCellValue('E'.(strval($start)), ucwords(' Schools Division Superintendent'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), null)
    ->setCellValue('E'.(strval($start)), null);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), ucwords('Date'))
    ->setCellValue('E'.(strval($start)), ucwords('Date'));

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Fund Cluster'));
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), ucwords($order->fund_cluster->fund_cluster))
    ->getStyle('B'.(strval($start)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('D'.(strval($start)), ucwords('ORS/BURS No.'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Funds Available'))
    ->setCellValue('D'.(strval($start)), ucwords('Date of the ORS/BURS'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('D'.(strval($start)), ucwords('Amount'));
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('E'.(strval($start)), number_format(doubleval($order->total),2))
    ->getStyle('E'.(strval($start)))
    ->getFont()
    ->setBold(true);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.(strval($start)), ucwords('Official'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
->setCellValue('B'.(strval($start)), ucwords('Position'));

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('PR-'.strval($order->no).'.xlsx').'"');
$writer->save('php://output');
exit(0);