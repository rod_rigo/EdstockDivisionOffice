<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plan $plan
 * @var \Cake\Collection\CollectionInterface|string[] $divisions
 */
?>

<script>
    var id = parseInt(<?=intval($plan->id)?>);
    var plan = '<?=ucwords($plan->plan)?>';
</script>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Secretary', 'controller' => 'Plans', 'action' => 'index'])?>" turbolink class="btn btn-primary rounded-0" title="Return">
            Return
        </a>
    </div>

    <div class="col-sm-12 col-md-7 col-lg-7">
        <?= $this->Form->create($plan,['id' => 'form', 'type' => 'file']) ?>
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-8 col-lg-8 mt-4">
                        <?=$this->Form->label('plan', ucwords('Plan'))?>
                        <?= $this->Form->text('plan',[
                            'class' => 'form-control rounded-0',
                            'id' => 'plan',
                            'required' => true,
                            'placeholder' => ucwords('Plan'),
                            'autocomplete' => 'off',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please enter a Plan')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="active" <?=(boolval($plan->is_active)? 'checked': null);?>/>
                                    <label for="active">Set As Active</label>
                                </div>
                            </div>

                            <?= $this->Form->hidden('is_active',[
                                'id' => 'is-active',
                                'required' => true,
                                'value' => intval($plan->is_active)
                            ]);?>
                            <small></small>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('description', ucwords('Description'))?>
                        <?= $this->Form->textarea('description',[
                            'class' => 'form-control rounded-0',
                            'id' => 'description',
                            'placeholder' => ucwords('Description'),
                            'title' => ucwords('please enter a Description')
                        ]);?>
                        <small></small>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',['id' => 'user-id', 'required' => true, 'readonly' => true, 'value' => intval($auth['id'])]);?>
                <?= $this->Form->hidden('division_id',['id' => 'division-id', 'required' => true, 'readonly' => true, 'value' => intval($auth['division_id'])]);?>
                <a href="<?=$this->Url->build(['prefix' => 'Secretary', 'controller' => 'Supplies', 'action' => 'index'])?>" class="btn btn-warning rounded-0">Cancel</a>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-sm-12 col-md-5 col-lg-5">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-info-circle"></i>
                    Plan Parts & Titles
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-content-below-part-tab" data-toggle="pill" href="#custom-content-below-part" role="tab" aria-controls="custom-content-below-part" aria-selected="true">
                            Part
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-plan-title-tab" data-toggle="pill" href="#custom-content-below-plan-title" role="tab" aria-controls="custom-content-below-plan-title" aria-selected="false">
                            Title
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="custom-content-below-tabContent">
                    <div class="tab-pane fade show active" id="custom-content-below-part" role="tabpanel" aria-labelledby="custom-content-below-part-tab">
                       <div class="row">

                           <div class="modal fade" id="part-modal">
                               <div class="modal-dialog modal-lg">
                                   <?=$this->Form->create($part,['id' => 'part-form', 'type' => 'file']);?>
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                       </div>
                                       <div class="modal-body">
                                           <div class="row">
                                               <div class="col-sm-12 col-md-8 col-lg-8">
                                                   <?=$this->Form->label('part', ucwords('Part'))?>
                                                   <?=$this->Form->text('part',[
                                                       'class' => 'form-control rounded-0',
                                                       'id' => 'part',
                                                       'required' => true,
                                                       'placeholder' => ucwords('Part'),
                                                       'pattern' => '(.){1,}',
                                                       'title' => ucwords('Please Enter A Part')
                                                   ])?>
                                                   <small></small>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="modal-footer justify-content-end align-items-center">
                                           <?=$this->Form->hidden('user_id',[
                                               'id' => 'user-id',
                                               'required' => true,
                                               'readonly' => true,
                                               'value' => intval($auth['id'])
                                           ])?>
                                           <?=$this->Form->hidden('plan_id',[
                                               'id' => 'plan-id',
                                               'required' => true,
                                               'readonly' => true,
                                               'value' => intval($plan->id)
                                           ])?>
                                           <?=$this->Form->button(ucwords('Reset'),[
                                               'class' => 'btn btn-danger rounded-0',
                                               'type' => 'reset'
                                           ])?>
                                           <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                                           <?=$this->Form->button(ucwords('Submit'),[
                                               'class' => 'btn btn-success rounded-0',
                                               'type' => 'submit'
                                           ])?>
                                       </div>
                                   </div>
                                   <?=$this->Form->end();?>
                               </div>
                           </div>

                           <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3 mt-3">
                               <button type="button" id="part-toggle-modal" class="btn btn-primary rounded-0" title="New Plan Part">
                                   New Plan Part
                               </button>
                           </div>
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="table-responsive">
                                   <table id="part-datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                       <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>Part</th>
                                           <th>Modified By</th>
                                           <th>Modified</th>
                                           <th>Options</th>
                                       </tr>
                                       </thead>
                                   </table>
                               </div>
                           </div>
                       </div>
                    </div>
                    <div class="tab-pane fade" id="custom-content-below-plan-title" role="tabpanel" aria-labelledby="custom-content-below-plan-title-tab">
                        <div class="row">

                            <div class="modal fade" id="plan-title-modal">
                                <div class="modal-dialog modal-lg">
                                    <?=$this->Form->create($planTitle,['id' => 'plan-title-form', 'type' => 'file']);?>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <?=$this->Form->label('part_id', ucwords('Part'))?>
                                                    <?=$this->Form->select('part_id', $parts,[
                                                        'class' => 'form-control rounded-0',
                                                        'id' => 'part-id',
                                                        'required' => true,
                                                        'empty' => ucwords('Select A Part'),
                                                    ])?>
                                                    <small></small>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <?=$this->Form->label('plan_title', ucwords('Plan Title'))?>
                                                    <?=$this->Form->text('plan_title',[
                                                        'class' => 'form-control rounded-0',
                                                        'id' => 'plan-title',
                                                        'required' => true,
                                                        'placeholder' => ucwords('Plan Title'),
                                                        'pattern' => '(.){1,}',
                                                        'title' => ucwords('Please Enter A Plan Title')
                                                    ])?>
                                                    <small></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-end align-items-center">
                                            <?=$this->Form->hidden('user_id',[
                                                'id' => 'user-id',
                                                'required' => true,
                                                'readonly' => true,
                                                'value' => intval($auth['id'])
                                            ])?>
                                            <?=$this->Form->hidden('plan_id',[
                                                'id' => 'plan-id',
                                                'required' => true,
                                                'readonly' => true,
                                                'value' => intval($plan->id)
                                            ])?>
                                            <?=$this->Form->button(ucwords('Reset'),[
                                                'class' => 'btn btn-danger rounded-0',
                                                'type' => 'reset'
                                            ])?>
                                            <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                                            <?=$this->Form->button(ucwords('Submit'),[
                                                'class' => 'btn btn-success rounded-0',
                                                'type' => 'submit'
                                            ])?>
                                        </div>
                                    </div>
                                    <?=$this->Form->end();?>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3 mt-3">
                                <button type="button" id="plan-title-toggle-modal" class="btn btn-primary rounded-0" title="New Plan Title">
                                    New Plan Title
                                </button>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table id="plan-title-datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Part</th>
                                            <th>Plan Title</th>
                                            <th>Modified By</th>
                                            <th>Modified</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="modal fade" id="plan-detail-modal" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-xl">
                                <?=$this->Form->create($planDetail,['id' => 'plan-detail-form', 'type' => 'file']);?>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <?=$this->Form->label('part_id', ucwords('Part'))?>
                                                <?=$this->Form->select('part_id', $parts,[
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'plan-detail-part-id',
                                                    'required' => true,
                                                    'empty' => ucwords('Select A Part'),
                                                ])?>
                                                <small></small>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <?=$this->Form->label('plan_title_id', ucwords('Plan Title'))?>
                                                <?=$this->Form->select('plan_title_id', [], [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'plan-title-id',
                                                    'required' => true,
                                                    'empty' => ucwords('choose part'),
                                                    'title' => ucwords('choose part')
                                                ])?>
                                                <small></small>
                                            </div>
                                            <div class="col-sm-12 col-md-7 col-lg-7 mt-2">

                                                <?=$this->Form->label('supply_name', ucwords('Supply Name'))?>
                                                <div class="input-group">
                                                    <?=$this->Form->text('supply_name', [
                                                        'class' => 'form-control rounded-0',
                                                        'id' => 'supply-name',
                                                        'required' => true,
                                                        'autocomplete' => 'off',
                                                        'pattern' => '(.){1,}',
                                                        'placeholder' => ucwords('Supply Name'),
                                                        'title' => ucwords('please enter a supply name')
                                                    ])?>
                                                    <div class="input-group-append rounded-0 border-info" id="reload-supplies-list">
                                                        <span class="input-group-text rounded-0 bg-info border-info">
                                                            <i class="fas fa-refresh"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-3 col-lg-3 mt-2">

                                                <?=$this->Form->label('supply_code', ucwords('Supply Code'))?>
                                                <?=$this->Form->text('supply_code', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'supply-code',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Supply Code'),
                                                    'title' => ucwords('please enter a supply Code'),
                                                    'readonly' => true,
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-2 col-lg-2 mt-2">

                                                <?=$this->Form->label('price', ucwords('price'))?>
                                                <?=$this->Form->number('price', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'price',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Supply Code'),
                                                    'title' => ucwords('please a price'),
                                                    'min' => 0,
                                                    'value' => 0,
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('january', ucwords('january'))?>
                                                <?=$this->Form->number('january', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'january',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('january'),
                                                    'title' => ucwords('please enter Quantity for january'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('february', ucwords('february'))?>
                                                <?=$this->Form->number('february', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'february',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('february'),
                                                    'title' => ucwords('please enter Quantity for february'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('march', ucwords('march'))?>
                                                <?=$this->Form->number('march', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'march',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('march'),
                                                    'title' => ucwords('please enter Quantity for march'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">

                                                <?=$this->Form->label('quarter_one', ucwords('Quarter One'))?>
                                                <?=$this->Form->number('quarter_one', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'quarter-one',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Quarter One'),
                                                    'title' => ucwords('please enter Quantity for quarter one'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('april', ucwords('april'))?>
                                                <?=$this->Form->number('april', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'april',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('april'),
                                                    'title' => ucwords('please enter Quantity for april'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('may', ucwords('may'))?>
                                                <?=$this->Form->number('may', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'may',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('may'),
                                                    'title' => ucwords('please enter Quantity for may'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('june', ucwords('june'))?>
                                                <?=$this->Form->number('june', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'june',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('june'),
                                                    'title' => ucwords('please enter Quantity for june'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">

                                                <?=$this->Form->label('quarter_two', ucwords('Quarter two'))?>
                                                <?=$this->Form->number('quarter_two', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'quarter-two',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Quarter two'),
                                                    'title' => ucwords('please enter Quantity for quarter two'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('july', ucwords('july'))?>
                                                <?=$this->Form->number('july', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'july',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('july'),
                                                    'title' => ucwords('please enter Quantity for july'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('august', ucwords('august'))?>
                                                <?=$this->Form->number('august', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'august',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('august'),
                                                    'title' => ucwords('please enter Quantity for august'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('september', ucwords('september'))?>
                                                <?=$this->Form->number('september', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'september',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('september'),
                                                    'title' => ucwords('please enter Quantity for september'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">

                                                <?=$this->Form->label('quarter_three', ucwords('Quarter three'))?>
                                                <?=$this->Form->number('quarter_three', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'quarter-three',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Quarter three'),
                                                    'title' => ucwords('please enter Quantity for quarter three'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('october', ucwords('october'))?>
                                                <?=$this->Form->number('october', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'october',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('october'),
                                                    'title' => ucwords('please enter Quantity for october'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('november', ucwords('november'))?>
                                                <?=$this->Form->number('november', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'november',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('november'),
                                                    'title' => ucwords('please enter Quantity for november'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 mt-2">

                                                <?=$this->Form->label('december', ucwords('december'))?>
                                                <?=$this->Form->number('december', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'december',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('december'),
                                                    'title' => ucwords('please enter Quantity for december'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">

                                                <?=$this->Form->label('quarter_four', ucwords('Quarter four'))?>
                                                <?=$this->Form->number('quarter_four', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'quarter-four',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('Quarter four'),
                                                    'title' => ucwords('please enter Quantity for quarter four'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-6 col-lg-6 mt-2">

                                                <?=$this->Form->label('quantity', ucwords('quantity'))?>
                                                <?=$this->Form->number('quantity', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'quantity',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('quantity'),
                                                    'title' => ucwords('please enter a Quantity'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                            <div class="col-sm-12 col-md-6 col-lg-6 mt-2">

                                                <?=$this->Form->label('total', ucwords('total'))?>
                                                <?=$this->Form->number('total', [
                                                    'class' => 'form-control rounded-0',
                                                    'id' => 'total',
                                                    'required' => true,
                                                    'autocomplete' => 'off',
                                                    'pattern' => '(.){1,}',
                                                    'placeholder' => ucwords('total'),
                                                    'title' => ucwords('please enter a total'),
                                                    'value' => intval(0),
                                                    'min' => intval(0),
                                                    'readonly' => true
                                                ])?>
                                                <small></small>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-end align-items-center">
                                        <?=$this->Form->hidden('user_id',[
                                            'id' => 'user-id',
                                            'required' => true,
                                            'readonly' => true,
                                            'value' => intval($auth['id'])
                                        ])?>
                                        <?=$this->Form->hidden('supply_id',[
                                            'id' => 'supply-id',
                                            'required' => true,
                                            'readonly' => true,
                                            'value' => uniqid()
                                        ])?>
                                        <?=$this->Form->hidden('plan_id',[
                                            'id' => 'plan-id',
                                            'required' => true,
                                            'readonly' => true,
                                            'value' => intval($plan->id)
                                        ])?>
                                        <?=$this->Form->button(ucwords('Reset'),[
                                            'class' => 'btn btn-danger rounded-0',
                                            'type' => 'reset'
                                        ])?>
                                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                                        <?=$this->Form->button(ucwords('Submit'),[
                                            'class' => 'btn btn-success rounded-0',
                                            'type' => 'submit'
                                        ])?>
                                    </div>
                                </div>
                                <?=$this->Form->end();?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3 mt-3">
                            <button type="button" id="plan-detail-toggle-modal" class="btn btn-primary rounded-0" title="New Plan Detail">
                                New Plan Detail
                            </button>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3 mt-3">
                            <div class="table-responsive">
                                <div id="spreadsheet">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">

            </div>
        </div>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = mainurl+'plans/';
        var editor;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl+'edit/'+(parseInt(id)),
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(window.location.href, {action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#plan').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Plan');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#active').change(function (e) {
            var checked = $(this).prop('checked');
            $('#is-active').val(Number(checked));
        });

        CKEDITOR.ClassicEditor.create(document.getElementById('description'), {
            // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
            toolbar: {
                items: [
                    'exportPDF','exportWord', '|',
                    'findAndReplace', 'selectAll', '|',
                    'heading', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'outdent', 'indent', '|',
                    'undo', 'redo',
                    '-',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                    'alignment', '|',
                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                    'specialCharacters', 'horizontalLine', 'pageBreak',
                ],
                shouldNotGroupWhenFull: true
            },
            // Changing the language of the interface requires loading the language file using the <script> tag.
            // language: 'es',
            list: {
                properties: {
                    styles: true,
                    startIndex: true,
                    reversed: true
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                    { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                    { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                    { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                    { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                ]
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
            placeholder: 'Description',
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
            fontFamily: {
                options: [
                    'default',
                    'Arial, Helvetica, sans-serif',
                    'Courier New, Courier, monospace',
                    'Georgia, serif',
                    'Lucida Sans Unicode, Lucida Grande, sans-serif',
                    'Tahoma, Geneva, sans-serif',
                    'Times New Roman, Times, serif',
                    'Trebuchet MS, Helvetica, sans-serif',
                    'Verdana, Geneva, sans-serif'
                ],
                supportAllValues: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
            fontSize: {
                options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                supportAllValues: true
            },
            // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
            // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
            htmlSupport: {
                allow: [
                    {
                        name: /.*/,
                        attributes: true,
                        classes: true,
                        styles: true
                    }
                ]
            },
            // Be careful with enabling previews
            // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
            htmlEmbed: {
                showPreviews: true
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
            link: {
                decorators: {
                    addTargetToExternalLinks: true,
                    defaultProtocol: 'https://',
                    toggleDownloadable: {
                        mode: 'manual',
                        label: 'Downloadable',
                        attributes: {
                            download: 'file'
                        }
                    }
                }
            },
            // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
            mention: {
                feeds: [
                    {
                        marker: '@',
                        feed: [
                            '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                            '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                            '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                            '@sugar', '@sweet', '@topping', '@wafer'
                        ],
                        minimumCharacters: 1
                    }
                ]
            },
            // The "super-build" contains more premium features that require additional configuration, disable them below.
            // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
            removePlugins: [
                // These two are commercial, but you can try them out without registering to a trial.
                // 'ExportPdf',
                // 'ExportWord',
                'CKBox',
                'CKFinder',
                'EasyImage',
                // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                // Storing images as Base64 is usually a very bad idea.
                // Replace it on production website with other solutions:
                // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                // 'Base64UploadAdapter',
                'RealTimeCollaborativeComments',
                'RealTimeCollaborativeTrackChanges',
                'RealTimeCollaborativeRevisionHistory',
                'PresenceList',
                'Comments',
                'TrackChanges',
                'TrackChangesData',
                'RevisionHistory',
                'Pagination',
                'WProofreader',
                // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                // from a local file system (file://) - load this site via HTTP server if you enable MathType
                'MathType'
            ]
        }).then( function (data) {
            editor = data;
        }).catch(function (error) {
            window.location.reload();
        });

        setInterval(function(){
            editor.updateSourceElement();
        }, 1000);

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
    $(document).ready(function (e) {
        const baseurl = mainurl+'parts/';
        var url = '';

        var datatable = $('#part-datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getParts/'+(parseInt(id)),
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [3],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [4],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'part'},
                { data: 'user.username'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+(parseInt(dataId));
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    url = 'edit/'+(parseInt(dataId));
                    $('button[type="reset"]').fadeOut(100);
                },
            }).done(function (data, status, xhr) {
                $('#part').val(data.part);
                $('#part-modal').modal('toggle');
                Swal.close();
            }).fail(function (data, status, xhr) {
                swal('error', 'Error', data.responseJSON.message);
            });

        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        $('#part-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl + url,
                type: 'POST',
                fund_cluster: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#part-form')[0].reset();
                $('#part-modal').modal('toggle');
                table.ajax.reload(null, false);
                swal('success', null, data.message);
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#part-toggle-modal').click(function (e) {
            url = 'add';
            $('#part-modal').modal('toggle');
        });

        $('#part-modal').on('hidden.bs.modal', function (e) {
            $('#part-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#part').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Part');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
    $(document).ready(function (e) {
        const baseurl = mainurl+'plan-titles/';
        var url = '';

        var datatable = $('#plan-title-datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getPlanTitles/'+(parseInt(id)),
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [4],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [5],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'part.part'},
                { data: 'plan_title'},
                { data: 'user.username'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+(parseInt(dataId));
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    url = 'edit/'+(parseInt(dataId));
                    $('button[type="reset"]').fadeOut(100);
                },
            }).done(function (data, status, xhr) {
                $('#part-id').val(parseInt(data.part_id));
                $('#plan-title').val(data.plan_title);
                $('#plan-title-modal').modal('toggle');
                Swal.close();
            }).fail(function (data, status, xhr) {
                swal('error', 'Error', data.responseJSON.message);
            });

        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        $('#plan-title-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl + url,
                type: 'POST',
                fund_cluster: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#plan-title-form')[0].reset();
                $('#plan-title-modal').modal('toggle');
                table.ajax.reload(null, false);
                swal('success', null, data.message);
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            });
        });

        $('#plan-title-toggle-modal').click(function (e) {
            url = 'add';
            $('#plan-title-modal').modal('toggle');
        });

        $('#plan-title-modal').on('hidden.bs.modal', function (e) {
            $('#plan-title-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#plan-title').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Plan Title');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
    $(document).ready(function (e) {
        const baseurl = mainurl+'plan-details/';
        var url = '';
        
        var sheet = [
            [
                'Supplies & Specifications',
                '',
                '',
                'Unit of Measure',
                'Monthly Quantity Requirement',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                'Total Quantity For The Year',
                'Price',
                'Total Amount For The Year',
            ],
            [],
            [
                '',
                '',
                '',
                '',
                'Jan',
                'Feb',
                'Mar',
                'Q1',
                'Q1 Amount',
                'Apr',
                'May',
                'Jun',
                'Q2',
                'Q2 Amount',
                'Jul',
                'Aug',
                'Sep',
                'Q3',
                'Q3 Amount',
                'Oct',
                'Nov',
                'Dec',
                'Q4',
                'Q4 Amount',
                '',
                '',
                ''
            ],
            []
        ];

        var table = jexcel(document.getElementById('spreadsheet'),{
            data: sheet,
            minDimensions:[26, 50],
            filters: true,
            allowComments:true,
            tableOverflow: false,
            lazyLoading: true,
            tableWidth: '100%',
            tableHeight: '100em',
            defaultColWidth: 100,
            freezeColumns: 0,
            columnDrag:true,
            columnSorting:true,
            rowResize: true,
            loadingSpin:true,
            csvFileName: plan,
            includeHeadersOnDownload: false,
            search: true,
            columns: [
                {
                    type: 'text',
                    wordWrap:true,

                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,

                },
                {
                    type: 'text',
                    wordWrap:true,
                    width:'200'
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,

                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,

                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                },
                {
                    type: 'text',
                    wordWrap:true,
                    width:'400',
                },
                {
                    type: 'text',
                    wordWrap:true,
                    width:'400',
                },
                {
                    type: 'text',
                    wordWrap:true,
                    width:'400',
                },
                {
                    type: 'html',
                    wordWrap:true,
                    width:'400',
                },
                {
                    type: 'html',
                    wordWrap:true,
                    width:'400',
                },
            ],
            nestedHeaders:[
                [
                    {
                        title: plan,
                        colspan: 1000,
                    },
                ],
            ],
            mergeCells:{
                A1:[3,4],
                D1:[1,4],
                E1:[20,2],
                E3:[1,2],
                F3:[1,2],
                G3:[1,2],
                H3:[1,2],
                I3:[1,2],
                J3:[1,2],
                K3:[1,2],
                L3:[1,2],
                M3:[1,2],
                N3:[1,2],
                O3:[1,2],
                P3:[1,2],
                Q3:[1,2],
                R3:[1,2],
                S3:[1,2],
                T3:[1,2],
                U3:[1,2],
                V3:[1,2],
                W3:[1,2],
                X3:[1,2],
                Y1:[1,4],
                Z1:[1,4],
                AA1:[1,4],
            },
            footers: [
                ['Total','=SUM(I1:I100)']
            ],
            toolbar:[
                {
                    type: 'i',
                    content: 'undo',
                    onclick: function() {
                        table.undo();
                    }
                },
                {
                    type: 'i',
                    content: 'redo',
                    onclick: function() {
                        table.redo();
                    }
                },
                {
                    type: 'i',
                    content: 'save',
                    onclick: function () {
                        Swal.fire({
                            title: 'Save As CSV',
                            text: 'Are You Sure?',
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                table.download(false);
                            }
                        });
                    }
                },
                {
                    type: 'i',
                    content: 'data_object',
                    onclick: function () {
                        var blob = new Blob([JSON.stringify(table.getData())], {
                            type: 'application/json'
                        });
                        Swal.fire({
                            title: 'Download As JSON',
                            text: 'Are You Sure?',
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                saveAs(blob, (plan)+'.json');
                            }
                        });
                    }
                },
                {
                    type: 'select',
                    k: 'font-family',
                    v: ['Arial','Verdana']
                },
                {
                    type: 'select',
                    k: 'font-size',
                    v: ['9px','10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px']
                },
                {
                    type: 'i',
                    content: 'format_align_left',
                    k: 'text-align',
                    v: 'left'
                },
                {
                    type:'i',
                    content:'format_align_center',
                    k:'text-align',
                    v:'center'
                },
                {
                    type: 'i',
                    content: 'format_align_right',
                    k: 'text-align',
                    v: 'right'
                },
                {
                    type: 'i',
                    content: 'format_bold',
                    k: 'font-weight',
                    v: 'bold'
                },
                {
                    type: 'color',
                    content: 'format_color_text',
                    k: 'color'
                },
                {
                    type: 'color',
                    content: 'format_color_fill',
                    k: 'background-color'
                },
                {
                    type: 'i',
                    content: 'open_in_full',
                    k: 'toggle-screen',
                    id: 'toggle-screen',
                    v: 'center',
                    onclick: function () {
                        if($('#toggle-screen').hasClass('is-full-screen')){
                            table.fullscreen(false);
                            $('#toggle-screen').text('open_in_full').removeClass('is-full-screen');
                        }else{
                            table.fullscreen(true);
                            $('#toggle-screen').text('close_fullscreen').addClass('is-full-screen');
                        }
                    }
                },
            ],
            contextMenu: function(obj, x, y, e) {
                var items = [];

                if (y == null) {
                    // Insert a new column
                    if (obj.options.allowInsertColumn == true) {
                        items.push({
                            title:obj.options.text.insertANewColumnBefore,
                            onclick:function() {
                                obj.insertColumn(1, parseInt(x), 1);
                            }
                        });
                    }

                    if (obj.options.allowInsertColumn == true) {
                        items.push({
                            title:obj.options.text.insertANewColumnAfter,
                            onclick:function() {
                                obj.insertColumn(1, parseInt(x), 0);
                            }
                        });
                    }

                    // Delete a column
                    if (obj.options.allowDeleteColumn == true) {
                        items.push({
                            title:obj.options.text.deleteSelectedColumns,
                            onclick:function() {
                                obj.deleteColumn(obj.getSelectedColumns().length ? undefined : parseInt(x));
                            }
                        });
                    }

                    // Rename column
                    if (obj.options.allowRenameColumn == true) {
                        items.push({
                            title:obj.options.text.renameThisColumn,
                            onclick:function() {
                                obj.setHeader(x);
                            }
                        });
                    }

                    // Sorting
                    if (obj.options.columnSorting == true) {
                        // Line
                        items.push({ type:'line' });

                        items.push({
                            title:obj.options.text.orderAscending,
                            onclick:function() {
                                obj.orderBy(x, 0);
                            }
                        });
                        items.push({
                            title:obj.options.text.orderDescending,
                            onclick:function() {
                                obj.orderBy(x, 1);
                            }
                        });
                    }
                } else {
                    // Insert new row
                    if (obj.options.allowInsertRow == true) {
                        items.push({
                            title:obj.options.text.insertANewRowBefore,
                            onclick:function() {
                                obj.insertRow(1, parseInt(y), 1);
                            }
                        });

                        items.push({
                            title:obj.options.text.insertANewRowAfter,
                            onclick:function() {
                                obj.insertRow(1, parseInt(y));
                            }
                        });
                    }

                    if (obj.options.allowDeleteRow == true) {
                        items.push({
                            title:obj.options.text.deleteSelectedRows,
                            onclick:function() {
                                obj.deleteRow(obj.getSelectedRows().length ? undefined : parseInt(y));
                            }
                        });
                    }

                    if (x) {
                        if (obj.options.allowComments == true) {
                            items.push({ type:'line' });

                            var title = obj.records[y][x].getAttribute('title') || '';

                            items.push({
                                title: title ? obj.options.text.editComments : obj.options.text.addComments,
                                onclick:function() {
                                    obj.setComments([ x, y ], prompt(obj.options.text.comments, title));
                                }
                            });

                            if (title) {
                                items.push({
                                    title:obj.options.text.clearComments,
                                    onclick:function() {
                                        obj.setComments([ x, y ], '');
                                    }
                                });
                            }
                        }
                    }
                }

                // Line
                items.push({ type:'line' });

                // Save
                if (obj.options.allowExport) {
                    items.push({
                        title: obj.options.text.saveAs,
                        shortcut: 'Ctrl + S',
                        onclick: function () {
                            obj.download();
                        }
                    });
                }

                // About
                if (obj.options.about) {
                    items.push({
                        title:obj.options.text.about,
                        onclick:function() {
                            alert(obj.options.about);
                        }
                    });
                }

                return items;
            },
            updateTable:function(instance, cell, col, row, val, label, cellName) {
                if(col == parseInt(27)){

                    if(parseInt(val)){
                        $(cell).html('<button class="btn btn-primary rounded-0 edit" type="button" data-id="'+(parseInt(val))+'">Edit</button> | '+
                        '<button class="btn btn-danger rounded-0 delete" type="button" data-id="'+(parseInt(val))+'">Delete</button>');
                    }

                }
            },
        });

        $(document).on('click', 'button.edit', function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+(parseInt(dataId));
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    url = 'edit/'+(parseInt(dataId));
                    $('button[type="reset"]').fadeOut(100);
                },
            }).done(function (data, status, xhr) {
                $('#plan-detail-part-id').val(parseInt(data.part_id));
                getPlanTitlesList(parseInt(data.part_id));
                $('#supply-name').val(data.supply.supply_name);
                $('#supply-code').val(data.supply_code);
                $('#supply-id').val(parseInt(data.supply_id));
                $('#price').val(parseFloat(data.price));

                $('#january').val(parseInt(data.january));
                $('#february').val(parseInt(data.february));
                $('#march').val(parseInt(data.march));
                $('#quarter-one').val(parseInt(data.quarter_one));

                $('#april').val(parseInt(data.april));
                $('#may').val(parseInt(data.may));
                $('#june').val(parseInt(data.june));
                $('#quarter-two').val(parseInt(data.quarter_two));

                $('#july').val(parseInt(data.july));
                $('#august').val(parseInt(data.august));
                $('#september').val(parseInt(data.september));
                $('#quarter-three').val(parseInt(data.quarter_three));

                $('#november').val(parseInt(data.november));
                $('#october').val(parseInt(data.october));
                $('#december').val(parseInt(data.december));
                $('#quarter-four').val(parseInt(data.quarter_four));

                setTimeout(function () {
                    $('#plan-title-id').val(parseInt(data.plan_title_id));
                }, 1000);

                $('#quantity').val(parseFloat(data.quantity));
                $('#total').val(parseFloat(data.total));
                $('#plan-detail-modal').modal('toggle');
                Swal.close();
            }).fail(function (data, status, xhr) {
                swal('error', 'Error', data.responseJSON.message);
            });
        });

        $(document).on('click','button.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        getPlanDetails();
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        $('#plan-detail-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl + url,
                type: 'POST',
                fund_cluster: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#plan-detail-form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                getPlanDetails();
                swal('success', null, data.message);
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            });
        });

        $('#plan-detail-toggle-modal').click(function (e) {
            url = 'add';
            $('#plan-detail-modal').modal('toggle');
        });

        $('#plan-detail-modal').on('hidden.bs.modal', function (e) {
            $('#plan-detail-form')[0].reset();
            $('small').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('#supply-name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').parent().next('small').text('Please Enter A Supply Name');
                return true;
            }

            $(this).removeClass('is-invalid').parent().next('small').empty();
        });

        $('#supply-code').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Supply Code');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('input[type="number"]').keypress(function (e) {
            var key = e.key;
            var regex = /^([0-9]){1,}$/;

            if(!key.match(regex)){
                e.preventDefault();
            }

        }).blur(function (e) {
            var value = $(this).val();

            if(parseInt(value.length) == parseInt(0)){
                $(this).val(parseInt(0));
            }

            $(this).removeClass('is-invalid').next('small').empty();

        }).not('#price').on('input', function (e) {
            var value = $(this).val();
            var name = $(this).attr('name');
            var label = $('label[for="'+(name)+'"]').text();
            var regex = /^([0-9]){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Quantity For '+(label));
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#price').on('input', function (e) {
            var value = $(this).val();
            var regex = /^([0-9]){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please A Price');
                return true;
            }

            computation();
            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#plan-detail-part-id').change(function (e) {
            var value = $(this).val();
            getPlanTitlesList(parseInt(value));
        });

        $('#january, #february, #march').on('input', function (e) {
            var january = ($('#january').val() ? parseFloat($('#january').val()): parseFloat(0));
            var february = ($('#february').val() ? parseFloat($('#february').val()): parseFloat(0));
            var march = ($('#march').val() ? parseFloat($('#march').val()): parseFloat(0));
            var quantity = (january + february + march);
            $('#quarter-one').val(parseFloat(quantity));
            computation();
        });

        $('#april, #may, #june').on('input', function (e) {
            var april = ($('#april').val() ? parseFloat($('#april').val()): parseFloat(0));
            var may = ($('#may').val() ? parseFloat($('#may').val()): parseFloat(0));
            var june = ($('#june').val() ? parseFloat($('#june').val()): parseFloat(0));
            var quantity = (april + may + june);
            $('#quarter-two').val(parseFloat(quantity));
            computation();
        });

        $('#july, #august, #september').on('input', function (e) {
            var july = ($('#july').val() ? parseFloat($('#july').val()): parseFloat(0));
            var august = ($('#august').val() ? parseFloat($('#august').val()): parseFloat(0));
            var september = ($('#september').val() ? parseFloat($('#september').val()): parseFloat(0));
            var quantity = (july + august + september);
            $('#quarter-three').val(parseFloat(quantity));
            computation();
        });

        $('#october, #november, #december').on('input', function (e) {
            var october = ($('#october').val() ? parseFloat($('#october').val()): parseFloat(0));
            var november = ($('#november').val() ? parseFloat($('#november').val()): parseFloat(0));
            var december = ($('#december').val() ? parseFloat($('#december').val()): parseFloat(0));
            var quantity = (october + november + december);
            $('#quarter-four').val(parseFloat(quantity));
            computation();
        });

        $('#reload-supplies-list').click(function (e) {
            getSuppliesList();
        });

        function computation() {
            var price = ($('#price').val() ? parseFloat($('#price').val()): parseFloat(0));
            var january = ($('#january').val() ? parseFloat($('#january').val()): parseFloat(0));
            var february = ($('#february').val() ? parseFloat($('#february').val()): parseFloat(0));
            var march = ($('#march').val() ? parseFloat($('#march').val()): parseFloat(0));
            var april = ($('#april').val() ? parseFloat($('#april').val()): parseFloat(0));
            var may = ($('#may').val() ? parseFloat($('#may').val()): parseFloat(0));
            var june = ($('#june').val() ? parseFloat($('#june').val()): parseFloat(0));
            var july = ($('#july').val() ? parseFloat($('#july').val()): parseFloat(0));
            var august = ($('#august').val() ? parseFloat($('#august').val()): parseFloat(0));
            var september = ($('#september').val() ? parseFloat($('#september').val()): parseFloat(0));
            var october = ($('#october').val() ? parseFloat($('#october').val()): parseFloat(0));
            var november = ($('#november').val() ? parseFloat($('#november').val()): parseFloat(0));
            var december = ($('#december').val() ? parseFloat($('#december').val()): parseFloat(0));
            var quantity = january + february + march + april + may + june + july + august + september + october + november + december;
            var total = parseFloat(quantity) * parseFloat(price);
            $('#quantity').val(parseInt(quantity));
            $('#total').val(parseFloat(total));
        }

        function getSuppliesList() {
            $.ajax({
                url:mainurl+'supplies/getSuppliesList',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#supply-name').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#supply-name').autocomplete({
                    delay: 100,
                    minLength: 2,
                    autoFocus: true,
                    source: function (request, response) {
                        response($.map(data, function (data, key) {
                            var name = data.supply_name.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    key: parseInt(data.id),
                                    label: data.supply_name,
                                    price: parseFloat(data.price),
                                    supply_code: data.supply_code,
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    create: function (event, ui) {
                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                            return $('<li></li>')
                                .data('item.autocomplete', item)
                                .append('<li>'+(label)+'</li>')
                                .appendTo(ul);
                        };
                    },
                    focus: function(e, ui) {
                        e.preventDefault();
                    },
                    select: function(e, ui) {
                        $('#supply-name').val(ui.item.label);
                        $('#supply-id').val(parseInt(ui.item.key));
                        $('#price').val(parseFloat(ui.item.price));
                        $('#supply-code').val(ui.item.supply_code);
                        computation();
                    },
                    change: function(e, ui ) {
                        e.preventDefault();
                    },
                }).prop('disabled', false);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function getPlanTitlesList(partId) {
            $.ajax({
                url:mainurl+'plan-titles/getPlanTitlesList/'+(parseInt(id))+'/'+(parseInt(partId)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#plan-title-id').empty().append('<option value="">Please Wait</option>').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                var planTitleId =  $('#plan-title-id').empty().append('<option value="">Select Plan Title</option>');
                $.map(data, function (data, key) {
                    planTitleId.append('<option value="'+(parseInt(key))+'">'+(data)+'</option>')
                });
                planTitleId.prop('disabled', false);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }
        
        function getPlanDetails() {
            var array = [
                [
                    'Supplies & Specifications',
                    '',
                    '',
                    'Unit of Measure',
                    'Monthly Quantity Requirement',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    'Total Quantity For The Year',
                    'Price',
                    'Total Amount For The Year',
                ],
                [],
                [
                    '',
                    '',
                    '',
                    '',
                    'Jan',
                    'Feb',
                    'Mar',
                    'Q1',
                    'Q1 Amount',
                    'Apr',
                    'May',
                    'Jun',
                    'Q2',
                    'Q2 Amount',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Q3',
                    'Q3 Amount',
                    'Oct',
                    'Nov',
                    'Dec',
                    'Q4',
                    'Q4 Amount',
                    '',
                    '',
                    ''
                ],
                [],
            ];
            var cell = array.length;
            var counter = 0;
            var styles = '';
            $.ajax({
                url:mainurl+'parts/getPartsList/'+(parseInt(id)),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
//                    table.destroyMerged();
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data, key) {
                    cell++;
                    array.push([
                        data.part
                    ]);
                    table.removeMerge('A'+(cell.toString()), 28, 1);
                    table.setMerge('A'+(cell.toString()), 28, 1);
                    styles += '"A'+(cell.toString())+'":"text-align: left; background-color: #DADADA",';
                    $.map(data.plan_titles, function (data, key) {
                        cell++;
                        array.push([
                            data.plan_title
                        ]);
                        table.removeMerge('A'+(cell.toString()), 28, 1);
                        table.setMerge('A'+(cell.toString()), 28, 1);
                        styles += '"A'+(cell.toString())+'":"text-align: left; background-color: #DADADA",';
                        $.map(data.plan_details, function (data, key) {
                            counter++;
                            cell++;
                            var price = parseFloat(data.price);
                            var quarter_one_total = (parseInt(data.january) + parseInt(data.february) + parseInt(data.march)) * price;
                            var quarter_two_total = (parseInt(data.april) + parseInt(data.may) + parseInt(data.june)) * price;
                            var quarter_three_total = (parseInt(data.july) + parseInt(data.august) + parseInt(data.september)) * price;
                            var quarter_four_total = (parseInt(data.october) + parseInt(data.november) + parseInt(data.december)) * price;
                            array.push([
                                counter,
                                data.supply_code,
                                data.supply.supply_name,
                                data.supply.unit.unit,
                                (new Intl.NumberFormat().format(parseInt(data.january))),
                                (new Intl.NumberFormat().format(parseInt(data.february))),
                                (new Intl.NumberFormat().format(parseInt(data.march))),
                                (new Intl.NumberFormat().format(parseInt(data.quarter_one))),
                                (new Intl.NumberFormat().format(parseFloat(quarter_one_total))),
                                (new Intl.NumberFormat().format(parseInt(data.april))),
                                (new Intl.NumberFormat().format(parseInt(data.may))),
                                (new Intl.NumberFormat().format(parseInt(data.june))),
                                (new Intl.NumberFormat().format(parseInt(data.quarter_two))),
                                (new Intl.NumberFormat().format(parseFloat(quarter_two_total))),
                                (new Intl.NumberFormat().format(parseInt(data.july))),
                                (new Intl.NumberFormat().format(parseInt(data.august))),
                                (new Intl.NumberFormat().format(parseInt(data.september))),
                                (new Intl.NumberFormat().format(parseInt(data.quarter_three))),
                                (new Intl.NumberFormat().format(parseFloat(quarter_three_total))),
                                (new Intl.NumberFormat().format(parseInt(data.october))),
                                (new Intl.NumberFormat().format(parseInt(data.november))),
                                (new Intl.NumberFormat().format(parseInt(data.december))),
                                (new Intl.NumberFormat().format(parseInt(data.quarter_four))),
                                (new Intl.NumberFormat().format(parseFloat(quarter_four_total))),
                                (new Intl.NumberFormat().format(parseFloat(data.quantity))),
                                (new Intl.NumberFormat().format(parseFloat(data.price))),
                                (new Intl.NumberFormat().format(parseFloat(data.total))),
                                parseInt(data.id)
                            ]);
                        });
                    });
                });
                styles = JSON.parse('{'+(styles.substring(0, (parseInt(styles.length) -parseInt(1))))+'}');
                table.setData(array);
                table.setStyle(styles);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Plan Details');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

        getSuppliesList();

        getPlanDetails();
        
    });
</script>
