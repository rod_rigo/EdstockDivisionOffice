<?php
class xtcpdf extends TCPDF {
    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 8);
        $this->Cell(0, 0, 'Appendix 60', 0, 0, 'R');
        $this->Ln(10);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'PURCHASE REQUEST', 0, 0, 'C');
        $this->Ln(6);
    }
}

$pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->SetMargins(10, 35, 10, true);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('times','',12);
$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$data = '';
$counter = 0;
$total = 0;
foreach ($request->request_supplies as $request_supply){
    $counter++;
    $data .='<tr>
                <td align="center">'.(intval($counter)).'</td>
                <td align="center">'.($request_supply->supply->unit->unit).'</td>
                <td align="center">'.($request_supply->supply->supply_name).'</td>
                <td align="right">'.(number_format($request_supply->quantity)).'</td>
                <td align="right">'.(number_format($request_supply->price,2)).'</td>
                <td align="right">'.(number_format($request_supply->total,2)).'</td>
                </tr>
    ';
    $total += doubleval($request_supply->total);
}

$html = '<table width="100%">
    <tr>
        <td width="60%"><p>Entity Name: <b>'.($request->division->division).'</b></p></td>
        <td width="1%"></td>
        <td width="35%"><p>Fund Cluster: <b>'.($request->fund_cluster->fund_cluster).'</b></p></td>
    </tr>
</table>
<table width="100%" cellpadding="2" style="border-collapse: collapse; border: 1px solid black;">
    <tr>
        <td width="21%">Office/Section: <b>'.($request->office->office).'</b></td>
        <td width="49%" style="border-left: 1px solid black;">PR No: <b><u>'.($request->no).'</u></b></td>
        <td width="30%" align="right" style="border-left: 1px solid black;">Date: <b><u>'.((new DateTime($request->created))->format('Y-M-d')).'</u></b></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;">Responsibility Center Code : </td>
        <td style="border-left: 1px solid black;"></td>
    </tr>
</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
        <th width="13%" align="center">Stock/ Property No.</th>
        <th width="8%" align="center">Unit</th>
        <th width="40%" align="center">Item Description</th>
        <th width="9%" align="center">Quantity</th>
        <th width="15%" align="center">Unit Cost</th>
        <th width="15%" align="center">Total Cost</th>
    </tr>
    '.$data.'
    <tr>
        <td width="70%"></td>
        <td width="15%" align="center"><p>TOTAL</p></td>
        <td width="15%" align="right"><b>'.(number_format($total,2)).'</b></td>
    </tr>
    <tr>
        <td colspan="3" align="center">Nothing Follows</td>
    </tr>
     <tr>
         <td colspan="3">Purpose:
            <p align="left">'.($request->purpose).'</p>
    </td>
</tr>
    
</table>
<table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
<tr>
        <td width="13%"></td>
        <td width="48%"><p>Requested By: <b></b></p></td>
        <td width="39%"><p>Approved: <b></b></p></td>
    </tr>
    <tr>
        <td width="20%"></td>
    </tr>
<tr>
    <td width="13%">Signature:</td>
    <td width="29%">_______________________________</td>
    <td width="20%"></td>
    <td width="43%">____________________________________</td>
    <td></td>
</tr>
<tr>
    <td width="13%">Printed Name:</td>
    <td width="49%" align="left"><b><u>Requester Name</u></b></td>
    <td width="38%" align="left"><b><u>Official Name</u></b></td>
</tr>
<tr>
    <td width="13%">Designation:</td>
    <td width="49%" align="left">Requester Position</td>
    <td width="38%" align="left"><p>Official Position</p></td>
</tr>
<tr>
    <td width="13%">Date:</td>
    <td width="49%" align="left">'.((new DateTime($request->created))->format('Y-M-d')).'</td>
    <td width="38%" align="left">'.((new DateTime($request->created))->format('Y-M-d')).'</td>
</tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('PR-'.(strval($request->no)).'.pdf', 'I');
exit(0);