<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

$counter = 0;
$start = 8;
$check = WWW_ROOT. 'img'. DS. 'check.png';

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator($auth['username'])
    ->setTitle(ucwords('PR').strval($requisition->no))
    ->setSubject(ucwords('REQUISITION AND ISSUE SLIP'));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', ucwords('Entity Name'))
    ->setCellValue('B1', (ucwords($requisition->office->office)))
    ->setCellValue('D1', ucwords('Fund Cluster'))
    ->setCellValue('E1', (ucwords($requisition->fund_cluster->fund_cluster)));

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A3', ucwords('Division'))
    ->setCellValue('B3', (ucwords($requisition->division->division)))
    ->setCellValue('E3', (ucwords('Responsibility Code Center')))
    ->setCellValue('F3', null);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A4', ucwords('Office'))
    ->setCellValue('B4', null)
    ->setCellValue('E4', (ucwords('RIS No')))
    ->setCellValue('F4', (strval($requisition->no)));

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':D'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Requisition'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('A'.(strval($start)))
    ->getFont()
    ->setItalic(true)
    ->setBold(true);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('E'.(intval($start)).':F'.(intval($start)))
    ->setCellValue('E'.(strval($start)), ucwords('Stock Available?'))
    ->getStyle('E'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('E'.(strval($start)))
    ->getFont()
    ->setItalic(true)
    ->setBold(true);

$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('G'.(intval($start)).':H'.(intval($start)))
    ->setCellValue('G'.(strval($start)), ucwords('Issue'))
    ->getStyle('G'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle('G'.(strval($start)))
    ->getFont()
    ->setItalic(true)
    ->setBold(true);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Stock No'))
    ->setCellValue('B'.(strval($start)), ucwords('Unit'))
    ->setCellValue('C'.(strval($start)), ucwords('Item Description'))
    ->setCellValue('D'.(strval($start)), ucwords('Quantity'))
    ->setCellValue('E'.(strval($start)), ucwords('Yes'))
    ->setCellValue('F'.(strval($start)), ucwords('No'))
    ->setCellValue('G'.(strval($start)), ucwords('Quantity'))
    ->setCellValue('H'.(strval($start)), ucwords('Remarks'));

foreach ($requisition->requisition_supplies as $key => $requisition_supply) {
    $counter++;
    $start++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.(strval($start)), intval($counter))
        ->setCellValue('B'.(strval($start)), $requisition_supply->supply->unit->unit)
        ->setCellValue('C'.(strval($start)), $requisition_supply->supply->supply_name)
        ->setCellValue('D'.(strval($start)), number_format(doubleval($requisition_supply->quantity),2));

        $drawing = new Drawing();
        $drawing->setWorksheet($spreadsheet->setActiveSheetIndex(0));
        $drawing->setName('Image');
        $drawing->setDescription('Image');
        $drawing->setPath($check);
        $drawing->setCoordinates('E'.(strval($start)));
        $drawing->setWidth(20);
        $drawing->setHeight(20);
        $drawing->setOffsetX(60);
        $drawing->setOffsetY(0);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('F'.(strval($start)), null)
        ->setCellValue('G'.(strval($start)), number_format(intval($requisition_supply->quantity),2))
        ->setCellValue('H'.(strval($start)), null);
}

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':H'.(intval($start)))
    ->setCellValue('A'.(strval($start)), ucwords('Nothing Follows'))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->mergeCells('A'.(intval($start)).':H'.(intval($start+1)))
    ->setCellValue('A'.(strval($start)), ucwords('Purpose:'.strval(strip_tags($requisition->request->purpose))))
    ->getStyle('A'.(strval($start)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$start++;
$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Signature'))
    ->setCellValue('B'.(strval($start)), ucwords('Requested By'))
    ->setCellValue('C'.(strval($start)), ucwords('Approved By'))
    ->setCellValue('D'.(strval($start)), ucwords('Issued By'))
    ->setCellValue('E'.(strval($start)), ucwords('Received By'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Printed Name'))
    ->setCellValue('B'.(strval($start)), ucwords('Coordinator Name'))
    ->setCellValue('C'.(strval($start)), ucwords('FLORDELIZA C. GECOBE PHD, CESO V'))
    ->setCellValue('D'.(strval($start)), ucwords('Requester Nam'))
    ->setCellValue('E'.(strval($start)), ucwords('Coordinator Name'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Designation'))
    ->setCellValue('B'.(strval($start)), ucwords('Coordinatior Position'))
    ->setCellValue('C'.(strval($start)), ucwords('SCHOOLS DIVISION SUPERINTENDENT'))
    ->setCellValue('D'.(strval($start)), ucwords('SUPPLY AND/OR PROPERTY CUSTODIAN '))
    ->setCellValue('E'.(strval($start)), ucwords('Coordinatior Position'));

$start++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.(strval($start)), ucwords('Date'))
    ->setCellValue('B'.(strval($start)), null)
    ->setCellValue('C'.(strval($start)), null)
    ->setCellValue('D'.(strval($start)), null)
    ->setCellValue('E'.(strval($start)), null);

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('RIS-'.strval($requisition->no).'.xlsx').'"');
$writer->save('php://output');
exit(0);