<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Application[]|\Cake\Collection\CollectionInterface $applications
 */
?>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-9 mb-3">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-4">
                <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                <?=$this->Form->date('start_date',[
                    'class' => 'form-control form-control-border',
                    'id' => 'start-date',
                    'title' => ucwords('Start Date')
                ])?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-4">
                <?=$this->Form->label('end_date', ucwords('End Date'))?>
                <?=$this->Form->date('end_date',[
                    'class' => 'form-control form-control-border',
                    'id' => 'end-date',
                    'title' => ucwords('End Date')
                ])?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-1 d-flex justify-content-start align-items-end">
                <?=$this->Form->button('Search',[
                    'class' => 'btn btn-primary rounded-0',
                    'type' => 'button',
                    'id' => 'search'
                ])?>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                <?=$this->Form->label('divisions', ucwords('Divisions'))?>
                <?=$this->Form->select('divisions', $divisions,[
                    'class' => 'form-control form-control-border',
                    'id' => 'divisions',
                    'title' => ucwords('Divisions'),
                    'empty' => ucwords('Divisions'),
                ])?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Secretary', 'controller' => 'Applications', 'action' => 'add'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="New Application">
            New Application
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No</th>
                        <th>Request By</th>
                        <th>Division</th>
                        <th>Office</th>
                        <th>Remarks</th>
                        <th>Is Approved</th>
                        <th>Is Declined</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'ris/';
        var url = '';
        var isApproved = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];
        var isDeclined = [
            '<span class="text-danger"><i class="fa fa-times"></i> No</span>',
            '<span class="text-success"><i class="fa fa-check"></i> Yes</span>',
        ];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getSerieses',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [6],
                    data: null,
                    render: function(data,type,row,meta){
                        return isApproved[(parseInt(row.application.is_approved))];
                    }
                },
                {
                    targets: [7],
                    data: null,
                    render: function(data,type,row,meta){
                        return isDeclined[(parseInt(row.application.is_declined))];
                    }
                },
                {
                    targets: [8],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [9],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white pdf" title="PDF">PDF</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white xlsx" title="XLSX">XLSX</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'no'},
                { data: 'application.user.username'},
                { data: 'application.division.division'},
                { data: 'application.office.office'},
                { data: 'application.remarks'},
                { data: 'application.is_approved'},
                { data: 'application.is_declined'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.view',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'view/'+(parseInt(dataId));
            Swal.fire({
                title: 'View Data',
                text: 'Open In New Window?',
                icon: 'question',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                denyButtonColor: '#6e7881',
                cancelButtonText: 'No',
                denyButtonText: 'Cancel',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                    return true;
                }else if(result.isDenied){
                    Swal.close();
                    return true;
                }
                Turbolinks.visit(href,{ action:'advance' });
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'pdf/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To PDF',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        datatable.on('click','.xlsx',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'xlsx/'+(parseInt(dataId));
            Swal.fire({
                title: 'Export To Excel',
                text: 'Are You Sure?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        $('#search').click(function () {
            var startDate = ($('#start-date').val())? moment($('#start-date').val()).format('Y-MM-DD'): moment().startOf('year').format('Y-MM-DD');
            var endDate = ($('#end-date').val())? moment($('#end-date').val()).format('Y-MM-DD'): moment().endOf('year').format('Y-MM-DD');
            $('#start-date, #end-date, #search, #divisions').prop('disabled', true) ;
            table.ajax.url(baseurl+'getSerieses?start_date='+(startDate)+'&end_date='+(endDate)+'').load(function () {
                $('#start-date, #end-date, #search, #divisions').prop('disabled', false);
                if($('#divisions').val()){
                    table.columns([3]).search(($('#divisions').val()) ? '^' + ($('#divisions').val()) + '$' : '', true, false).draw();
                }
            }, true);
        });

        $('#divisions').change(function (e) {
            var value = $(this).val();
            table.columns([3]).search((value) ? '^' + (value) + '$' : '', true, false).draw();
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
