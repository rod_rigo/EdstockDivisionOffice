<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FundCluster[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('quantity', ucwords('Quantity'))?>
                        <?=$this->Form->number('quantity',[
                            'class' => 'form-control rounded-0',
                            'id' => 'quantity',
                            'required' => true,
                            'placeholder' => ucwords('Quantity'),
                            'pattern' => '([0-9]){1,}',
                            'title' => ucwords('Please Enter A Quantity'),
                            'min' => '0',
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <?=$this->Form->label('previous_stocks', ucwords('Previous Stocks'))?>
                        <div class="input-group">
                            <div class="input-group-prepend rounded-0 bg-info border-info">
                                <span class="input-group-text rounded-0 bg-info border-info" id="operator-sign">
                                  <i class="fas fa-question"></i>
                                </span>
                            </div>
                            <?=$this->Form->number('previous_stocks',[
                                'class' => 'form-control rounded-0',
                                'id' => 'previous-stocks',
                                'required' => true,
                                'readonly' => true,
                                'placeholder' => ucwords('Previous Stocks'),
                                'pattern' => '([0-9]){1,}',
                                'title' => ucwords('Please Enter A Previous Stocks'),
                                'min' => '0',
                            ])?>
                            <small></small>
                            <div class="input-group-prepend rounded-0 bg-info border-info">
                                <span class="input-group-text rounded-0 bg-info border-info">
                                  <i class="fas fa-equals"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->label('total_stocks', ucwords('Total Stocks'))?>
                        <?=$this->Form->number('total_stocks',[
                            'class' => 'form-control rounded-0',
                            'id' => 'total-stocks',
                            'required' => true,
                            'readonly' => true,
                            'placeholder' => ucwords('Total Stocks'),
                            'pattern' => '([0-9]){1,}',
                            'title' => ucwords('Please Enter A Total Stocks'),
                            'min' => '0',
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="is-in-0" class="is-in" value="0" checked/>
                                    <label for="is-in-0">Out Stock</label>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-5 d-flex justify-content-start align-items-center">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="is-in-1" class="is-in" value="1"/>
                                    <label for="is-in-1">In Stock</label>
                                </div>
                            </div>

                            <?= $this->Form->hidden('is_in',[
                                'id' => 'is-in',
                                'required' => true,
                                'value' => intval(0),
                            ]);?>
                            <small></small>

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($auth['id'])
                ])?>
                <?=$this->Form->hidden('supply_id',[
                    'id' => 'supply-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('description',[
                    'id' => 'description',
                    'required' => true,
                    'readonly' => true,
                ])?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Secretary', 'controller' => 'Supplies', 'action' => 'add'])?>" turbolink class="btn btn-primary rounded-0" title="New Supply">
            New Supply
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Supply Name</th>
                        <th>Division</th>
                        <th>Category</th>
                        <th>Unit</th>
                        <th>Range</th>
                        <th>Supply Code</th>
                        <th>Stock</th>
                        <th>Price</th>
                        <th>Stock Availability</th>
                        <th>Semi/Expendable</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        const baseurl = mainurl+'supplies/';
        var url = '';
        var is_expendables = ['Semi-Expendable', 'Expendable'];
        var in_stock = ['Out of Stock', 'In Stock'];
        var is_in = ['<i class="fas fa-minus"></i>', '<i class="fas fa-plus"></i>'];
        var description = ['Out Stock', 'In Stock'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            lengthMenu:[100, 200, 500, 1000],
            ajax:{
                url:baseurl+'getSupplies',
                official: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {

                },
                error:function (data, status, xhr) {
                    window.location.reload();
                }
            },
            columnDefs: [
                {
                    targets: [0],
                    data: null,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: [7],
                    data: null,
                    render: function(data,type,row,meta){
                        return (new Intl.NumberFormat().format(parseFloat(row.stock)));
                    }
                },
                {
                    targets: [8],
                    data: null,
                    render: function(data,type,row,meta){
                        return (new Intl.NumberFormat().format(parseFloat(row.price)));
                    }
                },
                {
                    targets: [9],
                    data: null,
                    render: function(data,type,row,meta){
                        return (in_stock[parseInt(row.in_stock)]);
                    }
                },
                {
                    targets: [10],
                    data: null,
                    render: function(data,type,row,meta){
                        return (is_expendables[parseInt(row.is_expendable)]);
                    }
                },
                {
                    targets: [12],
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.modified).format('Y-MM-DD hh:mm A');
                    }
                },
                {
                    targets: [13],
                    data: null,
                    orderable:false,
                    searchable:false,
                    render: function(data,type,row,meta){
                        return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a> | '+
                            '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white stocks" title="Stocks">Stocks</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'supply_name'},
                { data: 'division.division'},
                { data: 'category.category'},
                { data: 'unit.unit'},
                { data: 'range.code'},
                { data: 'supply_code'},
                { data: 'stock'},
                { data: 'price'},
                { data: 'in_stock'},
                { data: 'is_expendable'},
                { data: 'user.username'},
                { data: 'modified'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.view',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'view/'+(parseInt(dataId));
            Swal.fire({
                title: 'View Data',
                text: 'Open In New Window?',
                icon: 'question',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                denyButtonColor: '#6e7881',
                cancelButtonText: 'No',
                denyButtonText: 'Cancel',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                    return true;
                }else if(result.isDenied){
                   Swal.close();
                   return true;
                }
                Turbolinks.visit(href,{ action:'advance' });
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+(parseInt(dataId));
            Swal.fire({
                title: 'Delete Data',
                text: 'Are You Sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        datatable.on('click','.stocks',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+(parseInt(dataId));
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                },
            }).done(function (data, status, xhr) {
                $('#modal-title').text(data.supply_name);
                $('#supply-id').val(parseInt(data.id));
                $('#previous-stocks').val(parseInt(data.stock));
                $('#total-stocks').val(parseInt(data.stock));
                $('#modal').modal('toggle');
                Swal.close();
            }).fail(function (data, status, xhr) {
                swal('error', 'Error', data.responseJSON.message);
            });
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: mainurl+'stocks/add',
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                $('#modal').modal('toggle');
                table.ajax.reload(null, false);
                swal('success', null, data.message);
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('#form')[0].reset();
            $('small').empty();
            $('#modal-title').empty();
            $('.form-control').removeClass('is-invalid');
            $('button[type="reset"]').fadeIn(100);
            $('button[type="reset"], button[type="submit"]').prop('disabled', false);
        });

        $('input[type="number"]').keypress(function (e) {
            var key = e.key;
            var regex = /^([0-9]){1,}$/;

            if(!key.match(regex)){
                e.preventDefault();
            }

        });

        $('#quantity').on('input', function (e) {
            var value = $(this).val();
            var regex = /^([0-9]){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Quantity');
                return true;
            }

            computation();
            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#previous-stocks').on('input', function (e) {
            var value = $(this).val();
            var regex = /^([0-9]){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Previous Stocks');
                return true;
            }

            computation();
            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#total-stocks').on('input', function (e) {
            var value = $(this).val();
            var regex = /^([0-9]){1,}$/;

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Total Stocks');
                return true;
            }

            computation();
            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('.is-in').change(function (e) {
            var value = $(this).val();
            $(this).prop('checked', true);
            $('.is-in:checked').not(this).prop('checked', false);
            $('#is-in').val(parseInt(value));
            $('#operator-sign').html(is_in[parseInt(value)]);
            $('#description').val(description[parseInt(value)]);
            computation();
        });

        function computation(){
            var total_stocks = 0;
            var is_in = $('#is-in').val();
            var quantity = ($('#quantity').val()? parseInt($('#quantity').val()): parseInt(0));
            var previous_stocks = $('#previous-stocks').val();
            if(parseInt(is_in)){
                total_stocks = parseInt(quantity) + parseInt(previous_stocks);
                $('#total-stocks').val(parseFloat(total_stocks));
            }else{

                if(parseInt(previous_stocks) > parseInt(quantity)){
                    total_stocks = parseInt(previous_stocks) - parseInt(quantity);
                    $('#total-stocks').val(parseFloat(total_stocks));
                }else{
                    $('#total-stocks').val(parseFloat(0));
                }

            }

        }

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>
