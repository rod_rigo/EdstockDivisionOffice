<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="login-box">

    <div class="login-logo">
        <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle" height="100" width="140" loading="lazy" alt="Deped Logo" title="Depen Logo">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
                <div class="input-group mb-3">
                    <?=$this->Form->text('username',[
                        'class' => 'form-control rounded-0',
                        'required' => true,
                        'id' => 'username',
                        'placeholder' => ucwords('Username Or Email'),
                    ])?>
                    <div class="input-group-append rounded-0">
                        <div class="input-group-text rounded-0">
                            <span class="fas fa-user-cog rounded-0"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->password('password',[
                        'class' => 'form-control rounded-0',
                        'required' => true,
                        'id' => 'password',
                        'placeholder' => ucwords('Password'),
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <?=$this->Form->checkbox('remember_me',[
                                'id' => 'remember-me',
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('remember_me',ucwords('Remember Me'))?>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <?=$this->Form->submit(ucwords('Submit'),[
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-block rounded-0'
                        ])?>
                    </div>
                    <!-- /.col -->
                </div>
            <?=$this->Form->end()?>
            <!-- /.social-auth-links -->

            <p class="mb-1">
                <a href="forgot-password.html">I forgot my password</a>
            </p>
            <p class="mb-0">
                <a href="register.html" class="text-center">Register a new membership</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(data.redirect,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            });
        }

    });
</script>