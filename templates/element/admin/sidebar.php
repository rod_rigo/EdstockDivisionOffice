<?php
/**
 * @var \App\View\AppView $this
 */

$inventory = [
    strtolower('supplies'),
    strtolower('categories'),
    strtolower('units'),
    strtolower('ranges'),
    strtolower('applications')
];

$procurement = [
    strtolower('requests'),
    strtolower('orders'),
    strtolower('inspections'),
    strtolower('requisitions'),
    strtolower('methods'),
    strtolower('fundclusters'),
    strtolower('suppliers'),
    strtolower('plans'),
];

$management = [
    strtolower('divisions'),
    strtolower('offices'),
    strtolower('positions'),
    strtolower('officials')
];

?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
            <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle w-100" loading="lazy" title="Deped Logo" alt="DEPED">
        </div>

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$this->Url->assetUrl('/img/user-avatar/'.(strval($auth['id'])).'.png')?>" class="img-circle elevation-2" loading="lazy" alt="User Image">
            </div>
            <div class="info">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])])?>" turbolink class="d-block"><?=@($auth['username'])?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('dashboards') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item <?=(in_array(strtolower($controller),[strtolower('users'), strtolower('profiles')]) ? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('users') == strtolower($controller)? 'active': null);?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Profiles', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('profiles') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Profiles</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Inventory</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('supplies') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-boxes-packing"></i>
                        <p>Supplies</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ranges', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('ranges') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-sort-numeric-desc"></i>
                        <p>Ranges</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('categories') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Units', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('units') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-balance-scale"></i>
                        <p>Units</p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-files-o"></i>
                        <p>
                            Applications
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('ics') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-file-o"></i>
                        <p>
                            ICS
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ics', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ics', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ics', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ics', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ics', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('ris') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-file-o"></i>
                        <p>
                            RIS
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ris', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ris', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ris', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ris', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ris', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Procurement</li>
                <li class="nav-item <?=(strtolower('requests') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-pencil-alt"></i>
                        <p>
                            Requests
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item <?=(strtolower('orders') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('orders') == strtolower($controller) && (strtolower('bin') != strtolower($action))? 'active': null);?>">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item <?=(strtolower('inspections') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('inspections') == strtolower($controller)? 'active': null);?>">
                        <i class="nav-icon fas fa-search"></i>
                        <p>
                            Inspections
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item <?=(strtolower('requisitions') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('requisitions') == strtolower($controller)? 'active': null);?>">
                        <i class="nav-icon fas fa-file-text"></i>
                        <p>
                            Requisitions
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Methods', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('methods') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-clipboard"></i>
                        <p>Methods</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'FundClusters', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('fundclusters') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-money"></i>
                        <p>Fund Clusters</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Suppliers', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('suppliers') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-truck"></i>
                        <p>Suppliers</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Plans', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('plans') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-calendar"></i>
                        <p>Plans</p>
                    </a>
                </li>

                <li class="nav-header">Management</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Divisions', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('divisions') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>Divisions</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Offices', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('offices') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-building-o" aria-hidden="true"></i>
                        <p>Offices</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('positions') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-id-card" aria-hidden="true"></i>
                        <p>Positions</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Officials', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('officials') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-user" aria-hidden="true"></i>
                        <p>Officials</p>
                    </a>
                </li>

                <li class="nav-header">Bin</li>
                <li class="nav-item <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $inventory)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $inventory)? 'active': null);?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Inventory
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('supplies') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplies</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ranges', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('ranges') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ranges</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('categories') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Units', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('units') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Applications', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Applications</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $procurement)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $procurement)? 'active': null);?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Procurement
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('orders') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Orders</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('inspections') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inspections</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('requisitions') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Requisitions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Methods', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('methods') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Methods</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'FundClusters', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('fundclusters') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Fund Clusters</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Suppliers', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('suppliers') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Suppliers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Plans', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('plans') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Plans</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $management)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $management)? 'active': null);?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Divisions', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('divisions') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Divisions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Offices', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('offices') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Offices</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('positions') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Positions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Officials', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('officials') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Officials</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header">Sign Out</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
