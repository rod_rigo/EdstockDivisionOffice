<?php
/**
 * @var \App\View\AppView $this
 */
?>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5><?=ucwords(strtoupper($prefix))?></h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><?=ucwords(strtoupper($action))?></li>
                    <li class="breadcrumb-item active"><?=ucwords(strtoupper($controller))?></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
