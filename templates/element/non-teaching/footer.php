<?php
/**
 * @var \App\View\AppView $this
 */?>

<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2024 All rights reserved.
</footer>
