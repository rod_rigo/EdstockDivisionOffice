<?php
/**
 * @var \App\View\AppView $this
 */

$inventory = [
    strtolower('supplies'),
    strtolower('categories'),
    strtolower('units'),
    strtolower('ranges')
];

$procurement = [
    strtolower('requests'),
    strtolower('orders'),
    strtolower('inspections'),
    strtolower('requisitions'),
    strtolower('methods'),
    strtolower('fundclusters'),
    strtolower('suppliers'),
    strtolower('plans'),
];

$management = [
    strtolower('divisions'),
    strtolower('offices'),
    strtolower('positions'),
    strtolower('officials')
];

?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
            <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle w-100" loading="lazy" title="Deped Logo" alt="DEPED">
        </div>

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$this->Url->assetUrl('/img/user-avatar/'.(strval($auth['id'])).'.png')?>" class="img-circle elevation-2" loading="lazy" alt="User Image">
            </div>
            <div class="info">
                <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])])?>" turbolink class="d-block"><?=@($auth['username'])?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('dashboards') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-header">Inventory</li>
                <li class="nav-item <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-files-o"></i>
                        <p>
                            Applications
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('ics') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-file-o"></i>
                        <p>
                            ICS
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ics', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ics', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ics', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ics', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ics', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('ics') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower('ris') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('bin') != strtolower($action)? 'active': null);?>">
                        <i class="nav-icon fas fa-file-o"></i>
                        <p>
                            RIS
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ris', 'action' => 'index'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('index') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ris', 'action' => 'today'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('today') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ris', 'action' => 'week'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('week') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ris', 'action' => 'month'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('month') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Ris', 'action' => 'year'])?>" turbolink class="nav-link <?=(strtolower('ris') == strtolower($controller) && strtolower('year') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Bin</li>
                <li class="nav-item <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $inventory)? 'menu-is-opening menu-open': null);?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower('bin') == strtolower($action) && in_array(strtolower($controller), $inventory)? 'active': null);?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Inventory
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'NonTeaching', 'controller' => 'Applications', 'action' => 'bin'])?>" turbolink class="nav-link <?=(strtolower('applications') == strtolower($controller) && strtolower('bin') == strtolower($action)? 'active': null);?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Applications</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header">Sign Out</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
