<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?=$this->Html->meta('csrf-token', $this->request->getAttribute('csrfToken'));?>

    <?= $this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/dist/css/adminlte.min',
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
        '/font-awesome/css/font-awesome'
    ]) ?>

    <?= $this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.all',
        '/turbo-links/js/turbolinks'
    ])?>

    <script>
        var mainurl = window.location.origin+'/EdstockDivisionOffice/';
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="hold-transition login-page">

<!-- Default box -->
<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>
<!-- /.card -->
<!-- ./wrapper -->

<?= $this->Html->script([
    '/plugins/bootstrap/js/bootstrap.bundle.min',
    '/dist/js/adminlte.min',
]) ?>

<script>
    Turbolinks.start();
</script>

</body>
</html>
