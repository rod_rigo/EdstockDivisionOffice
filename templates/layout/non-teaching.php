<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?=$this->Html->meta('csrf-token', $this->request->getAttribute('csrfToken'));?>

    <?=$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css')?>
    <?= $this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/font-awesome/css/font-awesome',
        '/dist/css/adminlte.min',
        '/jquery/css/jquery-ui',
        '/datatables/css/jquery.dataTables.min',
        '/datatables/css/dataTables.bootstrap4.min',
        '/datatables/css/responsive.bootstrap4.min',
        '/datatables/css/buttons/buttons.dataTables.min',
        '/datatables/css/buttons/buttons.bootstrap4.min',
        '/evo-calendar/css/evo-calendar',
        '/i-check/css/icheck-bootstrap',
        '/leaflet/css/leaflet',
        '/leaflet/css/leaflet-search',
        '/print-js/css/print',
        '/jqspreadsheet/css/jexcel',
        '/jqspreadsheet/css/jsuites',
        'custom',
    ]) ?>
    <?=$this->Html->css('https://fonts.googleapis.com/css?family=Material+Icons')?>

    <?= $this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.all',
        '/signaturepad/js/signature_pad.umd',
        '/chartjs/js/chart',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/ck-editor/js/ckeditor',
        '/datatables/js/jquery.dataTables.min',
        '/datatables/js/dataTables.responsive.min',
        '/datatables/js/responsive.bootstrap4.min',
        '/datatables/js/buttons/buttons.bootstrap4.min',
        '/datatables/js/buttons/buttons.print.min',
        '/datatables/js/buttons/dataTables.buttons.min',
        '/datatables/js/buttons/html2pdf.bundle.min',
        '/datatables/js/buttons/jszip.min',
        '/datatables/js/buttons/pdfmake.min',
        '/datatables/js/buttons/vfs_fonts',
        '/datatables/js/buttons/buttons.html5.min',
        '/download-js/js/download',
        '/evo-calendar/js/evo-calendar',
        '/excel-js/js/exceljs',
        '/leaflet/js/leaflet',
        '/leaflet/js/leaflet-search',
        '/print-js/js/print',
        '/jqspreadsheet/js/jexcel',
        '/jqspreadsheet/js/jsuites',
        '/jquery-mask/js/jquery.mask',
        '/filesaver-js/js/FileSaver',
        '/turbo-links/js/turbolinks'
    ])?>

    <script>
        var mainurl = window.location.origin+'/EdstockDivisionOffice/non-teaching/';
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="sidebar-mini sidebar-collapse">

<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <?=$this->element('non-teaching/navbar')?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?=$this->element('non-teaching/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?=$this->element('non-teaching/header')?>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?=$this->element('non-teaching/footer')?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?= $this->Html->script([
    '/plugins/bootstrap/js/bootstrap.bundle.min',
    '/dist/js/adminlte.min',
    'non-teaching'
]) ?>

</body>
</html>
