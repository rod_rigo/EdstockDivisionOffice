<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrdersFixture
 */
class OrdersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'request_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'division_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'office_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fund_cluster_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'supplier_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'method_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'no' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'place_of_delivery' => ['type' => 'text', 'length' => 4294967295, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'modified' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'deleted' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'orders to users' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'orders to requests' => ['type' => 'index', 'columns' => ['request_id'], 'length' => []],
            'orders to divisions' => ['type' => 'index', 'columns' => ['division_id'], 'length' => []],
            'orders to offices' => ['type' => 'index', 'columns' => ['office_id'], 'length' => []],
            'orders to fund_clusters' => ['type' => 'index', 'columns' => ['fund_cluster_id'], 'length' => []],
            'orders to suppliers' => ['type' => 'index', 'columns' => ['supplier_id'], 'length' => []],
            'orders to methods' => ['type' => 'index', 'columns' => ['method_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'orders to divisions' => ['type' => 'foreign', 'columns' => ['division_id'], 'references' => ['divisions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to fund_clusters' => ['type' => 'foreign', 'columns' => ['fund_cluster_id'], 'references' => ['fund_clusters', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to methods' => ['type' => 'foreign', 'columns' => ['method_id'], 'references' => ['methods', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to offices' => ['type' => 'foreign', 'columns' => ['office_id'], 'references' => ['offices', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to requests' => ['type' => 'foreign', 'columns' => ['request_id'], 'references' => ['requests', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to suppliers' => ['type' => 'foreign', 'columns' => ['supplier_id'], 'references' => ['suppliers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'orders to users' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_0900_ai_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'request_id' => 1,
                'division_id' => 1,
                'office_id' => 1,
                'fund_cluster_id' => 1,
                'supplier_id' => 1,
                'method_id' => 1,
                'no' => 'Lorem ipsum dolor sit amet',
                'place_of_delivery' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => 1705716743,
                'modified' => 1705716743,
                'deleted' => '2024-01-20 10:12:23',
            ],
        ];
        parent::init();
    }
}
