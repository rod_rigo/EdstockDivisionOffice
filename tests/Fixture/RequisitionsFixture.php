<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequisitionsFixture
 */
class RequisitionsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'request_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'order_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'inspection_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'division_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'office_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fund_cluster_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'supplier_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'method_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'no' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'modified' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'deleted' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'requisitions to users' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'requisitions to requests' => ['type' => 'index', 'columns' => ['request_id'], 'length' => []],
            'requisitions to orders' => ['type' => 'index', 'columns' => ['order_id'], 'length' => []],
            'requisitions to inspections' => ['type' => 'index', 'columns' => ['inspection_id'], 'length' => []],
            'requisitions to divisions' => ['type' => 'index', 'columns' => ['division_id'], 'length' => []],
            'requisitions to offices' => ['type' => 'index', 'columns' => ['office_id'], 'length' => []],
            'requisitions to fund_clusters' => ['type' => 'index', 'columns' => ['fund_cluster_id'], 'length' => []],
            'requisitions to suppliers' => ['type' => 'index', 'columns' => ['supplier_id'], 'length' => []],
            'requisitions to methods' => ['type' => 'index', 'columns' => ['method_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'requisitions to divisions' => ['type' => 'foreign', 'columns' => ['division_id'], 'references' => ['divisions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to fund_clusters' => ['type' => 'foreign', 'columns' => ['fund_cluster_id'], 'references' => ['fund_clusters', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to inspections' => ['type' => 'foreign', 'columns' => ['inspection_id'], 'references' => ['inspections', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to methods' => ['type' => 'foreign', 'columns' => ['method_id'], 'references' => ['methods', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to offices' => ['type' => 'foreign', 'columns' => ['office_id'], 'references' => ['offices', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to orders' => ['type' => 'foreign', 'columns' => ['order_id'], 'references' => ['orders', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to requests' => ['type' => 'foreign', 'columns' => ['request_id'], 'references' => ['requests', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to suppliers' => ['type' => 'foreign', 'columns' => ['supplier_id'], 'references' => ['suppliers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'requisitions to users' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_0900_ai_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'request_id' => 1,
                'order_id' => 1,
                'inspection_id' => 1,
                'division_id' => 1,
                'office_id' => 1,
                'fund_cluster_id' => 1,
                'supplier_id' => 1,
                'method_id' => 1,
                'no' => 'Lorem ipsum dolor sit amet',
                'created' => 1705801992,
                'modified' => 1705801992,
                'deleted' => '2024-01-21 09:53:12',
            ],
        ];
        parent::init();
    }
}
