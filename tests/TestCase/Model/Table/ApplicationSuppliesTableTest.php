<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationSuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationSuppliesTable Test Case
 */
class ApplicationSuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationSuppliesTable
     */
    protected $ApplicationSupplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ApplicationSupplies',
        'app.Applications',
        'app.Serieses',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ApplicationSupplies') ? [] : ['className' => ApplicationSuppliesTable::class];
        $this->ApplicationSupplies = $this->getTableLocator()->get('ApplicationSupplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ApplicationSupplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ApplicationSuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ApplicationSuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
