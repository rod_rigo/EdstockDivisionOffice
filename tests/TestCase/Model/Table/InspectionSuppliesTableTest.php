<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InspectionSuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InspectionSuppliesTable Test Case
 */
class InspectionSuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InspectionSuppliesTable
     */
    protected $InspectionSupplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.InspectionSupplies',
        'app.Inspections',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('InspectionSupplies') ? [] : ['className' => InspectionSuppliesTable::class];
        $this->InspectionSupplies = $this->getTableLocator()->get('InspectionSupplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->InspectionSupplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\InspectionSuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\InspectionSuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
