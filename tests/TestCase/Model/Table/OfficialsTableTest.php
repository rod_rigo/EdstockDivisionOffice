<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OfficialsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OfficialsTable Test Case
 */
class OfficialsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OfficialsTable
     */
    protected $Officials;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Officials',
        'app.Users',
        'app.Divisions',
        'app.Positions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Officials') ? [] : ['className' => OfficialsTable::class];
        $this->Officials = $this->getTableLocator()->get('Officials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Officials);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\OfficialsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\OfficialsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
