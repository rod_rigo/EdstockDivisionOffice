<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderSuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderSuppliesTable Test Case
 */
class OrderSuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderSuppliesTable
     */
    protected $OrderSupplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.OrderSupplies',
        'app.Orders',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('OrderSupplies') ? [] : ['className' => OrderSuppliesTable::class];
        $this->OrderSupplies = $this->getTableLocator()->get('OrderSupplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrderSupplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\OrderSuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\OrderSuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
