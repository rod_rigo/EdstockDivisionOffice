<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlanDetailsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlanDetailsTable Test Case
 */
class PlanDetailsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PlanDetailsTable
     */
    protected $PlanDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PlanDetails',
        'app.Users',
        'app.Plans',
        'app.PlanTitles',
        'app.Parts',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PlanDetails') ? [] : ['className' => PlanDetailsTable::class];
        $this->PlanDetails = $this->getTableLocator()->get('PlanDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PlanDetails);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PlanDetailsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PlanDetailsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
