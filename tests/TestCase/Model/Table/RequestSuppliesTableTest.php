<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestSuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestSuppliesTable Test Case
 */
class RequestSuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestSuppliesTable
     */
    protected $RequestSupplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RequestSupplies',
        'app.Requests',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('RequestSupplies') ? [] : ['className' => RequestSuppliesTable::class];
        $this->RequestSupplies = $this->getTableLocator()->get('RequestSupplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RequestSupplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RequestSuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\RequestSuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
