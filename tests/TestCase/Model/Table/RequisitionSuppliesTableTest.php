<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequisitionSuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequisitionSuppliesTable Test Case
 */
class RequisitionSuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequisitionSuppliesTable
     */
    protected $RequisitionSupplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RequisitionSupplies',
        'app.Requisitions',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('RequisitionSupplies') ? [] : ['className' => RequisitionSuppliesTable::class];
        $this->RequisitionSupplies = $this->getTableLocator()->get('RequisitionSupplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RequisitionSupplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RequisitionSuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\RequisitionSuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
