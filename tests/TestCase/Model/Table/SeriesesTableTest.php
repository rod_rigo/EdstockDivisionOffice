<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SeriesesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SeriesesTable Test Case
 */
class SeriesesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SeriesesTable
     */
    protected $Serieses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Serieses',
        'app.Applications',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Serieses') ? [] : ['className' => SeriesesTable::class];
        $this->Serieses = $this->getTableLocator()->get('Serieses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Serieses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SeriesesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SeriesesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
