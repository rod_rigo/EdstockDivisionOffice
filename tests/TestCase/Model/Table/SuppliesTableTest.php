<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuppliesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuppliesTable Test Case
 */
class SuppliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SuppliesTable
     */
    protected $Supplies;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Supplies',
        'app.Users',
        'app.Categories',
        'app.Units',
        'app.Ranges',
        'app.InspectionSupplies',
        'app.OrderSupplies',
        'app.PlanDetails',
        'app.RequestSupplies',
        'app.RequisitionSupplies',
        'app.Stocks',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Supplies') ? [] : ['className' => SuppliesTable::class];
        $this->Supplies = $this->getTableLocator()->get('Supplies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Supplies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SuppliesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SuppliesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
