/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : edstock_division_office_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/02/2024 16:47:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application_supplies
-- ----------------------------
DROP TABLE IF EXISTS `application_supplies`;
CREATE TABLE `application_supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_id` bigint UNSIGNED NOT NULL,
  `series_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `application_supplies to applications`(`application_id` ASC) USING BTREE,
  INDEX `application_supplies to serieses`(`series_id` ASC) USING BTREE,
  CONSTRAINT `application_supplies to applications` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `application_supplies to serieses` FOREIGN KEY (`series_id`) REFERENCES `serieses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of application_supplies
-- ----------------------------

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_approved` tinyint NOT NULL DEFAULT 0,
  `approved_at` datetime NULL DEFAULT NULL,
  `is_declined` tinyint NOT NULL DEFAULT 0,
  `declined_at` datetime NULL DEFAULT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `applications to users`(`user_id` ASC) USING BTREE,
  INDEX `applications to divisions`(`division_id` ASC) USING BTREE,
  INDEX `applications to offices`(`office_id` ASC) USING BTREE,
  CONSTRAINT `applications to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `applications to divisions` FOREIGN KEY (`division_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `applications to offices` FOREIGN KEY (`office_id`) REFERENCES `officials` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of applications
-- ----------------------------

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `categories to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `categories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 'Office Supply', '2024-01-17 15:54:36', '2024-01-23 17:53:19', NULL);

-- ----------------------------
-- Table structure for divisions
-- ----------------------------
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE `divisions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `divisions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `divisions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of divisions
-- ----------------------------
INSERT INTO `divisions` VALUES (1, 1, 'SDO - Santiago City', '2024-01-16 10:43:56', '2024-01-22 19:15:56', NULL);
INSERT INTO `divisions` VALUES (2, 1, 'SDO - Cordon', '2024-01-23 21:43:16', '2024-01-23 21:43:16', NULL);

-- ----------------------------
-- Table structure for fund_clusters
-- ----------------------------
DROP TABLE IF EXISTS `fund_clusters`;
CREATE TABLE `fund_clusters`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `fund_cluster` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fund_clusters_to_users`(`user_id` ASC) USING BTREE,
  INDEX `fund_clusters to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `fund_clusters to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fund_clusters_to_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fund_clusters
-- ----------------------------
INSERT INTO `fund_clusters` VALUES (1, 2, 1, 'MOOE', '2024-01-16 09:29:27', '2024-02-20 21:34:39', NULL);

-- ----------------------------
-- Table structure for inspection_supplies
-- ----------------------------
DROP TABLE IF EXISTS `inspection_supplies`;
CREATE TABLE `inspection_supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `inspection_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests_suppliers to requests`(`inspection_id` ASC) USING BTREE,
  INDEX `requests_suppliers to supplies`(`supply_id` ASC) USING BTREE,
  CONSTRAINT `inspection_supplies to inspections` FOREIGN KEY (`inspection_id`) REFERENCES `inspections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspection_supplies to supply` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inspection_supplies
-- ----------------------------
INSERT INTO `inspection_supplies` VALUES (1, 1, 13, 0, 2, 0, '2024-01-22 22:23:29', '2024-01-22 22:23:29', NULL);
INSERT INTO `inspection_supplies` VALUES (2, 1, 41, 0, 2, 0, '2024-01-22 22:23:29', '2024-01-22 22:23:29', NULL);
INSERT INTO `inspection_supplies` VALUES (3, 1, 11, 0, 2, 0, '2024-01-22 22:23:29', '2024-01-22 22:23:29', NULL);
INSERT INTO `inspection_supplies` VALUES (4, 10, 32, 0, 1, 0, '2024-02-20 22:30:12', '2024-02-20 22:30:12', NULL);

-- ----------------------------
-- Table structure for inspections
-- ----------------------------
DROP TABLE IF EXISTS `inspections`;
CREATE TABLE `inspections`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `request_id` bigint UNSIGNED NOT NULL,
  `order_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `fund_cluster_id` bigint UNSIGNED NOT NULL,
  `supplier_id` bigint UNSIGNED NOT NULL,
  `method_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_completed` tinyint NOT NULL DEFAULT 0,
  `is_inspected` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `inspections to users`(`user_id` ASC) USING BTREE,
  INDEX `inspections to requests`(`request_id` ASC) USING BTREE,
  INDEX `inspections to orders`(`order_id` ASC) USING BTREE,
  INDEX `inspections to divisions`(`division_id` ASC) USING BTREE,
  INDEX `inspections to offices`(`office_id` ASC) USING BTREE,
  INDEX `inspections to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `inspections to supplier_id`(`supplier_id` ASC) USING BTREE,
  INDEX `inspections to method_id`(`method_id` ASC) USING BTREE,
  CONSTRAINT `inspections to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to method_id` FOREIGN KEY (`method_id`) REFERENCES `methods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to supplier_id` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inspections
-- ----------------------------
INSERT INTO `inspections` VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, '2024-01-0001', 0, 1, '2024-01-22 22:23:29', '2024-01-22 22:44:32', NULL);
INSERT INTO `inspections` VALUES (10, 2, 2, 2, 1, 1, 1, 1, 2, '2024-02-0002', 1, 1, '2024-02-20 22:30:12', '2024-02-20 22:38:57', NULL);

-- ----------------------------
-- Table structure for methods
-- ----------------------------
DROP TABLE IF EXISTS `methods`;
CREATE TABLE `methods`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `methods to users`(`user_id` ASC) USING BTREE,
  INDEX `methods to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `methods to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `methods to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of methods
-- ----------------------------
INSERT INTO `methods` VALUES (1, 1, 1, 'Cash', '2024-01-16 09:04:41', '2024-01-22 18:09:19', NULL);
INSERT INTO `methods` VALUES (2, 1, 1, 'Check', '2024-01-16 09:04:58', '2024-01-22 21:24:12', NULL);

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `stocks` double NOT NULL,
  `days` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `notifications to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `notifications to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notifications
-- ----------------------------

-- ----------------------------
-- Table structure for offices
-- ----------------------------
DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NULL DEFAULT NULL,
  `office` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `office_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `telephone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `offices to users`(`user_id` ASC) USING BTREE,
  INDEX `offices to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `offices to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `offices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of offices
-- ----------------------------
INSERT INTO `offices` VALUES (1, 1, 1, 'Accounting Unit', 'AUS', '+63(0) 0000-0000', '2024-01-16 11:56:29', '2024-01-22 19:30:20', NULL);
INSERT INTO `offices` VALUES (2, 2, 1, 'Testo', 'TST', '+63(0) 0000-0001', '2024-02-20 22:49:03', '2024-02-20 23:58:45', NULL);

-- ----------------------------
-- Table structure for officials
-- ----------------------------
DROP TABLE IF EXISTS `officials`;
CREATE TABLE `officials`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `official` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `position_id` bigint UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `officials to users`(`user_id` ASC) USING BTREE,
  INDEX `officials to divisions`(`division_id` ASC) USING BTREE,
  INDEX `officials to positions`(`position_id` ASC) USING BTREE,
  CONSTRAINT `officials to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `officials to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `officials to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of officials
-- ----------------------------
INSERT INTO `officials` VALUES (2, 2, 1, 'Jeff', 1, '2024-01-16 13:20:16', '2024-02-21 00:02:16', NULL);

-- ----------------------------
-- Table structure for order_supplies
-- ----------------------------
DROP TABLE IF EXISTS `order_supplies`;
CREATE TABLE `order_supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests_suppliers to requests`(`order_id` ASC) USING BTREE,
  INDEX `requests_suppliers to supplies`(`supply_id` ASC) USING BTREE,
  CONSTRAINT `order_supplies to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_supplies to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order_supplies
-- ----------------------------
INSERT INTO `order_supplies` VALUES (1, 1, 13, 0, 2, 0, '2024-01-22 22:19:14', '2024-01-22 22:19:14', NULL);
INSERT INTO `order_supplies` VALUES (2, 1, 41, 0, 2, 0, '2024-01-22 22:19:14', '2024-01-22 22:19:14', NULL);
INSERT INTO `order_supplies` VALUES (3, 1, 11, 0, 2, 0, '2024-01-22 22:19:14', '2024-01-22 22:19:14', NULL);
INSERT INTO `order_supplies` VALUES (4, 2, 32, 0, 1, 0, '2024-02-20 21:57:43', '2024-02-20 21:57:43', NULL);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `request_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `fund_cluster_id` bigint UNSIGNED NOT NULL,
  `supplier_id` bigint UNSIGNED NOT NULL,
  `method_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `place_of_delivery` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `orders to users`(`user_id` ASC) USING BTREE,
  INDEX `orders to requests`(`request_id` ASC) USING BTREE,
  INDEX `orders to divisions`(`division_id` ASC) USING BTREE,
  INDEX `orders to offices`(`office_id` ASC) USING BTREE,
  INDEX `orders to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `orders to suppliers`(`supplier_id` ASC) USING BTREE,
  INDEX `orders to methods`(`method_id` ASC) USING BTREE,
  CONSTRAINT `orders to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to methods` FOREIGN KEY (`method_id`) REFERENCES `methods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 1, 1, 1, 1, 1, 1, 1, '2024-01-0001', '<p>aaaa</p>', '2024-01-22 22:19:14', '2024-01-22 22:38:03', NULL);
INSERT INTO `orders` VALUES (2, 2, 2, 1, 1, 1, 1, 2, '2024-02-0002', '<p>division office dso</p>', '2024-02-20 21:57:43', '2024-02-20 21:57:43', NULL);

-- ----------------------------
-- Table structure for parts
-- ----------------------------
DROP TABLE IF EXISTS `parts`;
CREATE TABLE `parts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `plan_id` bigint UNSIGNED NOT NULL,
  `part` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parts to plans`(`plan_id` ASC) USING BTREE,
  INDEX `parts_to_users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `parts to plan` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `parts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of parts
-- ----------------------------
INSERT INTO `parts` VALUES (1, 1, 1, 'Part 1', '2024-01-17 13:54:02', '2024-01-31 17:44:30', NULL);
INSERT INTO `parts` VALUES (2, 1, 1, 'Part-1', '2024-01-17 14:09:47', '2024-01-17 14:09:47', '2024-01-17 06:10:00');

-- ----------------------------
-- Table structure for plan_details
-- ----------------------------
DROP TABLE IF EXISTS `plan_details`;
CREATE TABLE `plan_details`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `plan_id` bigint UNSIGNED NOT NULL,
  `plan_title_id` bigint UNSIGNED NOT NULL,
  `part_id` bigint UNSIGNED NOT NULL,
  `supply_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `supply_id` bigint NOT NULL,
  `quantity` double NOT NULL,
  `price` double NOT NULL,
  `total` double NOT NULL,
  `january` double NOT NULL,
  `february` double NOT NULL,
  `march` double NOT NULL,
  `quarter_one` double NOT NULL,
  `april` double NOT NULL,
  `may` double NOT NULL,
  `june` double NOT NULL,
  `quarter_two` double NOT NULL,
  `july` double NOT NULL,
  `august` double NOT NULL,
  `september` double NOT NULL,
  `quarter_three` double NOT NULL,
  `october` double NOT NULL,
  `november` double NOT NULL,
  `december` double NOT NULL,
  `quarter_four` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `plandetails to plans`(`plan_id` ASC) USING BTREE,
  INDEX `plandetails to plan_titles`(`plan_title_id` ASC) USING BTREE,
  INDEX `plandetails to parts`(`part_id` ASC) USING BTREE,
  INDEX `plan_details to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `plan_details to parts` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_details to plan_titles` FOREIGN KEY (`plan_title_id`) REFERENCES `plan_titles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_details to plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_details to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plan_details
-- ----------------------------
INSERT INTO `plan_details` VALUES (1, 1, 1, 2, 1, '43211503-LAP004', 98, 78, 42380, 3305640, 1, 2, 3, 6, 4, 5, 6, 15, 7, 8, 9, 24, 10, 11, 12, 33, '2024-01-18 17:00:54', '2024-01-18 11:32:21', '2024-01-18 09:00:54');
INSERT INTO `plan_details` VALUES (2, 1, 1, 2, 1, '80141505-TS-044', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2024-01-18 17:06:22', '2024-01-18 16:32:58', '2024-01-18 09:06:22');
INSERT INTO `plan_details` VALUES (3, 1, 1, 1, 1, '12191601-AL-E03', 175, 3, 451.36, 1354.08, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, '2024-01-18 17:03:25', '2024-01-18 16:34:54', '2024-01-18 09:03:25');
INSERT INTO `plan_details` VALUES (4, 1, 1, 1, 1, '12191601-AL-E04', 199, 2, 50.96, 101.92, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '2024-01-18 17:15:19', '2024-01-18 17:15:19', NULL);
INSERT INTO `plan_details` VALUES (5, 1, 1, 1, 1, '80141505-TS-001', 62, 2, 0, 0, 1, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2024-01-18 17:16:22', '2024-01-18 17:16:22', NULL);
INSERT INTO `plan_details` VALUES (6, 1, 1, 2, 1, '80141505-TS-044', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2024-01-19 11:22:00', '2024-01-18 17:27:13', '2024-01-19 03:22:00');
INSERT INTO `plan_details` VALUES (7, 1, 1, 2, 1, '12191601-AL-E03', 175, 9, 451.36, 4062.24, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 3, 0, 0, 0, 0, '2024-01-19 13:13:40', '2024-01-19 12:27:47', '2024-01-19 05:13:40');
INSERT INTO `plan_details` VALUES (8, 1, 1, 2, 1, '80141505-TS-044', 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2024-01-19 13:36:16', '2024-01-19 13:36:16', NULL);
INSERT INTO `plan_details` VALUES (9, 2, 1, 1, 1, '44101807-CA-C01', 79, 2, 287.04, 574.08, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2024-02-20 20:58:40', '2024-02-20 20:58:40', NULL);

-- ----------------------------
-- Table structure for plan_titles
-- ----------------------------
DROP TABLE IF EXISTS `plan_titles`;
CREATE TABLE `plan_titles`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `plan_id` bigint UNSIGNED NOT NULL,
  `part_id` bigint UNSIGNED NOT NULL,
  `plan_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `plan_title to plans`(`plan_id` ASC) USING BTREE,
  INDEX `plan_titles to parts`(`part_id` ASC) USING BTREE,
  INDEX `plan_titles_to_users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `plan_titles to parts` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_titles to plan` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_titles to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plan_titles
-- ----------------------------
INSERT INTO `plan_titles` VALUES (1, 1, 1, 1, 'ALCOHOL OR ACETORE', '2024-01-17 14:34:43', '2024-01-17 14:34:43', NULL);
INSERT INTO `plan_titles` VALUES (2, 1, 1, 1, 'Technical Support', '2024-01-18 11:29:50', '2024-01-18 11:29:50', NULL);

-- ----------------------------
-- Table structure for plans
-- ----------------------------
DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `plan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `plans_to_users`(`user_id` ASC) USING BTREE,
  INDEX `plans to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `plans to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plans to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plans
-- ----------------------------
INSERT INTO `plans` VALUES (1, 1, 1, 'Plan 1', '<p>Plan for 2024</p>', 1, '2024-01-17 10:46:11', '2024-02-20 22:58:05', NULL);

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `positions to users`(`user_id` ASC) USING BTREE,
  INDEX `positions to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `positions to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `positions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO `positions` VALUES (1, 1, 1, 'Head', '2024-01-16 12:30:32', '2024-01-16 12:30:41', NULL);
INSERT INTO `positions` VALUES (4, 2, 1, 'Secretary', '2024-01-22 17:01:12', '2024-02-20 23:59:40', NULL);

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `profiles to users`(`user_id` ASC) USING BTREE,
  INDEX `profiles to divisions`(`division_id` ASC) USING BTREE,
  INDEX `profiles to office_id`(`office_id` ASC) USING BTREE,
  CONSTRAINT `profiles to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `profiles to office_id` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `profiles to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES (1, 2, 1, 1, '2024-01-24 19:43:22', '2024-01-24 19:43:22', NULL);
INSERT INTO `profiles` VALUES (3, 3, 1, 1, '2024-02-20 20:13:09', '2024-02-20 20:13:09', NULL);

-- ----------------------------
-- Table structure for ranges
-- ----------------------------
DROP TABLE IF EXISTS `ranges`;
CREATE TABLE `ranges`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `start_price` double NOT NULL,
  `end_price` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ranges to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `ranges to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ranges
-- ----------------------------
INSERT INTO `ranges` VALUES (1, 1, 'SPLV', 0, 5000, '2024-01-15 22:42:47', '2024-01-23 18:07:30', NULL);
INSERT INTO `ranges` VALUES (2, 1, 'SPHV', 5001, 50000, '2024-01-15 22:53:25', '2024-01-15 22:53:34', NULL);

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-01-15 13:08:38', '2024-01-15 13:08:38', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (8, '2024-02-07 18:16:45', '2024-02-07 18:16:45', 'Users', '2', '6e0fca5bd6c04cfe820725debf1ba4812e78fbc1', 'adad883de85b62088578e3c7816210b89f628d6c', '2024-03-08 18:16:45');
INSERT INTO `remember_me_tokens` VALUES (18, '2024-02-21 00:06:51', '2024-02-21 00:06:51', 'Users', '1', 'e9366bf81fc28c317088b203a18588112a96e62c', 'ff79913dc07031c936aac49e962800071629da9c', '2024-03-22 00:06:51');

-- ----------------------------
-- Table structure for request_supplies
-- ----------------------------
DROP TABLE IF EXISTS `request_supplies`;
CREATE TABLE `request_supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests_suppliers to requests`(`request_id` ASC) USING BTREE,
  INDEX `requests_suppliers to supplies`(`supply_id` ASC) USING BTREE,
  CONSTRAINT `requests_suppliers to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests_suppliers to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request_supplies
-- ----------------------------
INSERT INTO `request_supplies` VALUES (1, 1, 13, 0, 2, 0, '2024-01-22 21:34:10', '2024-01-22 21:34:10', NULL);
INSERT INTO `request_supplies` VALUES (2, 1, 41, 0, 2, 0, '2024-01-22 21:34:10', '2024-01-22 21:34:10', NULL);
INSERT INTO `request_supplies` VALUES (3, 1, 11, 0, 2, 0, '2024-01-22 21:34:10', '2024-01-22 21:34:10', NULL);
INSERT INTO `request_supplies` VALUES (4, 2, 32, 0, 1, 0, '2024-02-20 21:46:02', '2024-02-20 21:46:02', NULL);

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `fund_cluster_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `purpose` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_approved` tinyint NOT NULL DEFAULT 0,
  `approved_at` datetime NULL DEFAULT NULL,
  `is_declined` tinyint NOT NULL DEFAULT 0,
  `declined_at` datetime NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests to users`(`user_id` ASC) USING BTREE,
  INDEX `requests to divisions`(`division_id` ASC) USING BTREE,
  INDEX `requests to offices`(`office_id` ASC) USING BTREE,
  INDEX `requests to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  CONSTRAINT `requests to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES (1, 1, 1, 1, 1, '2024-01-0001', '<p>asda</p>', 1, '2024-01-22 14:19:14', 0, NULL, '2024-02-06 21:34:10', '2024-01-22 22:30:27', NULL);
INSERT INTO `requests` VALUES (2, 2, 1, 1, 1, '2024-02-0002', '<p>tewst samplke</p>', 1, '2024-02-20 13:57:42', 0, NULL, '2024-02-20 21:46:02', '2024-02-20 21:57:43', NULL);

-- ----------------------------
-- Table structure for requisition_supplies
-- ----------------------------
DROP TABLE IF EXISTS `requisition_supplies`;
CREATE TABLE `requisition_supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `requisition_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests_suppliers to requests`(`requisition_id` ASC) USING BTREE,
  INDEX `requests_suppliers to supplies`(`supply_id` ASC) USING BTREE,
  CONSTRAINT `requisition_supplies to requisition` FOREIGN KEY (`requisition_id`) REFERENCES `requisitions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisition_supplies to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requisition_supplies
-- ----------------------------
INSERT INTO `requisition_supplies` VALUES (1, 1, 13, 0, 2, 0, '2024-01-22 22:27:47', '2024-01-22 22:27:47', NULL);
INSERT INTO `requisition_supplies` VALUES (2, 1, 41, 0, 2, 0, '2024-01-22 22:27:48', '2024-01-22 22:27:48', NULL);
INSERT INTO `requisition_supplies` VALUES (3, 1, 11, 0, 2, 0, '2024-01-22 22:27:48', '2024-01-22 22:27:48', NULL);
INSERT INTO `requisition_supplies` VALUES (4, 2, 32, 0, 1, 0, '2024-02-20 22:39:36', '2024-02-20 22:39:36', NULL);

-- ----------------------------
-- Table structure for requisitions
-- ----------------------------
DROP TABLE IF EXISTS `requisitions`;
CREATE TABLE `requisitions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `request_id` bigint UNSIGNED NOT NULL,
  `order_id` bigint UNSIGNED NOT NULL,
  `inspection_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `fund_cluster_id` bigint UNSIGNED NOT NULL,
  `supplier_id` bigint UNSIGNED NOT NULL,
  `method_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requisitions to users`(`user_id` ASC) USING BTREE,
  INDEX `requisitions to requests`(`request_id` ASC) USING BTREE,
  INDEX `requisitions to orders`(`order_id` ASC) USING BTREE,
  INDEX `requisitions to inspections`(`inspection_id` ASC) USING BTREE,
  INDEX `requisitions to divisions`(`division_id` ASC) USING BTREE,
  INDEX `requisitions to offices`(`office_id` ASC) USING BTREE,
  INDEX `requisitions to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `requisitions to suppliers`(`supplier_id` ASC) USING BTREE,
  INDEX `requisitions to methods`(`method_id` ASC) USING BTREE,
  CONSTRAINT `requisitions to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to inspections` FOREIGN KEY (`inspection_id`) REFERENCES `inspections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to methods` FOREIGN KEY (`method_id`) REFERENCES `methods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requisitions
-- ----------------------------
INSERT INTO `requisitions` VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2024-01-0001', '2024-01-22 22:27:47', '2024-01-22 22:50:45', NULL);
INSERT INTO `requisitions` VALUES (2, 2, 2, 2, 10, 1, 1, 1, 1, 2, '2024-02-0002', '2024-02-20 22:39:36', '2024-02-20 22:39:36', NULL);

-- ----------------------------
-- Table structure for serieses
-- ----------------------------
DROP TABLE IF EXISTS `serieses`;
CREATE TABLE `serieses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_id` bigint UNSIGNED NOT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_expendable` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `serieses to applications`(`application_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of serieses
-- ----------------------------

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `supply_id` bigint UNSIGNED NOT NULL,
  `quantity` double NOT NULL,
  `is_in` tinyint NOT NULL,
  `total_stocks` double NOT NULL,
  `previous_stocks` double NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stocks to users`(`user_id` ASC) USING BTREE,
  INDEX `stocks to supplies`(`supply_id` ASC) USING BTREE,
  CONSTRAINT `stocks to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `stocks to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stocks
-- ----------------------------
INSERT INTO `stocks` VALUES (2, 2, 32, 1, 0, 1, 0, 'PROCUREMENT (IN)', '2024-02-20 22:30:12', '2024-02-20 22:30:12', NULL);

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tin_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `suppliers to users`(`user_id` ASC) USING BTREE,
  INDEX `suppliers to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `suppliers to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `suppliers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (1, 1, 1, 'FARMACIA NAVARRO, INC.', '941-104-334-000', 'Santiago City', '2024-01-16 10:26:00', '2024-01-22 21:05:50', NULL);

-- ----------------------------
-- Table structure for supplies
-- ----------------------------
DROP TABLE IF EXISTS `supplies`;
CREATE TABLE `supplies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `supply_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `category_id` bigint UNSIGNED NOT NULL,
  `unit_id` bigint UNSIGNED NOT NULL,
  `range_id` bigint UNSIGNED NOT NULL,
  `supply_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `stock` double NOT NULL,
  `price` double NOT NULL,
  `in_stock` tinyint NOT NULL,
  `is_expendable` tinyint NOT NULL,
  `expired_at` date NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `supplies to users`(`user_id` ASC) USING BTREE,
  INDEX `supplies to units`(`unit_id` ASC) USING BTREE,
  INDEX `supplies to ranges`(`range_id` ASC) USING BTREE,
  INDEX `supplies to categories`(`category_id` ASC) USING BTREE,
  INDEX `supplies to divisions`(`division_id` ASC) USING BTREE,
  CONSTRAINT `supplies to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to ranges` FOREIGN KEY (`range_id`) REFERENCES `ranges` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of supplies
-- ----------------------------
INSERT INTO `supplies` VALUES (1, 2, 'SPEAKERS', '<p>-</p>', 1, 1, 1, 1, '80141505-TS-044', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-02-20 20:19:36', NULL);
INSERT INTO `supplies` VALUES (2, 1, 'PHOTO PAPER', '-', 1, 1, 2, 1, '80141505-TS-006', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (3, 1, 'AUTOMOTIVE BATTERIES', '-', 1, 1, 3, 1, '80141505-TS-032', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (4, 1, 'MINI BUS', '-', 1, 1, 1, 1, '80141505-TS-065', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (5, 1, 'SMART TELEVISION', '-', 1, 1, 1, 2, '80141505-TS-012', 0, 8500, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (6, 1, 'WHITE BOARD', '-', 1, 1, 3, 1, '80141505-TS-013', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (7, 1, 'VAN', '-', 1, 1, 1, 1, '80141505-TS-057', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (8, 1, 'KEYBOARD', '-', 1, 1, 1, 1, '80141505-TS-046', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (9, 1, 'VELLUM BOARD PAPER', '-', 1, 1, 2, 1, '80141505-TS-019', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (10, 1, 'PADLOCK', '-', 1, 1, 3, 1, '80141505-TS-033', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (11, 1, 'STEEL RACK', '-', 1, 1, 4, 1, '80141505-TS-048', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (12, 1, 'MICROPHONE', '-', 1, 1, 1, 1, '80141505-TS-014', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (13, 1, 'MEDAL', '-', 1, 1, 3, 1, '80141505-TS-015', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (14, 1, 'TRI-WHEEL VEHICLE', '-', 1, 1, 1, 1, '80141505-TS-058', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (15, 1, 'DISPOSABLE GLOVES', '-', 1, 1, 5, 1, '80141505-TS-029', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (16, 1, 'DSLR CAMERA', '-', 1, 1, 1, 1, '80141505-TS-047', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (17, 1, 'COLORED PAPER', '-', 1, 1, 2, 1, '80141505-TS-022', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (18, 1, 'COMPACT DISC', '-', 1, 1, 3, 1, '80141505-TS-035', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (19, 1, 'PAINT', '-', 1, 1, 6, 1, '80141505-TS-021', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (20, 1, 'MOBILE PHONE', '-', 1, 1, 1, 1, '80141505-TS-034', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (21, 1, 'CERTIFICATE FRAME', '-', 1, 1, 7, 2, '80141505-TS-008', 0, 150.25, 0, 1, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (22, 1, 'STEEL FILING CABINET', '-', 1, 1, 3, 2, '80141505-TS-018', 0, 8500, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (23, 1, 'PICK-UP TRUCK', '-', 1, 1, 1, 1, '80141505-TS-059', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (24, 1, 'BOND PAPER', '-', 1, 1, 8, 1, '80141505-TS-023', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (25, 1, 'LAMINATING MACHINE', '-', 1, 1, 1, 1, '80141505-TS-049', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (26, 1, 'PUSH PIN', '-', 1, 1, 2, 1, '80141505-TS-024', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (27, 1, 'BLEACHING SOLUTION', '-', 1, 1, 9, 1, '80141505-TS-007', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (28, 1, 'STORAGE BOX', '-', 1, 1, 3, 1, '80141505-TS-037', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (29, 1, 'AMPLIFIER', '-', 1, 1, 1, 1, '80141505-TS-036', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (30, 1, 'CERTIFICATE HOLDER', '-', 1, 1, 7, 2, '80141505-TS-009', 0, 45, 0, 1, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (31, 1, 'EXTENSION CORD', '-', 1, 1, 3, 1, '80141505-TS-020', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (32, 1, 'ALL-TERRAIN VEHICLE (ATV)', '-', 1, 1, 1, 1, '80141505-TS-060', 1, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-02-20 22:30:12', NULL);
INSERT INTO `supplies` VALUES (33, 1, 'CAR (SEDAN OR HATCHBACK)', '-', 1, 1, 1, 1, '80141505-TS-053', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (34, 1, 'GLUE STICK (FOR GLUE GUN)', '-', 1, 1, 2, 1, '80141505-TS-027', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (35, 1, 'DISHWASHING LIQUID', '-', 1, 1, 9, 1, '80141505-TS-016', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (36, 1, 'PLASTIC ENVELOPE', '-', 1, 1, 3, 1, '80141505-TS-038', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (37, 1, 'DOCUMENT SCANNER', '-', 1, 1, 1, 1, '80141505-TS-039', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (38, 1, 'PLASTIC FASTENER', '-', 1, 1, 5, 2, '80141505-TS-017', 0, 0, 0, 1, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (39, 1, 'DOOR MAT', '-', 1, 1, 3, 1, '80141505-TS-025', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (40, 1, 'ASSEMBLED OWNER-TYPE JEEP', '-', 1, 1, 1, 1, '80141505-TS-061', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (41, 1, 'GLUE STICK (PASTE)', '-', 1, 1, 3, 1, '80141505-TS-026', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (42, 1, 'ASSEMBLED PASSENGER JEEPNEY-TYPE VEHICLE', '-', 1, 1, 1, 1, '80141505-TS-062', 0, 0, 0, 0, NULL, '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `supplies` VALUES (43, 1, 'MULTIFUNCTION PRINTER', '-', 1, 1, 1, 1, '80141505-TS-002', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (44, 1, 'MULTI-PURPOSE VEHICLE (MPV)', '-', 1, 1, 1, 1, '80141505-TS-054', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (45, 1, 'MANILA PAPER', '-', 1, 1, 2, 1, '80141505-TS-031', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (46, 1, 'POVIDONE IODINE', '-', 1, 1, 9, 1, '80141505-TS-045', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (47, 1, 'FUEL FILTERS', '-', 1, 1, 3, 1, '80141505-TS-050', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (48, 1, 'WATER DISPENSER', '-', 1, 1, 1, 1, '80141505-TS-040', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (49, 1, 'LAMINATING FILM', '-', 1, 1, 2, 1, '80141505-TS-003', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (50, 1, 'DEODORANT CAKE', '-', 1, 1, 3, 1, '80141505-TS-028', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (51, 1, 'MOTORCYCLE', '-', 1, 1, 1, 1, '80141505-TS-063', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (52, 1, 'UNINTERRUPTIBLE POWER SUPPLY', '-', 1, 1, 1, 1, '80141505-TS-010', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (53, 1, 'SPORTS UTILITY VEHICLE (SUV)', '-', 1, 1, 1, 1, '80141505-TS-055', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (54, 1, 'PARACETAMOL', '-', 1, 1, 2, 1, '80141505-TS-041', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (55, 1, 'GLUE GUN', '-', 1, 1, 1, 1, '80141505-TS-042', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (56, 1, 'INKJET PRINTER', '-', 1, 1, 1, 1, '80141505-TS-043', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (57, 1, 'STICKER PAPER ', '-', 1, 1, 2, 1, '80141505-TS-004', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (58, 1, 'OFFICE CHAIR', '-', 1, 1, 3, 1, '80141505-TS-030', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (59, 1, 'BUS', '-', 1, 1, 1, 1, '80141505-TS-064', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (60, 1, 'DOUBLE SIDED TAPE', '-', 1, 1, 10, 1, '80141505-TS-005', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (61, 1, 'AIR CONDITIONING UNIT', '-', 1, 1, 1, 1, '80141505-TS-011', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (62, 1, 'BALLPEN', '-', 1, 1, 3, 1, '80141505-TS-001', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (63, 1, 'PASSENGER VAN', '-', 1, 1, 1, 1, '80141505-TS-056', 0, 0, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (64, 1, 'COMPUTER CONTINUOUS FORM, 3 PLY, 280MM X 378MM', '-', 1, 1, 5, 1, '14111506-CF-L32', 0, 1629.68, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (65, 1, 'PUNCHER, PAPER, HEAVY DUTY', '-', 1, 1, 3, 1, '44101602-PU-P01', 0, 162.24, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (66, 1, 'MARKER, FLOURESCENT', '-', 1, 1, 4, 1, '44121716-MA-F01', 0, 42.64, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (67, 1, 'STAPLE WIRE, STANDARD', '-', 1, 1, 5, 1, '31151804-SW-S01', 0, 23.76, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (68, 1, 'DESKTOP, FOR MID-RANGE USERS', '-', 1, 1, 1, 2, '43211507-DSK004', 0, 42390.4, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (69, 1, 'PAPER CLIP, VINLY/PLASTIC COATED, JUMBO, 50MM', '-', 1, 1, 5, 1, '44122104-PC-J02', 0, 23.92, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (70, 1, 'MAGAZINE FILE BOX, LARGE', '-', 1, 1, 3, 1, '44111515-MF-B02', 0, 126.88, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (71, 1, 'FOLDER, L-TYPE, LEGAL', '-', 1, 1, 2, 1, '44122011-FO-L02', 0, 238.57, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (72, 1, 'TAPE, ELECTRICAL', '-', 1, 1, 10, 1, '31201502-TA-E01', 0, 19.45, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (73, 1, 'PAPER, MULTIPURPOSE A4', '-', 1, 1, 8, 1, '14111507-PP-C01', 0, 163.28, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (74, 1, 'FIRE EXTINGUISHER, PURE HCFC', '-', 1, 1, 1, 2, '46191601-FE-H01', 0, 5613.25, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (75, 1, 'ENVELOPE, EXPANDING, KRAFT', '-', 1, 1, 5, 1, '44121506-EN-X01', 0, 1374.88, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (76, 1, 'LIGHT-EMITTING DIODE (LED) LINEAR TUBE, 18 WATTS', '-', 1, 1, 3, 1, '39101628-LT-L01', 0, 205.82, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (77, 1, 'BINDING AND PUNCHING MACHINE', '-', 1, 1, 1, 2, '44101602-PB-M01', 0, 10104.64, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (78, 1, 'RULER, FLEXIBLE, PLASTIC, 450MM', '-', 1, 1, 3, 1, '41111604-RU-P02', 0, 20.8, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (79, 1, 'CALCULATOR, COMPACT', '-', 1, 1, 1, 1, '44101807-CA-C01', 0, 287.04, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (80, 1, 'PAPER, PARCHMENT', '-', 1, 1, 5, 1, '14111503-PA-P01', 0, 189.28, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (81, 1, 'BINDING RING/COMB, PLASTIC, 32 MM', '-', 1, 1, 3, 1, '44122037-RB-P10', 0, 274.23, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (82, 1, 'CHALK, WHITE ENAMEL', '-', 1, 1, 5, 1, '44121710-CH-W01', 0, 32.97, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (83, 1, 'EXTERNAL HARD DRIVE', '-', 1, 1, 1, 1, '43201827-HD-X02', 0, 3018.08, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (84, 1, 'PENCIL, LEAD/GRAPHITE, WITH ERASER', '-', 1, 1, 5, 1, '44121706-PE-L01', 0, 41.6, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (85, 1, 'MARKER, PERMANENT, BLACK', '-', 1, 1, 3, 1, '44121708-MP-B01', 0, 15.6, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (86, 1, 'FOLDER WITH TAB, A4', '-', 1, 1, 2, 1, '44122011-FO-T03', 0, 388.96, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (87, 1, 'TAPE, MASKING, 24MM', '-', 1, 1, 10, 1, '31201503-TA-M01', 0, 61.88, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (88, 1, 'PAPER, MULTIPURPOSE LEGAL', '-', 1, 1, 8, 1, '14111507-PP-C02', 0, 185.12, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (89, 1, 'MONOBLOC TABLE, WHITE', '-', 1, 1, 1, 1, '56101519-TM-S01', 0, 1348.88, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (90, 1, 'ENVELOPE, EXPANDING, PLASTIC', '-', 1, 1, 5, 1, '44121506-EN-X02', 0, 30.49, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (91, 1, 'ENVELOPE, MAILING', '-', 1, 1, 5, 1, '44121506-EN-M02', 0, 466.96, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (92, 1, 'CORRECTION TAPE', '-', 1, 1, 3, 1, '44121801-CT-R02', 0, 11.53, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (93, 1, 'PAPER SHREDDER', '-', 1, 1, 1, 2, '44101603-PS-M02', 0, 5699.2, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (94, 1, 'STAMP PAD, FELT', '-', 1, 1, 3, 1, '44121905-SP-F01', 0, 42.64, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (95, 1, 'HAND SANITIZER', '-', 1, 1, 9, 1, '53131626-HS-S01', 0, 87.36, 0, 0, NULL, '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `supplies` VALUES (96, 1, 'BLADE, FOR GENERAL PURPOSE CUTTER/UTILITY KNIFE', '-', 1, 1, 11, 1, '44121612-BL-H01', 0, 16, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (97, 1, 'CLIP, BACKFOLD, 19MM', '-', 1, 1, 5, 1, '44122105-BF-C01', 0, 9.36, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (98, 1, 'LAPTOP, FOR MID-RANGE USERS', '-', 1, 1, 1, 2, '43211503-LAP004', 0, 42380, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (99, 1, 'RUBBER BAND NO. 18', '-', 1, 1, 5, 1, '44122101-RU-B01', 0, 165.36, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (100, 1, 'MARKER, PERMANENT, BLUE', '-', 1, 1, 3, 1, '44121708-MP-B02', 0, 15.6, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (101, 1, 'FOLDER WITH TAB, LEGAL', '-', 1, 1, 2, 1, '44122011-FO-T04', 0, 427.44, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (102, 1, 'TAPE, MASKING, 48 MM', '-', 1, 1, 10, 1, '31201503-TA-M02', 0, 121.16, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (103, 1, 'MONOBLOC TABLE, BEIGE', '-', 1, 1, 1, 1, '56101519-TM-S02', 0, 1348.88, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (104, 1, 'ELECTRIC FAN, CEILING MOUNT, ORBIT TYPE', '-', 1, 1, 1, 1, '40101604-EF-C01', 0, 1380.08, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (105, 1, 'ENVELOPE, MAILING, WITH WINDOW', '-', 1, 1, 5, 1, '44121504-EN-W02', 0, 528.32, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (106, 1, 'CUTTER/UTILITY KNIFE, FOR GENERAL PURPOSE', '-', 1, 1, 3, 1, '44121612-CU-H01', 0, 31.72, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (107, 1, 'PAPER TRIMMER/CUTTING MACHINE', '-', 1, 1, 1, 2, '44101601-PT-M01', 0, 9297.6, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (108, 1, 'STAPLER, STANDARD TYPE', '-', 1, 1, 3, 1, '44121615-ST-S01', 0, 145.6, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (109, 1, 'CLIP, BACKFOLD, 25MM', '-', 1, 1, 5, 1, '44122105-BF-C02', 0, 15.6, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (110, 1, 'PHILIPPINE NATIONAL FLAG', '-', 1, 1, 3, 1, '55121905-PH-F01', 0, 284.84, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (111, 1, 'LAPTOP, LIGHTWEIGHT', '-', 1, 1, 1, 2, '43211503-LAP003', 0, 46800, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (112, 1, 'COMPUTER CONTINUOUS FORM, 1 PLY, 280MM X 241MM', '-', 1, 1, 5, 1, '14111506-CF-L11', 0, 953.68, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (113, 1, 'MARKER, PERMANENT, RED', '-', 1, 1, 3, 1, '44121708-MP-B03', 0, 15.6, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (114, 1, 'CARTOLINA, ASSORTED COLORS', '-', 1, 1, 2, 1, '14111525-CA-A01', 0, 90.48, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (115, 1, 'TAPE, PACKAGING, 48 MM', '-', 1, 1, 10, 1, '31201517-TA-P01', 0, 30.16, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (116, 1, 'TAPE, TRANSPARENT, 24MM', '-', 1, 1, 10, 1, '31201512-TA-T01', 0, 11.44, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (117, 1, 'ELECTRIC FAN, INDUSTRIAL, GROUND TYPE', '-', 1, 1, 1, 1, '40101604-EF-G01', 0, 1109.68, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (118, 1, 'FASTENER', '-', 1, 1, 5, 1, '44122118-FA-P01', 0, 94.64, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (119, 1, 'DATA FILE BOX', '-', 1, 1, 3, 1, '44111515-DF-B01', 0, 77.2, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (120, 1, 'STAPLER, HEAVY DUTY (BINDER)', '-', 1, 1, 1, 1, '44121615-ST-B01', 0, 954.72, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (121, 1, 'STAPLE REMOVER, PLIER-TYPE', '-', 1, 1, 3, 1, '44121613-SR-P02', 0, 47.84, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (122, 1, 'CLIP, BACKFOLD, 32MM', '-', 1, 1, 5, 1, '44122105-BF-C03', 0, 27.04, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (123, 1, 'MONOBLOC CHAIR, BEIGE', '-', 1, 1, 3, 1, '56101504-CM-B01', 0, 375.44, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (124, 1, 'COMPUTER MOUSE, WIRELESS', '-', 1, 1, 1, 1, '43211708-MO-O02', 0, 169.52, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (125, 1, 'COMPUTER CONTINUOUS FORM, 1 PLY, 280MM X 378MM', '-', 1, 1, 5, 1, '14111506-CF-L12', 0, 1260.48, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (126, 1, 'MARKER, WHITEBOARD, BLACK', '-', 1, 1, 3, 1, '44121708-MW-B01', 0, 10.4, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (127, 1, 'TISSUE, INTERFOLDED PAPER TOWEL', '-', 1, 1, 2, 1, '14111704-TT-P04', 0, 29.12, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (128, 1, 'TAPE, TRANSPARENT, 48 MM', '-', 1, 1, 10, 1, '31201512-TA-T02', 0, 23.92, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (129, 1, 'FACE MASK', '-', 1, 1, 5, 1, '42131713-SM-M06', 8, 52, 1, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (130, 1, 'ELECTRIC FAN, STAND TYPE', '-', 1, 1, 1, 1, '40101604-EF-S01', 0, 1006.72, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (131, 1, 'FOLDER, PRESSBOARD', '-', 1, 1, 5, 1, '44122027-FO-P01', 0, 952.64, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (132, 1, 'DATA FOLDER', '-', 1, 1, 3, 1, '44122011-DF-F01', 0, 68.64, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (133, 1, 'FILE TAB DIVIDER, A4', '-', 1, 1, 2, 1, '44122018-FT-D01', 0, 22.88, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (134, 1, 'TAPE DISPENSER, TABLE TOP', '-', 1, 1, 3, 1, '44121605-TD-T01', 0, 78, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (135, 1, 'CLIP, BACKFOLD, 50MM', '-', 1, 1, 5, 1, '44122105-BF-C04', 0, 60.32, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (136, 1, 'MONOBLOC CHAIR, WHITE', '-', 1, 1, 3, 1, '56101504-CM-W01', 0, 372.32, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (137, 1, 'PRINTER, IMPACT, DOT MATRIX, 24 PINS', '-', 1, 1, 1, 2, '43212102-PR-D02', 0, 33015.84, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (138, 1, 'COMPUTER CONTINUOUS FORM, 2 PLY, 280MM X 378MM', '-', 1, 1, 5, 1, '14111506-CF-L22', 0, 1652.56, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (139, 1, 'MARKER, WHITEBOARD, BLUE', '-', 1, 1, 3, 1, '44121708-MW-B02', 0, 10.4, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (140, 1, 'TOILET TISSUE PAPER, 2 PLY', '-', 1, 1, 2, 1, '14111704-TT-P02', 0, 100.88, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (141, 1, 'DATER STAMP', '-', 1, 1, 7, 1, '44103202-DS-M01', 0, 469.78, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (142, 1, 'TWINE, PLASTIC', '-', 1, 1, 10, 1, '31151507-TW-P01', 0, 70.72, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (143, 1, 'CARBON FILM, LEGAL SIZE', '-', 1, 1, 5, 1, '13111201-CF-P02', 0, 256.88, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (144, 1, 'INSECTICIDE', '-', 1, 1, 12, 1, '10191509-IN-A01', 0, 257.92, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (145, 1, 'ELECTRIC FAN, WALL MOUNT', '-', 1, 1, 1, 1, '40101604-EF-W01', 0, 914.16, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (146, 1, 'INDEX TAB', '-', 1, 1, 5, 1, '44122008-IT-T01', 0, 63.11, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (147, 1, 'ERASER, FELT, FOR BLACKBOARD/WHITEBOARD', '-', 1, 1, 3, 1, '44111912-ER-B01', 0, 12.33, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (148, 1, 'FILE TAB DIVIDER, LEGAL', '-', 1, 1, 2, 1, '44122018-FT-D02', 0, 26, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (149, 1, 'STENO NOTEBOOK', '-', 1, 1, 3, 1, '14111514-NB-S02', 0, 23.92, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (150, 1, 'PAPER, MULTICOPY A4', '-', 1, 1, 8, 1, '14111507-PP-M01', 0, 179.92, 0, 0, NULL, '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `supplies` VALUES (151, 1, 'ENVELOPE, DOCUMENTARY, A4', '-', 1, 1, 5, 1, '44121506-EN-D01', 0, 840.32, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (152, 1, 'FLASH DRIVE', '-', 1, 1, 3, 1, '43202010-FD-U04', 0, 224.64, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (153, 1, 'PRINTER, IMPACT, DOT MATRIX, 9 PINS', '-', 1, 1, 1, 2, '43212102-PR-D01', 0, 11014.64, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (154, 1, 'COMPUTER CONTINUOUS FORM, 2 PLY, 280MM X 241MM', '-', 1, 1, 5, 1, '14111506-CF-L21', 0, 1108.64, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (155, 1, 'MARKER, WHITEBOARD, RED', '-', 1, 1, 3, 1, '44121708-MW-B03', 0, 10.4, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (156, 1, 'PENCIL SHARPENER', '-', 1, 1, 3, 1, '44121619-PS-M01', 0, 213.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (157, 1, 'STAPLE WIRE, HEAVY DUTY (BINDER TYPE), 23/13', '-', 1, 1, 5, 1, '31151804-SW-H01', 0, 22.55, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (158, 1, 'DESKTOP, FOR BASIC USERS', '-', 1, 1, 1, 2, '43211507-DSK003', 0, 24793.6, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (159, 1, 'PAPER CLIP, VINLY/PLASTIC COATED, 33MM', '-', 1, 1, 5, 1, '44122104-PC-G01', 0, 9.36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (160, 1, 'FILE ORGANIZER, EXPANDING, PLASTIC, LEGAL', '-', 1, 1, 3, 1, '44111515-FO-X01', 0, 85.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (161, 1, 'FOLDER, L-TYPE, A4', '-', 1, 1, 2, 1, '44122011-FO-L01', 0, 187.54, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (162, 1, 'ACETATE', '-', 1, 1, 10, 1, '13111203-AC-F01', 0, 1048.32, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-23 17:44:41', NULL);
INSERT INTO `supplies` VALUES (163, 1, 'PAPER, MULTICOPY LEGAL', '-', 1, 1, 8, 1, '14111507-PP-M02', 0, 211.12, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (164, 1, 'FIRE EXTINGUISHER, DRY CHEMICAL', '-', 1, 1, 1, 1, '46191601-FE-M01', 0, 1398.8, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (165, 1, 'ENVELOPE, DOCUMENTARY, LEGAL, ', '-', 1, 1, 5, 1, '44121506-EN-D02', 0, 1078.48, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (166, 1, 'LIGHT-EMITTING DIODE (LED) LIGHT BULB, 7 WATTS', '-', 1, 1, 3, 1, '39101628-LB-L01', 0, 81.12, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (167, 1, 'PRINTER, LASER, MONOCHROME', '-', 1, 1, 1, 2, '43212105-PR-L01', 0, 23063.04, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (168, 1, 'COMPUTER CONTINUOUS FORM, 3 PLY, 280MM X 241MM', '-', 1, 1, 5, 1, '14111506-CF-L31', 0, 853.84, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (169, 1, 'HAND SOAP, LIQUID', '-', 1, 1, 9, 1, '73101612-HS-L01', 0, 42.95, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (170, 1, 'DETERGENT POWDER, ALL PURPOSE', '-', 1, 1, 13, 1, '47131811-DE-P02', 0, 88.4, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (171, 1, 'MOPHANDLE, HEAVY DUTY', '-', 1, 1, 3, 1, '47131613-MP-H02', 0, 134.16, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (172, 1, 'BATTERY, DRY CELL, SIZE D', '-', 1, 1, 14, 1, '26111702-BT-A03', 0, 92.56, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (173, 1, 'SIGN PEN, FINE TIP, BLUE', '-', 1, 1, 3, 1, '60121524-SP-G05', 5, 44.72, 1, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (174, 1, 'INK, FOR STAMP PAD', '-', 1, 1, 9, 1, '12171703-SI-P01', 0, 31.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (175, 1, 'ALCOHOL, ETHYL,  1 GALLON', '-', 1, 1, 6, 1, '12191601-AL-E03', 0, 451.36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (176, 1, 'RAGS', '-', 1, 1, 15, 1, '47131501-RG-C01', 0, 57.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (177, 1, 'MOPHEAD, MADE OF RAYON', '-', 1, 1, 3, 1, '47131619-MP-R01', 0, 114.04, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (178, 1, 'AIR FRESHENER', '-', 1, 1, 12, 1, '47131812-AF-A01', 0, 87.36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (179, 1, 'SIGN PEN, MEDIUM TIP, BLUE', '-', 1, 1, 3, 1, '60121524-SP-G08', 0, 57.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (180, 1, 'WASTEBASKET', '-', 1, 1, 3, 1, '47121702-WB-P01', 0, 43.68, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (181, 1, 'CLEANSER, SCOURING POWDER', '-', 1, 1, 12, 1, '47131805-CL-P01', 0, 36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (182, 1, 'SIGN PEN, EXTRA FINE TIP, RED', '-', 1, 1, 3, 1, '60121524-SP-G03', 0, 26, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (183, 1, 'SIGN PEN, FINE TIP, RED', '-', 1, 1, 3, 1, '60121524-SP-G06', 0, 45.76, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (184, 1, 'WRAPPING PAPER', '-', 1, 1, 2, 1, '60121124-WR-P01', 0, 214.24, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (185, 1, 'ERASER, PLASTIC/RUBBER', '-', 1, 1, 3, 1, '60121534-ER-P01', 0, 9.36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (186, 1, 'DOCUMENT CAMERA', '-', 1, 1, 1, 2, '45121517-DO-C03', 0, 25536.16, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (187, 1, 'CLEARBOOK, A4 SIZE', '-', 1, 1, 5, 1, '60121413-CB-P01', 0, 35.36, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (188, 1, 'DISINFECTANT SPRAY', '-', 1, 1, 12, 1, '47131803-DS-A01', 0, 150.8, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (189, 1, 'CLEARBOOK, LEGAL SIZE', '-', 1, 1, 5, 1, '60121413-CB-P02', 0, 38.48, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (190, 1, 'FLOOR WAX, PASTE TYPE, RED', '-', 1, 1, 12, 1, '47131802-FW-P03', 0, 135.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (191, 1, 'SIGN PEN, MEDIUM TIP, RED', '-', 1, 1, 3, 1, '60121524-SP-G09', 0, 57.2, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (192, 1, 'SCOURING PAD', '-', 1, 1, 2, 1, '47131602-SC-N01', 0, 89.44, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (193, 1, 'SIGN PEN, EXTRA FINE TIP, BLACK ', '-', 1, 1, 3, 1, '60121524-SP-G01', 0, 26, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (194, 1, 'MULTIMEDIA PROJECTOR', '-', 1, 1, 1, 2, '45111609-MM-P01', 0, 18470.4, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (195, 1, 'FURNITURE CLEANER', '-', 1, 1, 12, 1, '47131830-FC-A01', 0, 226.72, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (196, 1, 'BROOM (WALIS TAMBO)', '-', 1, 1, 3, 1, '47131604-BR-S01', 0, 127.92, 0, 0, NULL, '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `supplies` VALUES (197, 1, 'SIGN PEN, FINE TIP, BLACK ', '-', 1, 1, 3, 1, '60121524-SP-G04', 0, 44.72, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (198, 1, 'MOP BUCKET', '-', 1, 1, 1, 1, '47121804-MP-B01', 0, 2288, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (199, 1, 'ALCOHOL, ETHYL, 500 ML', '-', 1, 1, 9, 1, '12191601-AL-E04', 0, 50.96, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (200, 1, 'BROOM (WALIS TING-TING)', '-', 1, 1, 3, 1, '47131604-BR-T01', 0, 23.92, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (201, 1, 'BATTERY, DRY CELL, SIZE AA', '-', 1, 1, 14, 1, '26111702-BT-A02', 0, 21.84, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (202, 1, 'SIGN PEN, MEDIUM TIP, BLACK ', '-', 1, 1, 3, 1, '60121524-SP-G07', 0, 57.2, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (203, 1, 'DIGITAL VOICE RECORDER', '-', 1, 1, 1, 1, '52161535-DV-R01', 0, 4803.76, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (204, 1, 'CLEANER, TOILET BOWL AND URINAL', '-', 1, 1, 9, 1, '47131829-TB-C01', 0, 44.72, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (205, 1, 'DETERGENT BAR', '-', 1, 1, 16, 1, '47131811-DE-B02', 0, 9.36, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (206, 1, 'DUST PAN', '-', 1, 1, 3, 1, '47131601-DU-P01', 0, 46.8, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (207, 1, 'BATTERY, DRY CELL, SIZE AAA', '-', 1, 1, 14, 1, '26111702-BT-A01', 0, 19.76, 0, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);
INSERT INTO `supplies` VALUES (208, 1, 'SIGN PEN, EXTRA FINE TIP, BLUE', '-', 1, 1, 3, 1, '60121524-SP-G02', 17, 26, 1, 0, NULL, '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);

-- ----------------------------
-- Table structure for units
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `units to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `units to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES (1, 1, 'Unit', '2024-01-17 15:54:36', '2024-01-23 18:00:44', NULL);
INSERT INTO `units` VALUES (2, 1, 'Pack', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (3, 1, 'Can', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (4, 1, 'Set', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (5, 1, 'Box', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (6, 1, 'Gallon', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (7, 1, 'Pc', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (8, 1, 'Ream', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (9, 1, 'Bottle', '2024-01-17 15:54:36', '2024-01-17 15:54:36', NULL);
INSERT INTO `units` VALUES (10, 1, 'Roll', '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `units` VALUES (11, 1, 'Tubes', '2024-01-17 15:54:37', '2024-01-17 15:54:37', NULL);
INSERT INTO `units` VALUES (12, 1, 'Bundle', '2024-01-17 15:54:38', '2024-01-17 15:54:38', NULL);
INSERT INTO `units` VALUES (13, 1, 'Pair', '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `units` VALUES (14, 1, 'Pouch', '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `units` VALUES (15, 1, 'Pad', '2024-01-17 15:54:39', '2024-01-17 15:54:39', NULL);
INSERT INTO `units` VALUES (16, 1, 'Jar', '2024-01-17 15:54:40', '2024-01-17 15:54:40', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT 0,
  `is_division` tinyint NOT NULL DEFAULT 0,
  `is_secretary` tinyint NOT NULL DEFAULT 0,
  `is_non_teaching` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'jeff', 'jeff@gmail.com', '09100575676', '$2y$10$BTMMmuqWIkm/QI4auClh4.dXVDTDB7RKXf2hlSbxvicCZG8tBBL.m', 'jeff', 'jeff', 'jeff', 1, 0, 0, 0, 1, '65d45d9b26da965d45d9b26dac', '2024-01-14 20:47:14', '2024-02-21 00:06:51', NULL);
INSERT INTO `users` VALUES (2, 'santiago SDO', 'joana@gmail.com', '09168888042', '$2y$10$WYSG1NCG787LZQwOtze0ceYGkdkNorIhZRcrcokybgcuQ6EQtmgme', 'Joana', 'Ton', 'Doe', 0, 1, 0, 0, 1, '65d4597646acc65d4597646ad0', '2024-01-24 19:43:22', '2024-02-20 23:49:10', NULL);
INSERT INTO `users` VALUES (3, 'secretary', 'secretary@gmail.com', '09100575678', '$2y$10$iZqVnwnjGuC04RMsUfJzpe0wFESRcnrxjMxbYakno0l8TcYnAUUYS', 'Rods', '11', 'Rods', 0, 0, 1, 0, 1, '65d426d519bba65d426d519bbf', '2024-02-20 20:13:09', '2024-02-20 20:13:09', NULL);

SET FOREIGN_KEY_CHECKS = 1;
