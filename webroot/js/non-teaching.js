'use strict';
$(function () {
    Turbolinks.start();

    $('a[turbolink]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        Turbolinks.visit(href, {action : 'advance'});
    });
});