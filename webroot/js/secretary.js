'use strict';
$(function () {
    Turbolinks.start();

    $('a[turbolink]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        Turbolinks.visit(href, {action : 'advance'});
    });

    function getSupplyNotifications() {
        $.ajax({
            url: mainurl+'supplies/getSupplyNotifications',
            type:'GET',
            method:'GET',
            dataType:'JSON',
            beforeSend:function (e) {
                $('#notification-count').text(parseFloat(0));
            }
        }).done(function (data, status, xhr) {

            var notificationList = $('#notification-list');
            if(parseFloat(data.length) > parseFloat(0)){
                $('#notification-count').text(parseFloat(data.length));
                notificationList.append('<span class="dropdown-item dropdown-header">'+(parseFloat(data.length))+' Notifications</span> <div class="dropdown-divider"></div>');
            }

            $.map(data, function (data, key) {

                if((data.type).toLowerCase() == ('stock').toLowerCase()){
                    notificationList.append('<a href="javascript:void(0);" class="dropdown-item truncate"> ' +
                        '<i class="fas fa-boxes mr-2"></i> ' +
                        (data.supply_name) +' Is Low On Stock'+
                        '<span class="float-right text-muted text-sm truncate">'+(parseFloat(data.stock))+' Left</span> ' +
                        '</a> ' +
                        '<div class="dropdown-divider"></div>');
                }

                if((data.type).toLowerCase() == ('expiration').toLowerCase()){
                    notificationList.append('<a href="javascript:void(0);" class="dropdown-item truncate"> ' +
                        '<i class="fas fa-boxes mr-2"></i> ' +
                        (data.supply_name) +' Is Expiring'+
                        '<span class="float-right text-muted text-sm truncate">'+(moment(data.expired_at).fromNow(true))+'</span> ' +
                        '</a> ' +
                        '<div class="dropdown-divider"></div>');
                }

            });

            notificationList.append('<a href="javascript:void(0);" class="dropdown-item dropdown-footer">Notifications</a>');

        }).fail(function (data, status, xhr) {

        });
    }

    getSupplyNotifications();
});