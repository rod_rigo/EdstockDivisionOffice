/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : edstock_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 17/01/2024 15:56:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for units
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES (1, 'pc', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (2, 'bottle', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (3, 'ream', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (4, 'box', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (5, 'liter', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (6, 'pack', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (7, 'person', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (8, 'roll', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (9, 'set', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (10, 'Tubes', '2023-11-12 13:04:49', '2023-11-12 21:06:20', NULL);
INSERT INTO `units` VALUES (11, 'Kg', '2023-11-12 21:06:28', '2023-11-12 21:06:28', NULL);
INSERT INTO `units` VALUES (12, 'Unit', '2023-11-18 22:53:28', '2023-11-18 22:53:28', NULL);

SET FOREIGN_KEY_CHECKS = 1;
